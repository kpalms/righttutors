﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class AboutUs : System.Web.UI.Page
{
    BL_AboutUs bl_aboutus = new BL_AboutUs();
    protected void Page_Load(object sender, EventArgs e)
    {
        BindAboutUs();
    }

    private void BindAboutUs()
    {
        DataTable dt = bl_aboutus.SelectAboutUs();
        if (dt.Rows.Count > 0)
        {
            lblaboutus.Text = dt.Rows[0]["AboutUs"].ToString();
        }
       
    }
}