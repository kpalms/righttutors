﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Web.Script.Services;
using System.IO;
public partial class Student_StudentRegistration : System.Web.UI.Page
{
    Common common = new Common();
    BL_Students bl_student = new BL_Students();
    BL_Subject bl_subject = new BL_Subject();

    protected void Page_Load(object sender, EventArgs e)
    {
            if (!IsPostBack)
            {
                common.BindBoard(ddlBoard);
                common.BindClass(ddlClass);
                common.BindCountry(ddlCountry);
                common.SelectStateCountryId(ddlState, 0);
                common.SelectCityStateId(ddlCity, 0);
                common.SelectAreaCityId(ddlArea, 0);
            }

            if (Session["StudentsId"] != null)
            {
                Response.Redirect("StudentProfile.aspx");
            }

            if (Session["TeachersId"] != null)
            {
                Response.Redirect("TeacherProfile.aspx");
            }
    }

    private void SetFill()
    {
        bl_student.Name = txtName.Text;
        bl_student.MiddleName = txtMiddleName.Text;
        bl_student.LastName = txtLastName.Text;
        bl_student.DOB = txtDob.Text;
        bl_student.CountryId = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        bl_student.StateId = Convert.ToInt32(ddlState.SelectedItem.Value);
        bl_student.CityId = Convert.ToInt32(ddlCity.SelectedItem.Value);
        bl_student.AreaId = Convert.ToInt32(ddlArea.SelectedItem.Value);
        bl_student.Address = txtAddress.Text;
        if (txtpincode.Text != "")
        {
            bl_student.PinCode = Convert.ToInt32(txtpincode.Text);
        }
        bl_student.Gender = Convert.ToInt32(ddlGender.SelectedItem.Value);
        bl_student.MobileNo = txtMobileNo.Text;
        bl_student.Email = txtEmail.Text;
        bl_student.BoardId = Convert.ToInt32(ddlBoard.SelectedItem.Value);
        bl_student.ClassId = Convert.ToInt32(ddlClass.SelectedItem.Value);
        bl_student.Password = txtPassword.Text;
        bl_student.DateTime = Convert.ToDateTime(common.GetDate());
    }

    private void Clear()
{
    txtName.Text = "";
    txtMiddleName.Text = "";
    txtLastName.Text = "";
    txtDob.Text = "";
    txtAddress.Text = "";
    txtpincode.Text = "";
    txtMobileNo.Text = "";
    txtEmail.Text = "";
    txtPassword.Text = "";
    txtReTypePassword.Text = "";
    ddlArea.SelectedIndex = 0;
    ddlCity.SelectedIndex = 0;
    ddlCountry.SelectedIndex = 0;
    ddlState.SelectedIndex = 0;
    ddlGender.SelectedIndex = 0;
    ddlBoard.SelectedIndex = 0;
    ddlClass.SelectedIndex = 0;
    chksubject.Items.Clear();
    chksubject.Visible = false;
}

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (chksubject.SelectedIndex != -1)
        {
            lblsubject.Visible = false;
            string str = "select count(*) from Students where Email='" + txtEmail.Text + "'";
            string count = common.ReturnOneValue(str);

            //if (count == "0")
            //{
                lblemail.Visible = false;
                SetFill();
                bl_student.StudentsId = 0;
                bl_student.Mode = "Insert";

                string SubjectList = "";
                for (int i = 0; i <= chksubject.Items.Count - 1; i++)
                {
                    if (chksubject.Items[i].Selected)
                    {
                        if (SubjectList == "")
                        {
                            SubjectList = chksubject.Items[i].Value;
                        }
                        else
                        {
                            SubjectList += "," + chksubject.Items[i].Value;
                        }
                    }
                }
                bl_student.SubjectId = SubjectList + ",";


                if (fileimage.HasFile)
                {
                    string ext = Path.GetExtension(fileimage.FileName);
                    string filename = common.GetImageName() + "" + ext;
                    fileimage.PostedFile.SaveAs(Server.MapPath("rt/Images/Student/") + filename);
                    bl_student.ImageName = filename;
                }
                else
                {
                    bl_student.ImageName = "NoImage.png";
                }

                int sid = bl_student.AddStudents(bl_student);

                EmailSms email = new EmailSms();
                string url = "<table><tr><td>www.righttutors.in/StudentProfile.aspx?SID=" + sid + "</td></tr></table>";
                string body = "Wel Come to Right Tutors<br/>" + url;
                //email.SendMail("Right Tutors", body, "support@righttutors.in," + txtEmail.Text);
                Clear();
                this.ModalPopupExtender1.Show();
            //}
            //else
            //{
            //    lblemail.Text = "Already Existing Email !";
            //}

        }
        else
        {
            lblsubject.Text = "Please select at least one Subject.";
        }
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        int cid = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        common.SelectStateCountryId(ddlState, cid);
        common.SelectCityStateId(ddlCity, 0);
        common.SelectAreaCityId(ddlArea, 0);
    }

    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
        int sid = Convert.ToInt32(ddlState.SelectedItem.Value);
        common.SelectCityStateId(ddlCity, sid);
        common.SelectAreaCityId(ddlArea, 0);
    }

    protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        int ciid = Convert.ToInt32(ddlCity.SelectedItem.Value);
        common.SelectAreaCityId(ddlArea, ciid);
    }

    protected void ddlClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        bl_subject.BoardId = Convert.ToInt32(ddlBoard.SelectedItem.Value);
        bl_subject.ClassId = Convert.ToInt32(ddlClass.SelectedItem.Value);
         DataTable dt= bl_subject.SelectSubjectByBoardIdClassId(bl_subject);
         chksubject.DataSource = dt;
         chksubject.DataValueField = "SubjectId";
         chksubject.DataTextField = "Subject";
         chksubject.DataBind();
    }

    protected void ddlBoard_SelectedIndexChanged(object sender, EventArgs e)
    {
        bl_subject.BoardId = Convert.ToInt32(ddlBoard.SelectedItem.Value);
        bl_subject.ClassId = Convert.ToInt32(ddlClass.SelectedItem.Value);
        DataTable dt = bl_subject.SelectSubjectByBoardIdClassId(bl_subject);
        chksubject.DataSource = dt;
        chksubject.DataValueField = "SubjectId";
        chksubject.DataTextField = "Subject";
        chksubject.DataBind();
    }

    protected void btnok_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
}