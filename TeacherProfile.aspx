﻿<%--<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeFile="TeacherProfile.aspx.cs" Inherits="TeacherProfile" %>--%>
<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TeacherProfile.aspx.cs" Inherits="TeacherProfile" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script src="Js/jquery-1.8.2.js" type="text/javascript"></script>
<script src="JsSlider/validation.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function () {
        var showChar = 120, showtxt = "Full Post", hidetxt = "Less";
        $('.more').each(function () {
            var content = $(this).text();
            if (content.length > showChar) {
                var con = content.substr(0, showChar);
                var hcon = content.substr(showChar, content.length - showChar);
                var txt = con + '<span class="dots">...</span><span class="morecontent"><span>' + hcon + '</span>&nbsp;&nbsp;<a href="" class="moretxt" style="color:Blue">' + showtxt + '</a></span>';
                $(this).html(txt);
            }
        });
        $(".moretxt").click(function () {
            if ($(this).hasClass("sample")) {
                $(this).removeClass("sample");
                $(this).text(showtxt);
            } else {
                $(this).addClass("sample");
                $(this).text(hidetxt);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });
    });
</script>

<style type="text/css">

.more {

}
.morecontent span {
display: none;
}


.zoom {      
-webkit-transition: all 0.35s ease-in-out;    
-moz-transition: all 0.35s ease-in-out;    
transition: all 0.35s ease-in-out;     
cursor: -webkit-zoom-in;      
cursor: -moz-zoom-in;      
cursor: zoom-in;  
}     

.zoom:hover,  
.zoom:active,   
.zoom:focus {
/**adjust scale to desired size, 
add browser prefixes**/
-ms-transform: scale(2.5);    
-moz-transform: scale(2.5);  
-webkit-transform: scale(2.5);  
-o-transform: scale(2.5);  
transform: scale(2.5);    
position:relative;      
z-index:100;  
}



 .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=90);
        opacity: 0.8;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        border-width: 3px;
        border-style: solid;
        border-color: black;
        padding-top: 10px;
        padding-left: 10px;
        width: 300px;
        height: 140px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="aboutUs">
      <div class="container">
        <div class="row">
        <!-- Start about us area -->
        <div class="col-lg-4 col-md-4 col-sm-4">
          <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12" >
 <h2 class="titile" >
 <span style="color:Red"><asp:Label ID="lblnamehead" runat="server"></asp:Label></span>
 <asp:Label ID="lbllastname" runat="server"></asp:Label>
 </h2>   
  </div>
  </div>
        

         <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12" >
             <a href="#" class="blog_img"><asp:Image ID="imgprofile" runat="server"  Height="300px" Width="300px"/></a>
            </div>
         </div>
          
        
              <!-- start single sidebar -->
               <div class="row" style="margin-top:10px">
         <div class="col-lg-12 col-md-12 col-sm-12" >
              <div class="single_sidebar" style="margin-top:10px">
                <ul class="tags_nav">
                  <li><a href="EditTeacher.aspx?pg=et" style="font-size:13px"><i class="fa fa-tags"></i>Update Profile</a></li>
                  <li><a href="TestimonialTeacher.aspx?pg=tt" style="font-size:13px"><i class="fa fa-tags"></i>Write Testimonial</a></li>
                </ul>
             </div>
               </div>
         </div>



           <div class="row">
            <div class="col-ld-12 col-md-12 col-sm-12">
             <div class="footer_top2">
              <div class="single_footer_widget2">
                   <span style="font-size:18px;font-family:""Helvetica Neue",Helvetica,Arial,sans-serif"><asp:Label ID="lbltutors" runat="server" ForeColor="#00A3E8"></asp:Label>+(Our Tutors)</span>
               </div>
              <br />
              <div class="single_footer_widget2">
                 <span style="font-size:18px;font-family:""Helvetica Neue",Helvetica,Arial,sans-serif"><asp:Label ID="lblstudent" runat="server"  ForeColor="#00A3E8"></asp:Label>+(Our Students)</span>

             </div>
           
              <div class="single_footer_widget2">
                <h3>Social Links</h3>
                <ul class="footer_social2" style="margin-left:25px">
                  <li><a data-toggle="tooltip" data-placement="top" title="Facebook" class="soc_tooltip" href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a data-toggle="tooltip" data-placement="top" title="Twitter" class="soc_tooltip"  href="#"><i class="fa fa-twitter"></i></a></li>
                  <li><a data-toggle="tooltip" data-placement="top" title="Instagram" class="soc_tooltip"  href="#"><i class="fa fa-instagram"></i></a></li>
                  <li><a data-toggle="tooltip" data-placement="top" title="Youtube" class="soc_tooltip"  href="#"><i class="fa fa-youtube"></i></a></li>
                    <li><a data-toggle="tooltip" data-placement="top" title="pinterest" class="soc_tooltip"  href="#"><i class="fa fa-pinterest"></i></a></li>
                  <li><a data-toggle="tooltip" data-placement="top" title="Whatsup" class="soc_tooltip"  href="#"><i class="fa fa-whatsapp"></i></a></li>
                </ul>
              </div>
            </div>
            </div>
          </div>
          
      </div>


        <div class="col-lg-8 col-md-8 col-sm-8">
          <div class="newsfeed_area wow fadeInRight">
            <ul class="nav nav-tabs feed_tabs" id="myTab2">

              <li class="active"><a href="#news" data-toggle="tab">Profile</a></li>
              <li><a href="#notice" data-toggle="tab">Today's Post</a></li>
              <li><a href="#events" data-toggle="tab">News</a></li>     

            </ul>

            <!-- Tab panes -->
            <div class="tab-content">



              <!-- Start news tab content -->
              <div class="tab-pane fade in active" id="news">  
              
              
              <br />
                    <div class="row">

             
              <div class="col-lg-4 col-md-4 col-sm-4">
              <div class="panel panel-info">
                    <div class="panel-heading">
                    <h3 class="panel-title">Right Tutors Id</h3>
                    </div>
                    <div class="panel-body"><asp:Label ID="lblteacherid" runat="server"></asp:Label>  </div>
                </div>
              </div>

            
             


              <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="panel panel-success">
                    <div class="panel-heading">
                    <h3 class="panel-title">Date of Birth</h3>
                    </div>
                    <div class="panel-body"><asp:Label ID="lbldob" runat="server"></asp:Label> </div>
                </div>
                </div>


                 <div class="col-lg-4 col-md-4 col-sm-4">
                 <div class="panel panel-warning">
                    <div class="panel-heading">
                    <h3 class="panel-title">Gender</h3>
                    </div>
                    <div class="panel-body"><asp:Label ID="lblgender" runat="server"></asp:Label> </div>
                </div>
                </div>
                 </div>
             
               <div class="row">

              <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                    <h3 class="panel-title">Mobile No</h3>
                    </div>
                    <div class="panel-body"><asp:Label ID="lblmobileno" runat="server"></asp:Label> </div>
                </div>
                </div>

                 <div class="col-lg-6 col-md-6 col-sm-6">
                 <div class="panel panel-danger">
                    <div class="panel-heading">
                    <h3 class="panel-title">Email Id</h3>
                    </div>
                    <div class="panel-body"><asp:Label ID="lblemail" runat="server"></asp:Label> </div>
                </div>
                </div>

              </div>

               <div class="row">

              <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="panel panel-warning">
                    <div class="panel-heading">
                    <h3 class="panel-title">Address</h3>
                    </div>
                    <div class="panel-body"><asp:Label ID="lbladdress" runat="server"></asp:Label> </div>
                </div>
              </div>

              </div>


               <div class="row">

                  <div class="col-lg-4 col-md-4 col-sm-4">
                 <div class="panel panel-danger">
                    <div class="panel-heading">
                    <h3 class="panel-title">Experience</h3>
                    </div>
                    <div class="panel-body"><asp:Label ID="lblExprience" runat="server"></asp:Label> </div>
                </div>
                </div>

              <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="panel panel-info">
                    <div class="panel-heading">
                    <h3 class="panel-title">Education</h3>
                    </div>
                    <div class="panel-body"><asp:Label ID="lblEducation" runat="server"></asp:Label> </div>
                </div>
                </div>

              </div>




               <div class="row">

              <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="panel panel-success">
                    <div class="panel-heading">
                    <h3 class="panel-title">Tag Line</h3>
                    </div>
                    <div class="panel-body"><asp:Label ID="lbltagline" runat="server"></asp:Label> </div>
                </div>
              </div>

              </div>


               <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="panel panel-primary">
                    <div class="panel-heading">
                    <h3 class="panel-title">About Me</h3>
                    </div>
                    <div class="panel-body"><asp:Label ID="lblaboutme" runat="server"></asp:Label> </div>
                </div>
              </div>
              </div>
                
                      

                    <br />
              <br />
              <br />
               <span style="font-size:10px;color:red">(***Add Multiple Board And Multiple Subject From Account Menu)</span>  
              <asp:Repeater ID="rptteacherboardclass" runat="server" 
                  onitemdatabound="rptteacherboardclass_ItemDataBound">
              <ItemTemplate>
              <asp:Label ID="lblboard" runat="server" Text='<%# Bind("BoardId") %>' Visible="false"></asp:Label>
              <asp:Label ID="lblclass" runat="server" Text='<%# Bind("ClassId") %>' Visible="false"></asp:Label>

               <asp:Repeater ID="rptsubject" runat="server">
              <ItemTemplate>
               <div class="panel panel-warning">
                    <div class="panel-heading">
                    <table border="0px" width="100%" cellpadding="0px" cellspacing="0px">
            <tr>
            <td> 
                    <h3 class="panel-title"> <asp:Label ID="lblboard" runat="server" Text='<%# Bind("Board") %>'></asp:Label> </h3></td>
                   <td><h3  class="panel-title" style="float:right"> <asp:Label ID="lblclass" runat="server" Text='<%# Bind("Class") %>'></asp:Label></h3>
                   </td>
                   </tr>
                   </table>
                    </div>
                    <div class="panel-body">  <asp:Label ID="lblsubject" runat="server" Text='<%# Bind("Subjects") %>'></asp:Label> </div>
                </div>
    
               </ItemTemplate>
              </asp:Repeater>
              <br>
              </ItemTemplate>
              </asp:Repeater>

              </div>
              <!-- Start notice tab content -->  

              <div class="tab-pane fade " id="notice">
        
                <div class="single_notice_pane">
                <br />
                <span style="font-size:10px;color:Red";text-align:"center">***Post Relevant To Board,Class,Subjects Only</span>
                   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                   <ContentTemplate>

                   <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label><br />
  <asp:DropDownList ID="ddlBoard" runat="server" AutoPostBack="True" class="wp-form-control wpcf7-text"
         onselectedindexchanged="ddlBoard_SelectedIndexChanged">
    </asp:DropDownList>
    <asp:RequiredFieldValidator ControlToValidate="ddlBoard" ID="RequiredFieldValidator5"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a Board"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
    <br />
   <asp:DropDownList ID="ddlClass" runat="server" AutoPostBack="True" class="wp-form-control wpcf7-text"
         onselectedindexchanged="ddlClass_SelectedIndexChanged">
    </asp:DropDownList>
    <asp:RequiredFieldValidator ControlToValidate="ddlClass" ID="RequiredFieldValidator1"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a Class"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
    <br />
<asp:TextBox ID="txttitle" runat="server" class="wp-form-control wpcf7-text" placeholder="Enter Title(eg..Math,Science)"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please Enter Title" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txttitle"></asp:RequiredFieldValidator>
<br />
<asp:TextBox ID="txtdescription" runat="server" class="wp-form-control wpcf7-textarea" placeholder="Enter Description" TextMode="MultiLine"></asp:TextBox>

<br />
<asp:FileUpload ID="fileimage" runat="server" />
<br />
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.bmp|.jpeg)$"
    ControlToValidate="fileimage" ValidationGroup="new" runat="server" ForeColor="Red" ErrorMessage="Please select a valid png,jpg,gif,bmp,jpeg file."
    Display="Dynamic" />  
<sapm style="font-size:10px;color:red" >(upload only png,jpg,gif,bmp,jpeg)</sapm>
<br />
<br />
 <asp:Image ID="imgedit" runat="server" Visible="false" Width="50px" Height="50px"/>
 <br />
 <br />

<div class="row">
<div class="col-lg-3 col-md-3 col-sm-3">
</div>

<div class="col-lg-2 col-md-2 col-sm-2">
<asp:Button ID="btnsave" runat="server" class="wpcf7-submit" ValidationGroup="new" Text="Save" onclick="btnsave_Click" />
</div>

<div class="col-lg-2 col-md-2 col-sm-2">
<asp:Button ID="btnupdate" runat="server" class="wpcf7-updated" ValidationGroup="new" Text="Update" onclick="btnupdate_Click" />
</div>

<div class="col-lg-2 col-md-2 col-sm-2">
<asp:Button ID="btnclear" runat="server" Text="Clear" class="wpcf7-clear" onclick="btnclear_Click" />
</div>

<div class="col-lg-3 col-md-3 col-sm-3">
</div>
</div>
<br />
<br />

<asp:Repeater ID="rpttodaypost" runat="server" onitemcommand="rpttodaypost_ItemCommand">
<ItemTemplate>

 <div class="panel panel-warning">
            <div class="panel-heading">
            <table border="0px" width="100%" cellpadding="0px" cellspacing="0px">
            <tr>
            <td> <h3 class="panel-title"><asp:Label ID="lblTitle" runat="server" Text='<%# Bind("Title") %>'></asp:Label></h3></td>

            <td><h5 style="float:right"> <asp:Label ID="lbldate" runat="server" Text='<%# Bind("Date") %>'></asp:Label></h5>
           
            </td>
            <td>
            <div class="dropdown" style="float:right">
              <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-external-link" aria-hidden="true"></i>
              </button>
              <ul class="dropdown-menu">
                <li><asp:LinkButton ID="lnkactive" CommandName="Active" CommandArgument='<%# Bind("TodayPostId") %>' Text='<%# Bind("Active") %>' runat="server"></asp:LinkButton></li>
                <li><asp:LinkButton ID="lnkEdit" CommandName="EditRow"  CommandArgument='<%# Bind("TodayPostId") %>' runat="server">Edit</asp:LinkButton></li>
                <li><asp:LinkButton ID="lnkdelete" CommandName="DeleteRow"  CommandArgument='<%# Bind("TodayPostId") %>' OnClientClick="return conformbox();" runat="server">Delete</asp:LinkButton></li>
              </ul>
            </td>
            </tr>
            </table>
             </div>
            

             <div class="panel-body">
            <table border="0px" width="100%" cellpadding="0px" cellspacing="0px">
            <tr>

            <td style="width:40%">
             <img src='rt/<%# Eval("Image") %>' alt="Image" class="img-responsive img-thumbnail zoom" width="200px"/>
            </td>
            
             <td style="width:60%" class="more">
             <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
              <asp:Label ID="lblTodayPostId" runat="server" Visible="false" Text='<%# Bind("TodayPostId") %>'></asp:Label>
               <asp:Label ID="lblBoardId" runat="server" Visible="false" Text='<%# Bind("BoardId") %>'></asp:Label>
             <asp:Label ID="lblClassId" runat="server" Visible="false" Text='<%# Bind("ClassId") %>'></asp:Label>
             <asp:Label ID="lblImage" runat="server" Text='<%#Bind("Image")%>' Visible="false"></asp:Label>
           
             </td>
         
              </tr>
            </table>
            </div>
            <div class="panel-footer">
            <asp:Label ID="lblBoard" runat="server" Text='<%# Bind("Board") %>'></asp:Label>
            <div style="float:right">  <asp:Label ID="lblClass" runat="server" Text='<%# Bind("Class") %>'></asp:Label></div>
            </div>
            </div>
           
    <br />
</ItemTemplate>
</asp:Repeater>
    </ContentTemplate>
    <Triggers>
<asp:PostBackTrigger ControlID="btnsave" />
<asp:PostBackTrigger ControlID="btnupdate" />
</Triggers>
</asp:UpdatePanel>
                </div>               
              </div>


              <!-- Start events tab content -->
              <div class="tab-pane fade " id="events">
                <br />
                   <asp:Repeater ID="rptnews" runat="server">
                    <ItemTemplate>
                  <div class="panel panel-info">
            <div class="panel-heading">
            <table border="0px" width="100%" cellpadding="0px" cellspacing="0px">
            <tr>
            <td> <h3 class="panel-title"><asp:Label ID="lbltitle" runat="server" Text='<%# Bind("Title") %>'></asp:Label></h3></td>

            <td><h5 style="float:right"> <asp:Label ID="Label2" runat="server" Text='<%# Bind("Date") %>'></asp:Label></h5>
            </td>
            </tr>
            </table>
            </div>
           

            <div class="panel-body">
            <table border="0px" width="100%" cellpadding="0px" cellspacing="0px">
            <tr>

            <td style="width:40%"><img src='<%# Eval("Image") %>' alt="Image" class="img-responsive img-thumbnail zoom" width="200px"/></td>
             <td class="more"  style="width:60%">
            <asp:Label ID="lbldesc" runat="server" Text='<%# Bind("Description") %>'></asp:Label></td>
              </tr>
            </table>
            </div>
            </div>
                  
                    </ItemTemplate>
                    </asp:Repeater>
               
              </div>






            </div>
          </div>
        </div>
      </div>
      </div>
    </section>
    <!--=========== END ABOUT US SECTION ================--> 







     <asp:HiddenField ID="hfHidden" runat="server" />
    <asp:Panel ID="Panel1" CssClass="modalPopup" align="center" style = "display:none" runat="server">
    <table align="center">
    <tr>
   <b>
       <asp:Label ID="lblmsgdata" runat="server"></asp:Label></b>
    </tr>
    <br />
    <br />
    <br />
    <tr>
    <td>
    <asp:Button ID="Button2" class="LoginButton" runat="server" Text="Ok"/>
    </td>
    </tr>
    </table>
    
    </asp:Panel>
   
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" BackgroundCssClass="modalBackground"
    CancelControlID="Button2" PopupControlID="Panel1" TargetControlID="hfHidden" runat="server">
    </ajaxToolkit:ModalPopupExtender>





</asp:Content>
