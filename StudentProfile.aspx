﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="StudentProfile.aspx.cs" Inherits="StudentProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">



 <script src="Js/jquery-1.8.2.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function () {
        var showChar = 120, showtxt = "Full Post", hidetxt = "Less";
        $('.more').each(function () {
            var content = $(this).text();
            if (content.length > showChar) {
                var con = content.substr(0, showChar);
                var hcon = content.substr(showChar, content.length - showChar);
                var txt = con + '<span class="dots">...</span><span class="morecontent"><span>' + hcon + '</span>&nbsp;&nbsp;<a href="" class="moretxt" style="color:Blue">' + showtxt + '</a></span>';
                $(this).html(txt);
            }
        });
        $(".moretxt").click(function () {
            if ($(this).hasClass("sample")) {
                $(this).removeClass("sample");
                $(this).text(showtxt);
            } else {
                $(this).addClass("sample");
                $(this).text(hidetxt);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });
    });
</script>
<style type="text/css">

.more {

}
.morecontent span {
display: none;
}

.sticky {
    margin: 0;
    padding: 0;
    width: 100%;
    background-color: #bffff3;
    position: sticky;
}



.zoom {      
-webkit-transition: all 0.35s ease-in-out;    
-moz-transition: all 0.35s ease-in-out;    
transition: all 0.35s ease-in-out;     
cursor: -webkit-zoom-in;      
cursor: -moz-zoom-in;      
cursor: zoom-in;  
}     

.zoom:hover,  
.zoom:active,   
.zoom:focus {
/**adjust scale to desired size, 
add browser prefixes**/
-ms-transform: scale(2.5);    
-moz-transform: scale(2.5);  
-webkit-transform: scale(2.5);  
-o-transform: scale(2.5);  
transform: scale(2.5);    
position:relative;      
z-index:100;  
}

</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


 
    <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="aboutUs" >
      <div class="container">
        <div class="row" style="margin:0px;padding:0px;">
        <!-- Start about us area -->
        <div class="col-lg-4 col-md-4 col-sm-4 " >
        <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12" >
            <h2 class="titile">
            <span style="color:Red"><asp:Label ID="lblnamehead" runat="server"></asp:Label></span>
            <asp:Label ID="lbllastname" runat="server"></asp:Label>
            </h2>
         </div>
         </div>

         <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12" >
         <a href="#" class="blog_img"><asp:Image ID="imgprofile" runat="server"  Height="280px" Width="280px"/></a>
         </div>
         </div>
        

        
              <!-- start single sidebar -->
               <div class="row" style="margin-top:10px">
         <div class="col-lg-12 col-md-12 col-sm-12" >
              <div class="single_sidebar" style="margin-top:10px">
                <ul class="tags_nav">
                  <li><a href="EditStudent.aspx?pg=es" style="font-size:13px"><i class="fa fa-tags"></i>Update Profile</a></li>
                  <li><a href="TestimonialStudent.aspx?pg=ts" style="font-size:13px"><i class="fa fa-tags"></i>Write Testimonial</a></li>
                </ul>
              </div>
               </div>
         </div>
              <!-- End single sidebar -->
         


                
       
         <div class="row">
            <div class="col-ld-12 col-md-12 col-sm-12">
             <div class="footer_top2">
              <div class="single_footer_widget2">
    <span style="font-size:18px;font-family:""Helvetica Neue",Helvetica,Arial,sans-serif"><asp:Label ID="lbltutors" runat="server" ForeColor="#00A3E8"></asp:Label>+(Our Tutors)</span>
              </div>
              <br />
              <div class="single_footer_widget2">
 <span style="font-size:18px;font-family:""Helvetica Neue",Helvetica,Arial,sans-serif"><asp:Label ID="lblstudent" runat="server"  ForeColor="#00A3E8"></asp:Label>+(Our Students)</span>
              </div>
           
              <div class="single_footer_widget2">
                <h3>Social Links</h3>
                <ul class="footer_social2" style="margin-left:25px">
                  <li><a data-toggle="tooltip" data-placement="top" title="Facebook" class="soc_tooltip" href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a data-toggle="tooltip" data-placement="top" title="Twitter" class="soc_tooltip"  href="#"><i class="fa fa-twitter"></i></a></li>
                  <li><a data-toggle="tooltip" data-placement="top" title="Instagram" class="soc_tooltip"  href="#"><i class="fa fa-instagram"></i></a></li>
                  <li><a data-toggle="tooltip" data-placement="top" title="Youtube" class="soc_tooltip"  href="#"><i class="fa fa-youtube"></i></a></li>
                 <li><a data-toggle="tooltip" data-placement="top" title="pinterest" class="soc_tooltip"  href="#"><i class="fa fa-pinterest"></i></a></li>
                  <li><a data-toggle="tooltip" data-placement="top" title="Whatsup" class="soc_tooltip"  href="#"><i class="fa fa-whatsapp"></i></a></li>
                </ul>
              </div>
            </div>
            </div>
          </div>
          
      </div>


     
     
        




        <div class="col-lg-8 col-md-8 col-sm-8">
         
          <div class="newsfeed_area wow fadeInRight">
            <ul class="nav nav-tabs feed_tabs" id="myTab2">

              <li class="active"><a href="#news" data-toggle="tab">Profile</a></li>
              <li><a href="#notice" data-toggle="tab">Today's Post</a></li>
              <li><a href="#events" data-toggle="tab">News</a></li>     

            </ul>

            <!-- Tab panes -->
           
            <div class="tab-content" >

              <!-- Start news tab content -->
              <div class="tab-pane fade in active" id="news">  
              <br />
                    <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4">
              <div class="panel panel-info">
                    <div class="panel-heading">
                    <h3 class="panel-title">Right Tutors Id</h3>
                    </div>
                    <div class="panel-body"><asp:Label ID="lblstudentid" runat="server"></asp:Label> </div>
                </div>
              </div>

              <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="panel panel-success">
                    <div class="panel-heading">
                    <h3 class="panel-title">Date of Birth</h3>
                    </div>
                    <div class="panel-body"><asp:Label ID="lbldob" runat="server"></asp:Label> </div>
                </div>
                </div>

                 <div class="col-lg-4 col-md-4 col-sm-4">
                 <div class="panel panel-warning">
                    <div class="panel-heading">
                    <h3 class="panel-title">Gender</h3>
                    </div>
                    <div class="panel-body"><asp:Label ID="lblgender" runat="server"></asp:Label> </div>
                </div>
                </div>

              </div>

             
                <div class="row">

              <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                    <h3 class="panel-title">Mobile No</h3>
                    </div>
                    <div class="panel-body"><asp:Label ID="lblmobileno" runat="server"></asp:Label> </div>
                </div>
                </div>

                 <div class="col-lg-6 col-md-6 col-sm-6">
                 <div class="panel panel-danger">
                    <div class="panel-heading">
                    <h3 class="panel-title">Email Id</h3>
                    </div>
                    <div class="panel-body"><asp:Label ID="lblemail" runat="server"></asp:Label> </div>
                </div>
                </div>

              </div>

              


               <div class="row">

              <div class="col-lg-6 col-md-6 col-sm-6">
              
                <div class="panel panel-info">
                    <div class="panel-heading">
                    <h3 class="panel-title">Board</h3>
                    </div>
                    <div class="panel-body"><asp:Label ID="lblboard" runat="server"></asp:Label> </div>
                </div>
                </div>

                 <div class="col-lg-6 col-md-6 col-sm-6">
                 <div class="panel panel-success">
                    <div class="panel-heading">
                    <h3 class="panel-title">Class</h3>
                    </div>
                    <div class="panel-body"><asp:Label ID="lblclass" runat="server"></asp:Label> </div>
                </div>
                </div>

              </div>

               <div class="row">

              <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="panel panel-warning">
                    <div class="panel-heading">
                    <h3 class="panel-title">Subjects  <span style="font-size:10px;color:Red">(***Add Subjects Which Requires a "Right Tutors" From Account Menu)</span></h3>
                    </div>
                    <div class="panel-body"><asp:Label ID="lblsubject" runat="server"></asp:Label> </div>
                </div>
              </div>

              </div>


               <div class="row">

              <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="panel panel-primary">
                    <div class="panel-heading">
                    <h3 class="panel-title">Address</h3>
                    </div>
                    <div class="panel-body"><asp:Label ID="lbladdress" runat="server"></asp:Label> </div>
                </div>
              </div>

              </div>
        
                

                
                           

              </div>
              <!-- Start notice tab content -->  

              <div class="tab-pane fade " id="notice">
              <br />
                   <asp:Repeater ID="rpttodyapost" runat="server">
                    <ItemTemplate>
                     <div class="panel panel-warning">
            <div class="panel-heading">
            <table border="0px" width="100%" cellpadding="0px" cellspacing="0px">
            <tr>
            <td> <h3 class="panel-title" style="float:left"><asp:Label ID="lbltitle" runat="server" Text='<%# Bind("Title") %>'></asp:Label></h3></td>

            <td> <h5 style="float:right"> <asp:Label ID="Label2" runat="server" Text='<%# Bind("Date") %>'></asp:Label></h5></td>
            </tr>
            </table>
               
               
            </div>

            <div class="panel-body">
            <table border="0px" width="100%" cellpadding="0px" cellspacing="0px">
            <tr>

            <td style="width:40%">

            
                        <img src='<%# Eval("Image") %>' alt="Image" class="img-responsive img-thumbnail zoom" width="200px" />
            
            
            </td>
            
             <td style="width:60%" class="more">
             
            
             <asp:Label ID="lbldesc" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
            
             
            </td>
              
              </tr>
            </table>
            </div>
             <div class="panel-footer">
            <asp:Label ID="lblRTT" runat="server" Text='<%# Bind("RTT") %>'></asp:Label>
            </div>
            </div>
                    
                    </ItemTemplate>
                    </asp:Repeater>               
              </div>


              <!-- Start events tab content -->
              <div class="tab-pane fade " id="events">
              <br />
                   <asp:Repeater ID="rptnews" runat="server">
                    <ItemTemplate>
                  <div class="panel panel-info">
            <div class="panel-heading">
            <table border="0px" width="100%" cellpadding="0px" cellspacing="0px">
            <tr>
            <td> <h3 class="panel-title" style="float:left"><asp:Label ID="lbltitle" runat="server" Text='<%# Bind("Title") %>'></asp:Label></h3></td>

            <td> <h5 style="float:right"> <asp:Label ID="Label2" runat="server" Text='<%# Bind("Date") %>'></asp:Label></h5></td>
            </tr>
            </table>
               
               
            </div>
            <div class="panel-body">
            <table border="0px" width="100%" cellpadding="0px" cellspacing="0px">
            <tr>

            <td style="width:40%">
             <img src='<%# Eval("Image") %>' alt="Image" class="img-responsive img-thumbnail zoom" width="200px"/>
            </td>
             
             <td style="width:60%" class="more">
             <asp:Label ID="lbldesc" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
            </td>
              
              </tr>
            </table>
            </div>
            </div>
                  
                    </ItemTemplate>
                    </asp:Repeater>
              </div>


            </div>
          
          </div>
        </div>
       
       </div>

        </div>



      </div>
      </div>
    </section>
    <!--=========== END ABOUT US SECTION ================--> 

</asp:Content>

