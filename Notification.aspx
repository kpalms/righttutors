﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Notification.aspx.cs" Inherits="Notification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   <script src="Js/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">

//    window.onload = BindGridview;

//    var pageIndex = 1;
//    var pageCount;
//    $(window).scroll(function () {
//        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
//            GetRecords();
//        }
//    });

//    function BindGridview() {
//        debugger;
//        var kk = 0;
//        $.ajax({
//            type: "POST",
//            contentType: "application/json; charset=utf-8",
//            url: "Notification.aspx/BindNotifications",
//            data: "{}",
//            dataType: "json",
//            success: function (data) {
//                var result = data.d;
//                $("#getnotifications").empty();
//                for (var i = 0; i < result.length; i++) {
//$("#getnotifications").append(
//+'<table border="0">'
//+ '<tr>'
//+ '<td>'

//+ '<div class="alert alert-info alert-dismissable">'
//+ '<h4>' + result[i].DateTime + '</h4>'
//+ '<p>' + result[i].Message + '</p>'
//+ '</div>'

//+ '</td>'
//+ '</tr>'
//+ '</table>');
//                }

//            },
//            error: function (data) {
//                var r = data.responseText;
//                var errorMessage = r.Message;
//                alert(errorMessage);
//            }
//        });
//    }


//    function GetRecords() {
//        debugger;
//        pageIndex++;
//        if (pageIndex != 1 || pageIndex <= pageCount) {
//            $.ajax({
//                type: "POST",
//                url: "Notification.aspx/GetNotifications",
//                data: '{pageIndex: ' + pageIndex + '}',
//                contentType: "application/json; charset=utf-8",
//                dataType: "json",
//                success: function (data) {
//                    var result = data.d;
//                    for (var i = 0; i < result.length; i++) {
//                        $("#getnotifications").append(
//+'<table border="0">'
//+ '<tr>'
//+ '<td>'
//+ '<div class="alert alert-info alert-dismissable">'
//+ '<h4>' + result[i].DateTime + '</h4>'
//+ '<p>' + result[i].Message + '</p>'
//+ '</div>'
//+ '</td>'
//+ '</tr>'
//+ '</table>');
//                    }

//                },
//                error: function (data) {
//                    var r = data.responseText;
//                    var errorMessage = r.Message;
//                    alert(errorMessage);
//                }
//            });
//        }
//    }

</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<section id="contact">
      <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">Notifications</h2>
            </div>
          </div>
       </div>
       <div class="row" >
<%--<div id="getnotifications">
</div>--%>

<asp:Repeater ID="rptnotification" runat="server">
<ItemTemplate>
<table border="0">
<tr>
<td>

<div class="alert alert-info alert-dismissable">
<h4><asp:Label ID="Label2" runat="server" Text='<%# Bind("DateTime") %>'></asp:Label></h4>
<p><asp:Label ID="Label1" runat="server" Text='<%# Bind("Message") %>'></asp:Label></p>
</div>
</td>
</tr>
</table>
</ItemTemplate>
</asp:Repeater>
        </div>
      </div>
    </section>
</asp:Content>

