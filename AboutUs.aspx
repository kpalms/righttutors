﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AboutUs.aspx.cs" Inherits="AboutUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">

li{
  margin: 10px 0;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<section id="contact">
      <div class="container">
       <div class="row">
       
       </div>
        </div>
    </section>

<%--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--%>


<!-- Services -->

<div class="content">
	<div class="container theme">



    		<div class="row vspace30">
			<div class="col-md-8">

            <div class="col-lg-10">
              <div class="single_footer_widget">
                <h3><asp:Label ID="Label1" runat="server"></asp:Label></h3>
                <ul class="footer_widget_nav">
                  <li class="aboutus">RIGHT TUTORS is one of the leading consultant for tutors in primary education(kindergarten to 12 th grade) and we help you to get the most relevent tutors for your children.</li>
                  <li class="aboutus">We have the best tutors for kindergarten to 12th grade(all borads,all subjects).</li>
                  <li class="aboutus">Foundation is most important when it comes to career.We ensure that every child gets the "Right Tutor" to help and guide him/her in building up the strongest foundation of there career.</li>
                  <li class="aboutus">We just Don`t help you to get a tutor but we make sure you get the "Right Tutor".</li>
                  <li class="aboutus">Easy Transaction of Fees between Student And Tutor.</li>
                  <li class="aboutus">Easy Refund of fees in case Tutor failed to keep up his/her commitment.</li>
                 </ul>
              </div>
            </div>
                </p>
			</div>

			<div class="col-md-4">
				<img class="img-responsive"  src="img/about_us.png" alt=""/>
			</div>
		</div>




		<div class="row vspace30">
			<div class="col-md-4">
				<img class="img-responsive" src="img/Vision.png" alt=""/>
			</div>

			<div class="col-md-8">
				<h2>Vision:-</h2>
				<li class="aboutus">To Be Amongst the top 3 Education consultatnt instituion in world.</li>
			</div>
		</div>

		<div class="row vspace30">
			<div class="col-md-8">
				<h2>Mission:-</h2>
				<span style="font-size:15px;color:Blue;"><b>work And will Be working to provide Unique Tutor Experience based on:-</b></span>
                
            <div class="col-lg-10">
              <div class="single_footer_widget">
                <h3><asp:Label ID="lblstudent" runat="server"></asp:Label></h3>
                <ul class="footer_widget_nav">
                  <li class="aboutus">Matching up with exact Student/Tutor requirement.</li>
                   <li class="aboutus">Win -Win parternership with both Students And Tutors.</li>
                    <li class="aboutus">Quality Monitoring of both Student And Tutor Progress.</li>
                     <li class="aboutus">Moving every stone possible to help a student become a TQP(Total Quality Person).</li>
                </ul>
              </div>
            </div>


                </p>
			</div>

			<div class="col-md-4">
				<img class="img-responsive"  src="img/mission.png" alt=""/>
			</div>
		</div>


	</div>
</div>
<!-- /Services -->



<section id="contact">
      <div class="container">
       <div class="row">

       <asp:Label ID="lblaboutus" runat="server"></asp:Label>

       </div>
        </div>
    </section>



</asp:Content>

