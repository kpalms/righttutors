﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class ChangePassword : System.Web.UI.Page
{
    BL_Students bl_student = new BL_Students();
    BL_Teachers bl_teacher = new BL_Teachers();
    Common common = new Common();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["StudentsId"] == null && Session["TeachersId"] == null)
        {
            Response.Redirect("Default.aspx");
        }
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (Session["LoginType"].ToString() == "Teacher")
        {
            string str = "select Password from Teachers where TeachersId='" + Session["TeachersId"].ToString() + "'";
            string pass = common.ReturnOneValue(str);

            if (pass != "0")
            {
                if (pass == txtpassword.Text)
                {
                    bl_student.TeachersId = Convert.ToInt32(Session["TeachersId"]);
                    bl_student.StudentsId = 0;
                    bl_student.Mode = "Teacher";
                    bl_student.Password = txtNewPassword.Text;
                    bl_student.ChangePassword(bl_student);
                    lblmsg.Visible = true;
                    lblmsg.Text = "Password Change Sussfully !";
                    lblmsg.ForeColor = System.Drawing.Color.Green;

                    EmailSms email = new EmailSms();
                    string body = "<table><tr><td>User Name :</td><td>'" + Session["TEmail"].ToString() + "'</td></tr><tr><td>Password :</td><td>'" + txtNewPassword.Text + "'</td></tr></table>";
                    //email.SendMail("Change Password Sussfully", body, Session["TEmail"].ToString());

                    txtpassword.Text = "";
                    txtNewPassword.Text = "";
                    txtReTypePassword.Text = "";
                    this.ModalPopupExtender1.Show();
                }
                else
                {
                    lblmsg.Visible = true;
                    lblmsg.Text = "Existing Password is Wrong !";
                    lblmsg.ForeColor = System.Drawing.Color.Red;
                }
            }

        }
        else
        {

            string str = "select Password from Students where StudentsId='" + Session["StudentsId"].ToString() + "'";
            string pass = common.ReturnOneValue(str);

            if (pass != "0")
            {
                if (pass == txtpassword.Text)
                {
                    bl_student.StudentsId = Convert.ToInt32(Session["StudentsId"]);
                    bl_student.Mode = "Student";
                    bl_student.TeachersId = 0;
                    bl_student.Password = txtNewPassword.Text;
                    bl_student.ChangePassword(bl_student);
                    lblmsg.Visible = true;
                    lblmsg.Text = "Password Change Sussfully !";
                    lblmsg.ForeColor = System.Drawing.Color.Green;

                    EmailSms email = new EmailSms();
                    string body = "<table><tr><td>User Name :</td><td>'" + Session["SEmail"].ToString() + "'</td></tr><tr><td>Password :</td><td>'" + txtNewPassword.Text + "'</td></tr></table>";
                    //email.SendMail("Change Password Sussfully", body, Session["SEmail"].ToString());
                
                    txtpassword.Text = "";
                     txtNewPassword.Text = "";
                     txtReTypePassword.Text = "";
                     this.ModalPopupExtender1.Show();
                }
                else
                {
                    lblmsg.Visible = true;
                    lblmsg.Text = "Existing Password is Wrong !";
                    lblmsg.ForeColor = System.Drawing.Color.Red;
                }
            }

        }
    }
}