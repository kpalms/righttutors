﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ForgotPassword.aspx.cs" Inherits="ForgotPassword" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">
         .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=90);
        opacity: 0.8;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        border-width: 3px;
        border-style: solid;
        border-color: black;
        padding-top: 10px;
        padding-left: 10px;
        width: 300px;
        height: 140px;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



    <!--=========== BEGIN CONTACT SECTION ================-->
    <section id="contact">
      <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">Forgot Password</h2>
              <span></span> 
            </div>
          </div>
       </div>
       <div class="row" >



<div class="container" id="parent">

  <div class="row">
    <div class="col-lg-12">
      <div class="row ">

        <div class="col-lg-12" id="child" >
 <div align="center" >


 <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
 <ContentTemplate>
<asp:Label ID="lblmsg" runat="server" Visible="false"></asp:Label>

<asp:RadioButton ID="rdostudent" Text="Student" GroupName="login" runat="server" />
<asp:RadioButton ID="rdoteacher" Text="Teacher" GroupName="login" runat="server" /> 
<br />              

 <asp:TextBox ID="txtemailid" runat="server" class="wp-form-control wpcf7-text" placeholder="Enter Email Id"></asp:TextBox>
 <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please Enter Email Id" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtemailid"></asp:RequiredFieldValidator>
 <asp:RegularExpressionValidator ID="EmailFormat" runat="server" Text="Please enter a valid Email"  ValidationGroup="new" ToolTip="Please enter a valid Email" ControlToValidate="txtemailid" ValidationExpression="(\w)+@(\w)+.com(.(\w)+)*" ForeColor="Red"  />
 <br />
 <asp:Button ID="btnlogin" runat="server" ValidationGroup="new" Text="Send" onclick="btnlogin_Click" class="wpcf7-submit"/>

  </ContentTemplate>
</asp:UpdatePanel>

<asp:HiddenField ID="hfHidden" runat="server" />
    <asp:Panel ID="Panel1" CssClass="modalPopup" align="center" style = "display:none" runat="server">
    <table align="center">
    <tr>
   <b> Password has been sent to your email ! </b>
    </tr>
    <br />
    <br />
    <br />
    <tr>
    <asp:Button ID="Button2" class="LoginButton" runat="server" Text="Ok"/>
    </tr>
    </table>
    
    </asp:Panel>
   
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" BackgroundCssClass="modalBackground"
    CancelControlID="Button2" PopupControlID="Panel1" TargetControlID="hfHidden" runat="server">
    </ajaxToolkit:ModalPopupExtender>
</div>
        </div>
      </div>
    </div>
  </div>



       </div>
      </div>
    </section>
    <!--=========== END CONTACT SECTION ================-->

 
 
</asp:Content>

