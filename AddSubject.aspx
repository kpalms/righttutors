﻿<%@ Page Title="" Language="C#"  MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AddSubject.aspx.cs" Inherits="SelectedSubject" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script src="JsSlider/validation.js" type="text/javascript"></script>
<style type="text/css">
         .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=90);
        opacity: 0.8;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        border-width: 3px;
        border-style: solid;
        border-color: black;
        padding-top: 10px;
        padding-left: 10px;
        width: 300px;
        height: 140px;
    }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
<ContentTemplate>

<section id="contact">
      <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">Add Subject</h2>
            </div>
          </div>
       </div>
       <div class="row" >

<div class="container" id="parent">

  <div class="row">
    <div class="col-lg-12">
      <div class="row ">

        <div class="col-lg-12" id="child" >
        


<div align="center" >

    <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label>
<asp:DropDownList ID="ddlBoard" runat="server" class="wp-form-control wpcf7-text" 
        AutoPostBack="True" onselectedindexchanged="ddlBoard_SelectedIndexChanged">
</asp:DropDownList>
<asp:RequiredFieldValidator ControlToValidate="ddlBoard" ID="RequiredFieldValidator1"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a Board"
InitialValue="0" runat="server"  Display="Dynamic">
</asp:RequiredFieldValidator>
<br />


<asp:DropDownList ID="ddlClass" runat="server" 
AutoPostBack="True" onselectedindexchanged="ddlClass_SelectedIndexChanged" class="wp-form-control wpcf7-text">
</asp:DropDownList>
<asp:RequiredFieldValidator ControlToValidate="ddlClass" ID="RequiredFieldValidator2"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a Class"
InitialValue="0" runat="server"  Display="Dynamic">
</asp:RequiredFieldValidator>
<br />


<asp:DropDownList ID="ddlSubject" runat="server" class="wp-form-control wpcf7-text">
</asp:DropDownList>
<asp:RequiredFieldValidator ControlToValidate="ddlSubject" ID="RequiredFieldValidator3"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a Subject"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
<br />

<asp:Button ID="btnsubmit" runat="server" Text="Submit" class="wpcf7-submit" ValidationGroup="new"
onclick="btnsubmit_Click" />

    <asp:Button ID="btnupdate" runat="server" ValidationGroup="new" class="wpcf7-updated" Text="Update" 
        onclick="btnupdate_Click" />
    <asp:Button ID="btnclear" runat="server" class="wpcf7-clear" Text="Clear" 
        onclick="btnclear_Click" />
<br />
    <asp:GridView ID="gvselectedsubject" class="table table-hover" runat="server" AutoGenerateColumns="False" 
        onrowcommand="gvselectedsubject_RowCommand" EmptyDataText="No records Found"
        onselectedindexchanged="gvselectedsubject_SelectedIndexChanged" BorderStyle="None" GridLines="None" >
        <Columns>
            <asp:TemplateField HeaderText="SelectedSubjectId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("TeacherSubjectId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblTeacherSubjectId" runat="server" Text='<%# Bind("TeacherSubjectId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="BoardId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("BoardId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblBoardId" runat="server" Text='<%# Bind("BoardId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Board">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Board") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblBoard" runat="server" Text='<%# Bind("Board") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ClassId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("ClassId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblClassId" runat="server" Text='<%# Bind("ClassId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Class">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Class") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblClass" runat="server" Text='<%# Bind("Class") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="SubjectId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("SubjectId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblSubjectId" runat="server" Text='<%# Bind("SubjectId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Subject">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("Subject") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("Subject") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkedit" runat="server" CommandName="EditRow" class="btn btn-info" CommandArgument='<%# Bind("TeacherSubjectId") %>'><i class="glyphicon glyphicon-edit icon-white"></i>Edit</asp:LinkButton>
                    <asp:LinkButton ID="lnkdelete" CommandName="DeleteRow" class="btn btn-danger" CommandArgument='<%# Bind("TeacherSubjectId") %>' OnClientClick="return conformbox();" runat="server"> <i class="glyphicon glyphicon-trash icon-white"></i>Delete</asp:LinkButton>
                    <asp:LinkButton ID="lnkaction" CommandName="Action" class="btn btn-success" CommandArgument='<%# Bind("TeacherSubjectId") %>' Text='<%# Bind("Active") %>' runat="server"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

        </div>


        </div>
      </div>
    </div>
  </div>



       </div>
      </div>
    </section>

    <asp:HiddenField ID="hfHidden" runat="server" />
    <asp:Panel ID="Panel1" CssClass="modalPopup" align="center" style = "display:none" runat="server">
    <table align="center">
    <tr>
   <b>
       <asp:Label ID="lblmsgdata" runat="server" Text="Label"></asp:Label></b>
    </tr>
    <br />
    <br />
    <br />
    <tr>
    <td>
    <asp:Button ID="Button2" class="LoginButton" runat="server" Text="Ok"/>
    </td>
    </tr>
    </table>
    
    </asp:Panel>
   
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" BackgroundCssClass="modalBackground"
    CancelControlID="Button2" PopupControlID="Panel1" TargetControlID="hfHidden" runat="server">
    </ajaxToolkit:ModalPopupExtender>


</ContentTemplate>
</asp:UpdatePanel>
 </asp:Content>  


