﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Web.Script.Services;
using System.IO;
public partial class TeacherRegistration : System.Web.UI.Page
{
    Common common = new Common();
    BL_Teachers bl_teachers = new BL_Teachers();
    BL_Subject bl_subject = new BL_Subject();
    protected void Page_Load(object sender, EventArgs e)
    {
                if (!IsPostBack)
                {
                    common.BindCountry(ddlCountry);
                    common.SelectStateCountryId(ddlState, 0);
                    common.SelectCityStateId(ddlCity, 0);
                    common.SelectAreaCityId(ddlArea, 0);
                    common.BindBoard(ddlBoard);
                    common.BindClass(ddlClass);
                    TeachingExperience();
                }

                if (Session["StudentsId"] != null)
                {
                    Response.Redirect("StudentProfile.aspx");
                }

                if (Session["TeachersId"] != null)
                {
                    Response.Redirect("TeacherProfile.aspx");
                }
    }

    private void SetFill()
    {
        bl_teachers.Name = txtName.Text;
        bl_teachers.MiddleName = txtMiddleName.Text;
        bl_teachers.LastName = txtLastName.Text;
        bl_teachers.BirthYear = txtDob.Text;
        bl_teachers.TagLine = txttagline.Text;
        bl_teachers.Education = txtEducation.Text;
        bl_teachers.Experience = Convert.ToInt32(ddlteex.SelectedItem.Value);
        bl_teachers.CountryId = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        bl_teachers.StateId = Convert.ToInt32(ddlState.SelectedItem.Value);
        bl_teachers.CityId = Convert.ToInt32(ddlCity.SelectedItem.Value);
        bl_teachers.AreaId = Convert.ToInt32(ddlArea.SelectedItem.Value);
        bl_teachers.Address = txtAddress.Text;
        if (txtpincode.Text != "")
        {
            bl_teachers.PinCode = Convert.ToInt32(txtpincode.Text);
        }
        bl_teachers.Gender = Convert.ToInt32(ddlGender.SelectedItem.Value);
        bl_teachers.MobileNo = txtMobileNo.Text;
        bl_teachers.Email = txtEmail.Text;
        bl_teachers.AboutMe = txtaboutme.Text;
        bl_teachers.Password = txtPassword.Text;
        bl_teachers.BoardId = Convert.ToInt32(ddlBoard.SelectedItem.Value);
        bl_teachers.ClassId = Convert.ToInt32(ddlClass.SelectedItem.Value);
        bl_teachers.DateTime = common.GetDate();
    }

    private void Clear()
    {
        txtName.Text = "";
        txtMiddleName.Text = "";
        txtLastName.Text = "";
        txtDob.Text = "";
        txtEducation.Text="";
        txttagline.Text="";
        txtAddress.Text = "";
        txtaboutme.Text="";
        txtpincode.Text = "";
        txtMobileNo.Text = "";
        txtEmail.Text = "";
        txtPassword.Text = "";
        txtReTypePassword.Text = "";
        ddlArea.SelectedIndex = 0;
        ddlCity.SelectedIndex = 0;
        ddlCountry.SelectedIndex = 0;
        ddlState.SelectedIndex = 0;
        ddlteex.SelectedIndex = 0;
        ddlGender.SelectedIndex = 0;
        ddlBoard.SelectedIndex = 0;
        ddlClass.SelectedIndex = 0;
        chksubject.Items.Clear();
        chksubject.Visible = false;
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (chksubject.SelectedIndex != -1)
        {
            lblsubject.Visible = false;

            string str = "select count(*) from Teachers where Email= '"+txtEmail.Text+"'";
            string count = common.ReturnOneValue(str);

            //if (count == "0")
            //{

            lblemail.Visible = false;
        SetFill();
        bl_teachers.TeachersId = 0;
        bl_teachers.Mode = "Insert";

        string SubjectList = "";
        for (int i = 0; i <= chksubject.Items.Count - 1; i++)
        {
            if (chksubject.Items[i].Selected)
            {
                if (SubjectList == "")
                {
                    SubjectList = chksubject.Items[i].Value;
                }
                else
                {
                    SubjectList += "," + chksubject.Items[i].Value;

                }
            }
        }

        bl_teachers.SubjectId = SubjectList + ","; 

        if (fileimage.HasFile)
        {
            string ext = Path.GetExtension(fileimage.FileName);
            string filename = common.GetImageName() +"" + ext;
            fileimage.PostedFile.SaveAs(Server.MapPath("rt/Images/Teacher/") + filename);
            bl_teachers.ImageName = filename;
        }
        else
        {
            bl_teachers.ImageName = "NoImage.png";
        }

        int tid = bl_teachers.AddTeachers(bl_teachers);


        EmailSms email = new EmailSms();
        string url = "<table><tr><td>www.righttutors.in/TeacherProfile.aspx?SID=" + tid + "</td></tr></table>";
        string body = "Wel Come to Right Tutors<br/>" + url;
       // email.SendMail("Right Tutors", body, "support@righttutors.in," + txtEmail.Text);
        Clear();
        this.ModalPopupExtender1.Show();
        //}

        //else
        //{
        //    lblemail.Text = "Already Existing Email !";
        //}



        }
        else
        {
            lblsubject.Text = "Please select at least one Subject.";
        }



    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        int cid = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        common.SelectStateCountryId(ddlState, cid);
        common.SelectCityStateId(ddlCity, 0);
        common.SelectAreaCityId(ddlArea, 0);
    }

    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
        int sid = Convert.ToInt32(ddlState.SelectedItem.Value);
        common.SelectCityStateId(ddlCity, sid);
        common.SelectAreaCityId(ddlArea, 0);
    }

    protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        int ciid = Convert.ToInt32(ddlCity.SelectedItem.Value);
        common.SelectAreaCityId(ddlArea, ciid);
    }

    private void TeachingExperience()
    {
        ddlteex.Items.Clear();

        for (int i = 0; i <= 20; i++)
        {
            ddlteex.Items.Add(i.ToString());
        }
        ddlteex.DataBind();
        ddlteex.Items.Insert(0, new ListItem("--Select Teaching Experience--", "0"));
    }

    protected void ddlClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        bl_subject.BoardId = Convert.ToInt32(ddlBoard.SelectedItem.Value);
        bl_subject.ClassId = Convert.ToInt32(ddlClass.SelectedItem.Value);
        DataTable dt = bl_subject.SelectSubjectByBoardIdClassId(bl_subject);
        chksubject.DataSource = dt;
        chksubject.DataValueField = "SubjectId";
        chksubject.DataTextField = "Subject";
        chksubject.DataBind();
    }
    protected void ddlBoard_SelectedIndexChanged(object sender, EventArgs e)
    {
        bl_subject.BoardId = Convert.ToInt32(ddlBoard.SelectedItem.Value);
        bl_subject.ClassId = Convert.ToInt32(ddlClass.SelectedItem.Value);
        DataTable dt = bl_subject.SelectSubjectByBoardIdClassId(bl_subject);
        chksubject.DataSource = dt;
        chksubject.DataValueField = "SubjectId";
        chksubject.DataTextField = "Subject";
        chksubject.DataBind();
    }

    protected void btnok_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
    
}