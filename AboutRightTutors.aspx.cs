﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class AboutRightTutors : System.Web.UI.Page
{
    BL_Testimonial bl_testimonial = new BL_Testimonial();
    Common common = new Common();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["StudentsId"] == null && Session["TeachersId"] == null)
        {
            Response.Redirect("Default.aspx");
        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (Session["StudentsId"] != null)
        {
            bl_testimonial.StudentId = Convert.ToInt32(Session["StudentsId"].ToString());
            bl_testimonial.TeachersId = 0;
        }
        else
        {
            bl_testimonial.TeachersId = Convert.ToInt32(Session["TeachersId"].ToString());
            bl_testimonial.StudentId = 0;
        }
        bl_testimonial.Writes = txtWrites.Text;
        bl_testimonial.Role = "RT";
        bl_testimonial.DateTime = Convert.ToDateTime(common.GetDate());
        bl_testimonial.AddTestimonial(bl_testimonial);
        txtWrites.Text = "";
        pnlmsg.Visible = false;
        lblmsg.Visible = true;

    }
}