﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using System.Data;
using System.Data.SqlClient;

public partial class Default2 : System.Web.UI.Page
{

    BL_Students bl_student = new BL_Students();
    BL_Teachers bl_teacher = new BL_Teachers();
    BL_Advertisement bl_advertisement = new BL_Advertisement();
    Common common = new Common();
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (Session["StudentsId"] != null)
        {
            lnklogout.Visible = true;
            lnkhome.Visible = false;
            lnkstudentprofile.Visible = true;
            lnkteacherprofile.Visible = false;
            Response.Redirect("StudentProfile.aspx?pg=sp");
        }
        else if (Session["TeachersId"] != null)
        {
            lnklogout.Visible = true;
            lnkhome.Visible = false;
            lnkstudentprofile.Visible = false;
            lnkteacherprofile.Visible = true;
            Response.Redirect("TeacherProfile.aspx?pg=tp");
        }
        else
        {
            lnklogout.Visible = false;
            lnkhome.Visible = true;
            lnkstudentprofile.Visible = false;
            lnkteacherprofile.Visible = false;
            if (!IsPostBack)
            {
                rdostudent.Checked = true;
                BindAdvertisement();
            }
            lblstudent.Text = common.GetTotalStudent();
            lbltutors.Text = common.GetTotalTutor();
        }

      
    }

    protected void btnlogin_Click(object sender, EventArgs e)
    {
        if (rdostudent.Checked == false && rdoteacher.Checked == false)
        {
            lblmsg.Text = "Please Select Log In Type !";
        }
        else
        {
            if (rdostudent.Checked == true)
            {
                bl_student.Email = txtemailid.Text;
                bl_student.Password = txtpassword.Text;
                DataTable dt = bl_student.LogInStudent(bl_student);
                if (dt.Rows.Count > 0)
                {
                    string sid = dt.Rows[0]["StudentsId"].ToString();
                    string active = dt.Rows[0]["Active"].ToString();
                    string verified = dt.Rows[0]["Verified"].ToString();

                    if (sid != null && active == "True" && verified == "True")
                    {
                        Session["StudentsId"] = sid;
                        Session["SEmail"] = txtemailid.Text;
                        Session["LoginType"] = "Student";
                        Response.Redirect("StudentProfile.aspx?pg=sp");
                    }
                    else if (sid != null && active == "True" && verified == "False")
                    {
                        lblmsg.Text = "Your Account is Unverified !";
                    }
                    else
                    {
                        lblmsg.Text = "Your Account is Deactived !";
                    }
                }
                else
                {
                    lblmsg.Text = "Invalid UserName or Password !";
                }
            }
            else
            {
                if (rdoteacher.Checked == true)
                {
                    bl_teacher.Email = txtemailid.Text;
                    bl_teacher.Password = txtpassword.Text;
                    DataTable dt = bl_teacher.LogInTeacher(bl_teacher);
                    if (dt.Rows.Count > 0)
                    {
                        string tid = dt.Rows[0]["TeachersId"].ToString();
                        string active = dt.Rows[0]["Active"].ToString();
                        string verified = dt.Rows[0]["Verified"].ToString();

                        if (tid != null && active == "True" && verified == "True")
                        {
                            Session["TeachersId"] = tid;
                            Session["TEmail"] = txtemailid.Text;
                            Session["LoginType"] = "Teacher";
                            Response.Redirect("TeacherProfile.aspx?pg=tp");
                        }
                        else if (tid != null && active == "True" && verified == "False")
                        {
                            lblmsg.Text = "Your Account is Unverified !";
                        }
                        else
                        {
                            lblmsg.Text = "Your Account is Deactived !";
                        }
                       
                    }
                    else
                    {
                        lblmsg.Text = "Invalid UserName or Password !";
                    }
                }
            }
        }
    }


    private void BindAdvertisement()
    {
        DataTable dt = bl_advertisement.SelectAdvertisement();
        rptadvertimsement.DataSource = dt;
        rptadvertimsement.DataBind();
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        txtemailid.Text = "";
        txtpassword.Text = "";
    }

    protected void lnklogout_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Session.Clear();
        Response.Redirect("Default.aspx");
    }
}