﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class StudentSubject : System.Web.UI.Page
{
    Common common = new Common();
    BL_StudentSubject bl_studentsubject = new BL_StudentSubject();
    BL_Subject bl_subject = new BL_Subject();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["StudentsId"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                SelectedBind();
                common.BindBoard(ddlBoard);
                common.BindClass(ddlClass);
                common.BindSubjects(ddlSubject, 0, 0);
                btnupdate.Enabled = false;
                btnupdate.CssClass = "wpcf7-updated";
            }
        }
    }


    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        bl_studentsubject.SubjectId = Convert.ToInt32(ddlSubject.SelectedItem.Value);
        bl_studentsubject.StudentsId = Convert.ToInt32(Session["StudentsId"].ToString());
        DataTable dt = bl_studentsubject.SelectStudentSubjectById(bl_studentsubject);

        if (dt.Rows.Count == 0)
        {
            bl_studentsubject.StudentSubjectId = 0;
            bl_studentsubject.Mode = "Insert";
            Setfill();
            bl_studentsubject.AddStudentSubject(bl_studentsubject);
            Clear();
            lblmsgdata.Text = "Subject Saved successfully !";
            this.ModalPopupExtender1.Show();
            SelectedBind();
            btnupdate.Enabled = false;
            lblmsg.Visible = false;
        }
        else
        {
            lblmsg.Text = "Already existing this Subject !";
        }
    }

    protected void ddlClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        int boardid = Convert.ToInt32(ddlBoard.SelectedItem.Value);
        int classid = Convert.ToInt32(ddlClass.SelectedItem.Value);
        common.BindSubjects(ddlSubject, boardid, classid);
    }
    protected void ddlBoard_SelectedIndexChanged(object sender, EventArgs e)
    {
        int boardid = Convert.ToInt32(ddlBoard.SelectedItem.Value);
        int classid = Convert.ToInt32(ddlClass.SelectedItem.Value);
        common.BindSubjects(ddlSubject, boardid, classid);
    }


    private void SelectedBind()
    {
        bl_studentsubject.StudentsId = Convert.ToInt32(Session["StudentsId"].ToString());
        DataTable dt = bl_studentsubject.SelectStudentSubject(bl_studentsubject);
        gvStudentSubject.DataSource = dt;
        gvStudentSubject.DataBind();
    }

    private void Clear()
    {
        ddlSubject.SelectedIndex = 0;
        ddlBoard.SelectedIndex = 0;
        ddlClass.SelectedIndex = 0;
        lblmsg.Visible = false;
    }

    private void Setfill()
    {
        bl_studentsubject.BoardId = Convert.ToInt32(ddlBoard.SelectedItem.Value);
        bl_studentsubject.ClassId = Convert.ToInt32(ddlClass.SelectedItem.Value);
        bl_studentsubject.SubjectId = Convert.ToInt32(ddlSubject.SelectedItem.Value);
        bl_studentsubject.StudentsId = Convert.ToInt32(Session["StudentsId"].ToString());
        bl_studentsubject.Active = 0;
    }

    protected void gvStudentSubject_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblBoardId = (Label)row.FindControl("lblBoardId");
            Label lblClassId = (Label)row.FindControl("lblClassId");
            Label lblSubjectId = (Label)row.FindControl("lblSubjectId");
            LinkButton lnkaction = (LinkButton)row.FindControl("lnkaction");

            if (e.CommandName == "EditRow")
            {
                id = Convert.ToInt32(e.CommandArgument.ToString());
                ddlBoard.Text = lblBoardId.Text;
                ddlClass.Text = lblClassId.Text;
                common.BindSubjects(ddlSubject, Convert.ToInt32(lblBoardId.Text), Convert.ToInt32(lblClassId.Text));
                ddlSubject.Text = lblSubjectId.Text;
                btnupdate.Enabled = true;
                btnsubmit.Enabled = false;
                btnsubmit.CssClass = "wpcf7-submit";

            }
            if (e.CommandName == "DeleteRow")
            {
                bl_studentsubject.StudentSubjectId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_studentsubject.BoardId = 0;
                bl_studentsubject.SubjectId = 0;
                bl_studentsubject.ClassId = 0;
                bl_studentsubject.Active = 0;
                bl_studentsubject.Mode = "Delete";
                bl_studentsubject.AddStudentSubject(bl_studentsubject);
                SelectedBind();
                Clear();
                btnupdate.Enabled = false;
                btnsubmit.Enabled = true;
            }
            //if (e.CommandName == "Action")
            //{
            //    bl_studentsubject.StudentSubjectId = Convert.ToInt32(e.CommandArgument.ToString());
            //    bl_studentsubject.BoardId = 0;
            //    bl_studentsubject.SubjectId = 0;
            //    bl_studentsubject.ClassId = 0;
            //    bl_studentsubject.Mode = "Active";

            //    if (lnkaction.Text == "Active")
            //    {
            //        bl_studentsubject.Active = 0;

            //    }
            //    else
            //    {
            //        bl_studentsubject.Active = 1;
            //    }
            //    bl_studentsubject.AddStudentSubject(bl_studentsubject);
            //    btnupdate.Enabled = false;
            //    btnsubmit.Enabled = true;
            //    SelectedBind();
            //    Clear();
            //}
        }
        catch
        {


        }
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        btnupdate.Enabled = false;
        btnsubmit.Enabled = true;
        ddlBoard.SelectedIndex = 0;
        ddlClass.SelectedIndex = 0;
        Clear();
    }
    protected void gvStudentSubject_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        bl_studentsubject.StudentSubjectId = id;
        bl_studentsubject.Mode = "Update";
        Setfill();
        bl_studentsubject.AddStudentSubject(bl_studentsubject);
        Clear();
        lblmsgdata.Text = "Subject Updated successfully !";
        this.ModalPopupExtender1.Show();
        SelectedBind();
        btnupdate.Enabled = false;
        btnsubmit.Enabled = true;
        lblmsg.Visible = false;
    }
}