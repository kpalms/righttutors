﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class SelectedSubject : System.Web.UI.Page
{
    Common common = new Common();
    BL_SelectedSubject bl_selectedsubject = new BL_SelectedSubject();
    BL_Subject bl_subject = new BL_Subject();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["TeachersId"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                SelectedBind();
                common.BindBoard(ddlBoard);
                common.BindClass(ddlClass);
                common.BindSubjects(ddlSubject, 0, 0);
                btnupdate.Enabled = false;
                btnupdate.CssClass = "wpcf7-updated";
            }
        }
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        bl_selectedsubject.SubjectId = Convert.ToInt32(ddlSubject.SelectedItem.Value);
        bl_selectedsubject.TeachersId = Convert.ToInt32(Session["TeachersId"].ToString());
        DataTable dt = bl_selectedsubject.SelectTeacherSubjectById(bl_selectedsubject);

        if (dt.Rows.Count == 0)
        {
            bl_selectedsubject.TeacherSubjectId = 0;
            bl_selectedsubject.Mode = "Insert";
            Setfill();
            bl_selectedsubject.AddTeacherSubject(bl_selectedsubject);
            Clear();
            SelectedBind();
            lblmsgdata.Text = "Subject Saved successfully !";
            this.ModalPopupExtender1.Show();
            btnupdate.Enabled = false;
        }
        else
        {
            lblmsg.Text = "Already existing this Subject !";
        }
    }

    protected void ddlClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        int boardid = Convert.ToInt32(ddlBoard.SelectedItem.Value);
        int classid = Convert.ToInt32(ddlClass.SelectedItem.Value);
        common.BindSubjects(ddlSubject, boardid, classid);
    }

    protected void ddlBoard_SelectedIndexChanged(object sender, EventArgs e)
    {
        int boardid = Convert.ToInt32(ddlBoard.SelectedItem.Value);
        int classid = Convert.ToInt32(ddlClass.SelectedItem.Value);
        common.BindSubjects(ddlSubject, boardid, classid);
    }

    private void SelectedBind()
    {
        bl_selectedsubject.TeachersId = Convert.ToInt32(Session["TeachersId"].ToString());
        DataTable dt = bl_selectedsubject.SelectTeacherSubject(bl_selectedsubject);
        gvselectedsubject.DataSource = dt;
        gvselectedsubject.DataBind();
    }

    private void Clear()
    {
        ddlSubject.SelectedIndex = 0;
        ddlClass.SelectedIndex = 0;
        ddlBoard.SelectedIndex = 0;
    }

    private void Setfill()
    {
        bl_selectedsubject.BoardId = Convert.ToInt32(ddlBoard.SelectedItem.Value);
        bl_selectedsubject.ClassId = Convert.ToInt32(ddlClass.SelectedItem.Value);
        bl_selectedsubject.SubjectId = Convert.ToInt32(ddlSubject.SelectedItem.Value);
        bl_selectedsubject.TeachersId = Convert.ToInt32(Session["TeachersId"].ToString());
        bl_selectedsubject.Active = 0;
    }

    protected void gvselectedsubject_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblBoardId = (Label)row.FindControl("lblBoardId");
            Label lblClassId = (Label)row.FindControl("lblClassId");
            Label lblSubjectId = (Label)row.FindControl("lblSubjectId");
            LinkButton lnkaction = (LinkButton)row.FindControl("lnkaction");

            if (e.CommandName == "EditRow")
            {
                id = Convert.ToInt32(e.CommandArgument.ToString());
                ddlBoard.Text = lblBoardId.Text;
                ddlClass.Text = lblClassId.Text;
                common.BindSubjects(ddlSubject, Convert.ToInt32(lblBoardId.Text), Convert.ToInt32(lblClassId.Text));
                ddlSubject.Text = lblSubjectId.Text;
                btnupdate.Enabled = true;
                btnsubmit.Enabled = false;
                btnsubmit.CssClass = "wpcf7-submit";

            }
            if (e.CommandName == "DeleteRow")
            {
                bl_selectedsubject.TeacherSubjectId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_selectedsubject.BoardId = 0;
                bl_selectedsubject.SubjectId = 0;
                bl_selectedsubject.ClassId = 0;
                bl_selectedsubject.Active = 0;
                bl_selectedsubject.Mode = "Delete";
                bl_selectedsubject.AddTeacherSubject(bl_selectedsubject);
                SelectedBind();
                Clear();
                ddlClass.SelectedIndex = 0;
                ddlBoard.SelectedIndex = 0;
                btnupdate.Enabled = false;
                btnsubmit.Enabled = true;
            }
            if (e.CommandName == "Action")
            {
                bl_selectedsubject.TeacherSubjectId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_selectedsubject.BoardId = 0;
                bl_selectedsubject.SubjectId = 0;
                bl_selectedsubject.ClassId = 0;
                bl_selectedsubject.Mode = "Active";

                if (lnkaction.Text == "Active")
                {
                    bl_selectedsubject.Active = 0;
                    lblmsgdata.Text = "Subject Is Actived !";
                    this.ModalPopupExtender1.Show();

                }
                else
                {
                    bl_selectedsubject.Active = 1;
                    lblmsgdata.Text = "Subject Is Deactived !";
                    this.ModalPopupExtender1.Show();
                }
                bl_selectedsubject.AddTeacherSubject(bl_selectedsubject);
                btnupdate.Enabled = false;
                btnsubmit.Enabled = true;
                SelectedBind();
                Clear();
            }
        }
        catch
        {
        }
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        btnupdate.Enabled = false;
        btnsubmit.Enabled = true;
        ddlBoard.SelectedIndex = 0;
        ddlClass.SelectedIndex = 0;
        Clear();
    }
    protected void gvselectedsubject_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        bl_selectedsubject.TeacherSubjectId = id;
        bl_selectedsubject.Mode = "Update";
        Setfill();
        bl_selectedsubject.AddTeacherSubject(bl_selectedsubject);
        Clear();
        lblmsgdata.Text = "Subject Updated successfully !";
        this.ModalPopupExtender1.Show();
        SelectedBind();
        btnupdate.Enabled = false;
        btnsubmit.Enabled = true;
    }
}