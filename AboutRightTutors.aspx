﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeFile="AboutRightTutors.aspx.cs" Inherits="AboutRightTutors" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
        <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/superslides.css"/>
    <link href="css/slick.css" rel="stylesheet"/> 
    <link rel='stylesheet prefetch' href="css/https___cdn.rawgit.com_pguso_jquery-plugin-circliful_master_css_jquery.circliful.css is not available.css"/>  
    <link rel="stylesheet" href="css/animate.css"/> 
    <link rel="stylesheet" href="css/queryLoader.css" type="text/css" />
    <link type="text/css" media="all" rel="stylesheet" href="css/jquery.tosrus.all.css" />    
    <link id="switcher" href="css/themes/default-theme.css" rel="stylesheet"/>
    <link href="style.css" rel="stylesheet"/>
    <link href="css/css.css"' rel='stylesheet' type='text/css'/>   
    <link href="css/css1.css" rel='stylesheet' type='text/css'/>  
    <link rel="stylesheet" href="css/MyStyleSheet.css" type="text/css" />
</head>
<body>
<form id="form1" runat="server">


        <div class="container">
          <div class="row">
            <div class="col-ld-3  col-md-3 col-sm-3">
              
              <div class="single_footer_widget2">
                <h1>  <asp:Label ID="lblmsg" runat="server" Text="Thank You" ForeColor="Red" Visible="false"></asp:Label></h1>
                  <asp:Panel ID="pnlmsg" runat="server">
                
                 <ul class="footer_widget_nav">
                  <li>
                    <asp:TextBox ID="txtWrites" runat="server" TextMode="MultiLine" Columns="30" Rows="5"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Please write something..." ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtWrites"></asp:RequiredFieldValidator>

                  </li>
                   <li>
                   <asp:Button ID="btnsave" runat="server" ValidationGroup="new" Text="Submit" onclick="btnsave_Click" class="LoginButton"/>
                   </li>
                </ul>
                  </asp:Panel>
              </div>
          

            </div>
          </div>
        </div>

</form>
</body>
</html>
