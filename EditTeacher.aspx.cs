﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Web.Script.Services;
using System.IO;
public partial class EditTeacher : System.Web.UI.Page
{
    Common common = new Common();
    BL_Teachers bl_teachers = new BL_Teachers();
    static string imagename;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["TeachersId"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        {
            if (!IsPostBack)
            {
                common.BindCountry(ddlCountry);
                BindTeachers();
                TeachingExperience();
            }
        }
    }

    private void BindTeachers()
    {
        bl_teachers.TeachersId = Convert.ToInt32(Session["TeachersId"]);
        DataSet ds = bl_teachers.SelectTeachersById(bl_teachers);
        if (ds.Tables[0].Rows.Count > 0)
       {
           string img = ds.Tables[0].Rows[0]["ImageName"].ToString();
         imagename = img.Substring(18);
         txtName.Text = ds.Tables[0].Rows[0]["Name"].ToString();
         txtMiddleName.Text = ds.Tables[0].Rows[0]["MiddleName"].ToString();
         txtLastName.Text = ds.Tables[0].Rows[0]["LastName"].ToString();
         txtDob.Text = ds.Tables[0].Rows[0]["BirthYear"].ToString();
         ddlCountry.Text = ds.Tables[0].Rows[0]["CountryId"].ToString();
         txttagline.Text = ds.Tables[0].Rows[0]["TagLine"].ToString();
         txtEducation.Text = ds.Tables[0].Rows[0]["Education"].ToString();
         ddlteex.Text = ds.Tables[0].Rows[0]["Experience"].ToString();
         common.SelectStateCountryId(ddlState, Convert.ToInt32(ds.Tables[0].Rows[0]["CountryId"]));
        ddlState.Text = ds.Tables[0].Rows[0]["StateId"].ToString();
        common.SelectCityStateId(ddlCity, Convert.ToInt32(ds.Tables[0].Rows[0]["StateId"]));
        ddlCity.Text = ds.Tables[0].Rows[0]["CityId"].ToString();
        common.SelectAreaCityId(ddlArea, Convert.ToInt32(ds.Tables[0].Rows[0]["CityId"]));
        ddlArea.Text = ds.Tables[0].Rows[0]["AreaId"].ToString();
        txtAddress.Text = ds.Tables[0].Rows[0]["Address"].ToString();
        txtpincode.Text = ds.Tables[0].Rows[0]["PinCode"].ToString();
        txtaboutme.Text = ds.Tables[0].Rows[0]["AboutMe"].ToString();
        ddlGender.Text = ds.Tables[0].Rows[0]["Gender"].ToString();
        txtMobileNo.Text = ds.Tables[0].Rows[0]["MobileNo"].ToString();
        txtEmail.Text = ds.Tables[0].Rows[0]["Email"].ToString();
        imgprofile.ImageUrl = ds.Tables[0].Rows[0]["ImageName"].ToString();

       }
    }

    private void SetFill()
    {
        bl_teachers.Name = txtName.Text;
        bl_teachers.MiddleName = txtMiddleName.Text;
        bl_teachers.LastName = txtLastName.Text;
        bl_teachers.BirthYear = txtDob.Text;
        bl_teachers.TagLine = txttagline.Text;
        bl_teachers.Education = txtEducation.Text;
        bl_teachers.Experience = Convert.ToInt32(ddlteex.SelectedItem.Value);
        bl_teachers.CountryId = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        bl_teachers.StateId = Convert.ToInt32(ddlState.SelectedItem.Value);
        bl_teachers.CityId = Convert.ToInt32(ddlCity.SelectedItem.Value);
        bl_teachers.AreaId = Convert.ToInt32(ddlArea.SelectedItem.Value);
        bl_teachers.Address = txtAddress.Text;
        bl_teachers.PinCode = Convert.ToInt32(txtpincode.Text);
        bl_teachers.Gender = Convert.ToInt32(ddlGender.SelectedItem.Value);
        bl_teachers.MobileNo = txtMobileNo.Text;
        bl_teachers.Email = txtEmail.Text;
        bl_teachers.AboutMe = txtaboutme.Text;
        bl_teachers.Password = "";
        bl_teachers.DateTime = Convert.ToDateTime(DateTime.Now.ToString());
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        string filename;
            SetFill();
            bl_teachers.TeachersId = Convert.ToInt32(Session["TeachersId"]);
            bl_teachers.Mode = "Update";
            if (fileimage.HasFile)
            {
                string ext = Path.GetExtension(fileimage.FileName);

                if ("NoImage.png" == imagename)
                {
                    filename = common.GetImageName() + "" + ext;
                }
                else
                {
                    filename = common.GetImageName() + "" + ext;
                    string imageFilePath = Server.MapPath("rt/Images/Teacher/" + imagename);
                    File.Delete(imageFilePath);
                }
                fileimage.PostedFile.SaveAs(Server.MapPath("rt/Images/Teacher/") + filename);
                bl_teachers.ImageName = filename;
            }
            else
            {
                bl_teachers.ImageName = imagename;
            }
            bl_teachers.UpdateTeachers(bl_teachers);
            BindTeachers();
            this.ModalPopupExtender1.Show();
           
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        int cid = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        common.SelectStateCountryId(ddlState, cid);

        common.SelectCityStateId(ddlCity, 0);

        common.SelectAreaCityId(ddlArea, 0);
    }
    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
        int sid = Convert.ToInt32(ddlState.SelectedItem.Value);
        common.SelectCityStateId(ddlCity, sid);
        common.SelectAreaCityId(ddlArea, 0);
    }
    protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        int ciid = Convert.ToInt32(ddlCity.SelectedItem.Value);
        common.SelectAreaCityId(ddlArea, ciid);
    }

    private void TeachingExperience()
    {
        ddlteex.Items.Clear();

        for (int i = 0; i <= 20; i++)
        {
            ddlteex.Items.Add(i.ToString());
        }
        ddlteex.DataBind();
        ddlteex.Items.Insert(0, new ListItem("--Select Teaching Experience--", "0"));
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        this.ModalPopupExtender1.Hide();
        Response.Redirect("TeacherProfile.aspx");

    }
}