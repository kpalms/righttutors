﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Collections;
using System.Web.Services;
using System.Web.Script.Services;
using System.Globalization;
using System.Threading;
public partial class Notification : System.Web.UI.Page
{
    BL_Notification bl_notifiation = new BL_Notification();
    Common common = new Common();
    public static int teacherid { get; set; }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.MasterPageFile = "~/MasterPage.master"; 
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        
        

        if (Session["TeachersId"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                BindNotificatino();
               // teacherid = Convert.ToInt32(Session["TeachersId"]);
               //  String strnotification= Convert.ToString(common.ReturnOneValue("select count(*) from Notification where Status=0 and TeachersId='" + Session["TeachersId"] + "'"));
               //if (strnotification != "0")
               // {
               //     bl_notifiation.TeachersId = Convert.ToInt32(Session["TeachersId"]);
               //     bl_notifiation.UpdateNotification(bl_notifiation);
               // }
       
            }
        }

    }

    //public class Notifications
    //{
    //    public string DateTime { get; set; }
    //    public string Message { get; set; }
    //}

    //[WebMethod]
    //public static Notifications[] BindNotifications()
    //{
    //    DataSet ds = selectNotificationbyTeacherId(1);
    //    List<Notifications> lstdetails = new List<Notifications>();
    //    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
    //    {
    //        Notifications rd = new Notifications();
    //        rd.DateTime = ds.Tables[0].Rows[i]["DateTime"].ToString();
    //        rd.Message = ds.Tables[0].Rows[i]["Message"].ToString();
    //        lstdetails.Add(rd);
    //    }
    //    return lstdetails.ToArray();
    //}

    //[WebMethod]
    //public static Notifications[] GetNotifications(int pageIndex)
    //{
    //    DataSet ds = selectNotificationbyTeacherId(pageIndex);
    //    List<Notifications> lstdetails = new List<Notifications>();
    //    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
    //    {
    //        Notifications rd = new Notifications();
    //        rd.DateTime = ds.Tables[0].Rows[i]["DateTime"].ToString();
    //        rd.Message = ds.Tables[0].Rows[i]["Message"].ToString();
    //        lstdetails.Add(rd);
    //    }
    //    return lstdetails.ToArray();
    //}

    //public static DataSet selectNotificationbyTeacherId(int pageIndex)
    //{
        //BL_Notification bl_notifiation = new BL_Notification();
        //DataSet ds = bl_notifiation.selectNotificationbyTeacherId(bl_notifiation, pageIndex, teacherid);
        //return ds;
    //}


    private void BindNotificatino()
    {
        bl_notifiation.TeachersId = Convert.ToInt32(Session["TeachersId"]);
       DataTable dt= bl_notifiation.selectNotificationbyTeacherId(bl_notifiation);
       rptnotification.DataSource = dt;
       rptnotification.DataBind();
    }
}