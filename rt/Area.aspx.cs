﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class RT_Area : System.Web.UI.Page
{
    static int id;
    BL_Area bl_area = new BL_Area();
    Common common = new Common();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            common.BindCountry(ddlCountry);
            common.SelectStateCountryId(ddlState, Convert.ToInt32(0));
            common.SelectCityStateId(ddlCity, Convert.ToInt32(0));
            //BindArea();
            btnupdate.Enabled = false;
        }
    }

    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
       int sid = Convert.ToInt32(ddlState.SelectedItem.Value);
       common.SelectCityStateId(ddlCity,sid);
       BindArea();
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        bl_area.Area = txtArea.Text;
        bl_area.CountryId = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        bl_area.StateId = Convert.ToInt32(ddlState.SelectedItem.Value);
        bl_area.CityId = Convert.ToInt32(ddlCity.SelectedItem.Value);
       DataTable dt= bl_area.SelectAreaName(bl_area);
        if(dt.Rows.Count == 0)
        {
            lblmsg.Visible = false;
        bl_area.AreaId = 0;
        bl_area.Mode = "Insert";
        bl_area.AddArea(bl_area);
        BindArea();
        Clear();
        btnupdate.Enabled = false;
        btnsave.Enabled = true;
        }
        else
        {
            lblmsg.Visible = true;
           lblmsg.Text = "Already existing this country !";
        }
    }

    private void Clear()
    {
        
        txtArea.Text = "";
        //ddlCountry.SelectedItem.Value = Convert.ToString(0);
    }

    private void BindArea()
    {
        bl_area.CountryId = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        bl_area.StateId = Convert.ToInt32(ddlState.SelectedItem.Value);
        bl_area.CityId = Convert.ToInt32(ddlCity.SelectedItem.Value);
        DataTable dt = bl_area.SelectArea(bl_area);
        gvArea.DataSource = dt;
        gvArea.DataBind();
    }

    protected void gvArea_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            lblmsg.Visible = false;
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblCountryId = (Label)row.FindControl("lblCountryId");
            Label lblStateId = (Label)row.FindControl("lblStateId");
            Label lblCityId = (Label)row.FindControl("lblCityId");
            Label lblArea = (Label)row.FindControl("lblArea");
            if (e.CommandName == "EditRow")
            {
                id = Convert.ToInt32(e.CommandArgument.ToString());
                ddlCountry.Text = lblCountryId.Text;
                common.SelectStateCountryId(ddlState, Convert.ToInt32(lblCountryId.Text));
                common.SelectCityStateId(ddlCity, Convert.ToInt32(lblStateId.Text));
                ddlState.Text = lblStateId.Text;
                ddlCity.Text = lblCityId.Text;
                txtArea.Text = lblArea.Text;
                btnupdate.Enabled = true;
                btnsave.Enabled = false;
            }
            if (e.CommandName == "DeleteRow")
            {
                bl_area.Area = "";
                bl_area.CountryId = 0;
                bl_area.StateId = 0;
                bl_area.CityId = 0;
                bl_area.AreaId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_area.Mode = "Delete";
                bl_area.AddArea(bl_area);
                BindArea();
                btnupdate.Enabled = false;
                btnsave.Enabled = true;
                Clear();
            }
        }
        catch
        {


        }
    }

    protected void btnclear_Click(object sender, EventArgs e)
    {
        lblmsg.Visible = false;
        Clear();
        ddlCountry.SelectedIndex = 0;
        common.SelectStateCountryId(ddlState, Convert.ToInt32(0));
        common.SelectCityStateId(ddlCity, Convert.ToInt32(0));
        BindArea();
        btnupdate.Enabled = false;
        btnsave.Enabled = true;
    }

    protected void btnupdate_Click(object sender, EventArgs e)
    {
        lblmsg.Visible = false;
        bl_area.Area = txtArea.Text;
        bl_area.CountryId = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        bl_area.StateId = Convert.ToInt32(ddlState.SelectedItem.Value);
        bl_area.CityId = Convert.ToInt32(ddlCity.SelectedItem.Value);
        bl_area.AreaId = id;
        bl_area.Mode = "Update";
        bl_area.AddArea(bl_area);
        BindArea();
        Clear();
        btnupdate.Enabled = false;
        btnsave.Enabled = true;
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        int cid = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        common.SelectStateCountryId(ddlState, cid);


        int sid = Convert.ToInt32(ddlState.SelectedItem.Value);
        common.SelectCityStateId(ddlCity, sid);
        BindArea();
    }

    protected void gvArea_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvArea.PageIndex = e.NewPageIndex;
        BindArea();
    }

    protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindArea();
    }
}