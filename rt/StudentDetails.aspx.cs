﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class rt_StudentDetails : System.Web.UI.Page
{
    BL_Students bl_student = new BL_Students();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["Status"].ToString() == "UVS")
        {
            lnkactive.Visible = false;
        }
        if (Request.QueryString["Status"].ToString() == "DS")
        {
            lnkdelete.Visible = true;
        }
        BindStudent();
    }

    private void BindStudent()
    {
        bl_student.StudentsId =Convert.ToInt32(Request.QueryString["key"].ToString());
        DataSet ds = bl_student.SelectStudentsById(bl_student);

        if (ds.Tables[0].Rows.Count > 0)
        {
            lblstudentid.Text = ds.Tables[0].Rows[0]["SID"].ToString();
            lblfullname.Text = ds.Tables[0].Rows[0]["Name"].ToString() + " " + ds.Tables[0].Rows[0]["MiddleName"].ToString() + " " + ds.Tables[0].Rows[0]["LastName"].ToString();
            lbldob.Text = ds.Tables[0].Rows[0]["DOB"].ToString();
            lbladdress.Text = ds.Tables[0].Rows[0]["Address"].ToString() + ",<br>" + ds.Tables[0].Rows[0]["Area"].ToString() + "," + ds.Tables[0].Rows[0]["City"].ToString() + ",<br>" + ds.Tables[0].Rows[0]["State"].ToString() + "," + ds.Tables[0].Rows[0]["Country"].ToString() + ",<br>" + ds.Tables[0].Rows[0]["PinCode"].ToString();
            lblgender.Text = ds.Tables[0].Rows[0]["Genders"].ToString();
            lblmobileno.Text = ds.Tables[0].Rows[0]["MobileNo"].ToString();
            lblemail.Text = ds.Tables[0].Rows[0]["Email"].ToString();
            imgprofile.ImageUrl = ds.Tables[0].Rows[0]["ImageName"].ToString();
            lnkactive.Text = ds.Tables[0].Rows[0]["Active"].ToString();
        }

        string img = ds.Tables[0].Rows[0]["ImageName"].ToString();

        if (img != "")
        {
            imgprofile.ImageUrl = img.Substring(3);
        }

        if (ds.Tables[1].Rows.Count > 0)
        {
            lblboard.Text = ds.Tables[1].Rows[0]["Board"].ToString();
            lblclass.Text = ds.Tables[1].Rows[0]["Class"].ToString();

            bl_student.BoardId = Convert.ToInt32(ds.Tables[1].Rows[0]["BoardId"].ToString());
            bl_student.ClassId = Convert.ToInt32(ds.Tables[1].Rows[0]["ClassId"].ToString());
            bl_student.StudentsId = Convert.ToInt32(Session["StudentsId"]);
            DataTable dt = bl_student.StudentsubjectbyBoradIClassId(bl_student);
            lblsubject.Text = "";

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                lblsubject.Text = lblsubject.Text + dt.Rows[i]["Subject"].ToString() + ",";
            }
        }

        if (ds.Tables[4].Rows.Count > 0)
        {
            lstteacher.DataSource = ds.Tables[4];
            lstteacher.DataBind();
        }

        if (ds.Tables[5].Rows.Count > 0)
        {
            rptteachertesto.DataSource = ds.Tables[5];
            rptteachertesto.DataBind();
        }
    }

    protected void lnkactive_Click(object sender, EventArgs e)
    {
        bl_student.Mode = "Active";
        bl_student.StudentsId = Convert.ToInt32(Request.QueryString["key"].ToString());
        if (lnkactive.Text == "Deactive")
        {
            bl_student.Active = 0;
            bl_student.StudentsUpdates(bl_student);
            Response.Redirect("ViewRegisterStudent.aspx");
        }
        else
        {
            bl_student.Active = 1;
            bl_student.StudentsUpdates(bl_student);
            Response.Redirect("ViewDeactiveStudent.aspx");
        }

    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        bl_student.StudentsId = Convert.ToInt32(Request.QueryString["key"].ToString());
        bl_student.DeleteStudent(bl_student);
        Response.Redirect("ViewDeactiveStudent.aspx");
    }
}