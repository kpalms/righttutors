﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class rt_RTTestimonial : System.Web.UI.Page
{
    BL_Testimonial bl_testimonial = new BL_Testimonial();

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            bindTestimonial();
        }
    }

    private void bindTestimonial()
    {
      DataSet ds= bl_testimonial.SelectRTTestimonial();

      rptstudenttesto.DataSource=ds.Tables[0];
      rptstudenttesto.DataBind();

      rptteachertesto.DataSource = ds.Tables[1];
      rptteachertesto.DataBind();

    }
}