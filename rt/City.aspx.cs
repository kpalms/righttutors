﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class RT_City : System.Web.UI.Page
{
    static int id;
    BL_City bl_city = new BL_City();
    Common common = new Common();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            common.BindCountry(ddlCountry);
            common.SelectStateCountryId(ddlState, Convert.ToInt32(0));
           // BindCity();
            btnupdate.Enabled = false;
        }
    }


    protected void btnsave_Click(object sender, EventArgs e)
    {
        bl_city.City = txtCity.Text;
        bl_city.CountryId = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        bl_city.StateId = Convert.ToInt32(ddlState.SelectedItem.Value);
        DataTable dt=   bl_city.SelectCityName(bl_city);

     if (dt.Rows.Count == 0)
     {
         lblmsg.Visible = false;
         bl_city.CityId = 0;
         bl_city.Mode = "Insert";
         bl_city.AddCity(bl_city);
         BindCity();
         Clear();
         btnupdate.Enabled = false;
         btnsave.Enabled = true;
     }
     else
     {
         lblmsg.Visible = true;
         lblmsg.Text = "Already existing this country !";
     }

    }

    private void Clear()
    {
        txtCity.Text = "";
    }

    private void BindCity()
    {
        bl_city.CountryId = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        bl_city.StateId = Convert.ToInt32(ddlState.SelectedItem.Value);
        DataTable dt = bl_city.SelectCity(bl_city);
        gvCity.DataSource = dt;
        gvCity.DataBind();
    }
    protected void gvCity_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        lblmsg.Visible = false;
        try
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblCountryId = (Label)row.FindControl("lblCountryId");
            Label lblStateId = (Label)row.FindControl("lblStateId");
            Label lblCity = (Label)row.FindControl("lblCity");

            if (e.CommandName == "EditRow")
            {
                id = Convert.ToInt32(e.CommandArgument.ToString());
                ddlCountry.Text = lblCountryId.Text;
                common.SelectStateCountryId(ddlState, Convert.ToInt32(lblCountryId.Text));
                ddlState.Text = lblStateId.Text;
                txtCity.Text = lblCity.Text;
                btnupdate.Enabled = true;
                btnsave.Enabled = false;
            }
            if (e.CommandName == "DeleteRow")
            {
                bl_city.City = "";
                bl_city.CountryId = 0;
                bl_city.StateId = 0;
                bl_city.CityId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_city.Mode = "Delete";
                bl_city.AddCity(bl_city);
                BindCity();
                txtCity.Text = "";
                btnupdate.Enabled = false;
                btnsave.Enabled = true;
                //ddlCountry.SelectedIndex = 0;
                //common.SelectStateCountryId(ddlState, Convert.ToInt32(0));
            }
        }
        catch
        {


        }
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        Clear();
        ddlCountry.SelectedIndex = 0;
        common.SelectStateCountryId(ddlState, Convert.ToInt32(0));
        BindCity();
        btnupdate.Enabled = false;
        btnsave.Enabled = true;
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        lblmsg.Visible = false;
        bl_city.City = txtCity.Text;
        bl_city.CountryId = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        bl_city.StateId = Convert.ToInt32(ddlState.SelectedItem.Value);
        bl_city.CityId = id;
        bl_city.Mode = "Update";
        bl_city.AddCity(bl_city);
        BindCity();
        Clear();
        btnupdate.Enabled = false;
        btnsave.Enabled = true;
        //ddlCountry.SelectedIndex = 0;
        //common.SelectStateCountryId(ddlState, Convert.ToInt32(0));
    }
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        int cid = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        common.SelectStateCountryId(ddlState,cid);
        BindCity();
    }


    protected void gvCity_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCity.PageIndex = e.NewPageIndex;
        BindCity();
    }
    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindCity();
    }
}