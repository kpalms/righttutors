﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class rt_ViewRegisterStudent : System.Web.UI.Page
{
    BL_Students bl_student = new BL_Students();
    protected void Page_Load(object sender, EventArgs e)
    {
        BindStudent();
    }

    private void BindStudent()
    {
        DataTable dt = bl_student.SelectStudent(bl_student);
        lststudent.DataSource = dt;
        lststudent.DataBind();
    }
    protected void lnksearch_Click(object sender, EventArgs e)
    {
        if (txtsearch.Text != "")
        {
            bl_student.StudentsId = Convert.ToInt32(txtsearch.Text);
            DataTable dt = bl_student.SearchStudent(bl_student);
            lststudent.DataSource = dt;
            lststudent.DataBind();
        }
        else
        {
            BindStudent();
            txtsearch.Text = "";
        }
    }
    protected void lnkcancel_Click(object sender, EventArgs e)
    {
        BindStudent();
        txtsearch.Text = "";
    }
}