﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class rt_Home : System.Web.UI.Page
{
    BL_Students bl_student = new BL_Students();
    protected void Page_Load(object sender, EventArgs e)
    {
        BindAllcount();
    }

    private void BindAllcount()
    {
      DataSet ds= bl_student.SelectAllCount(bl_student);

        if(ds.Tables[0].Rows.Count > 0)
        {
            lblACS.Text = ds.Tables[0].Rows[0]["ACS"].ToString();
        }

        if (ds.Tables[1].Rows.Count > 0)
        {
            lblDES.Text = ds.Tables[1].Rows[0]["DES"].ToString();
        }

        if (ds.Tables[2].Rows.Count > 0)
        {
            lblUVS.Text = ds.Tables[2].Rows[0]["UVS"].ToString();
        }

        if (ds.Tables[3].Rows.Count > 0)
        {
            lblACT.Text = ds.Tables[3].Rows[0]["ACT"].ToString();
        }

        if (ds.Tables[4].Rows.Count > 0)
        {
            lblDET.Text = ds.Tables[4].Rows[0]["DET"].ToString();
        }

        if (ds.Tables[5].Rows.Count > 0)
        {
            lblUVT.Text = ds.Tables[5].Rows[0]["UVT"].ToString();
        }
    }
}