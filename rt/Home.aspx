﻿<%@ Page Title="" Language="C#" MasterPageFile="~/rt/RTMasterPage.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="rt_Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<section id="contact">
  <div class="container">
  <div class="row" style="padding:20px 50px 20px 50px;">
    <div style="height:200px;width:250px;margin:15px;" class="col-lg-4 col-md-4 col-sm-4">
     <span class="notification" style="text-align:left"><asp:Label ID="lblACS" ForeColor="Red" runat="server"></asp:Label></span>
  <a href="ViewRegisterStudent.aspx"><img src="../img/loginstudent.png" class="img-responsive img-thumbnail" style="padding:40px"></a>
   
    <h4 style="align:center">View Student</h4>
    </div>


      <div style="height:200px;width:250px;margin:15px;" class="col-lg-4 col-md-4 col-sm-4">
            <span class="notification" style="text-align:left"><asp:Label ID="lblDES" ForeColor="Red" runat="server"></asp:Label></span>
       <a href="ViewDeactiveStudent.aspx"><img src="../img/loginstudent.png" class="img-responsive img-thumbnail" style="padding:40px"></a>
      <h4 style="align:center">View Deactive Student</h4>
    </div>


      
     <div style="height:200px;width:250px;margin:15px;" class="col-lg-4 col-md-4 col-sm-4">
                 <span class="notification" style="text-align:left"><asp:Label ID="lblUVS" ForeColor="Red" runat="server"></asp:Label></span>

     <a href="ViewUnVerifiedStudent.aspx"><img src="../img/loginstudent.png" class="img-responsive img-thumbnail" style="padding:40px"></a>
    <h4 style="align:center">View Unverified Student</h4>
    </div>
  </div>

   <div class="row" style="padding:20px 50px 20px 50px;">
      
    <div style="height:200px;width:250px;margin:15px;" class="col-lg-4 col-md-4 col-sm-4">
    <span class="notification"><asp:Label ID="lblACT" ForeColor="Red" runat="server"></asp:Label></span>
    <a href="ViewRegisterTeacher.aspx"><img src="../img/loginteacher.jpg" class="img-responsive img-thumbnail" style="padding:40px"></a>
   <h4 style="align:center">View Teacher</h4>
    </div>


      <div style="height:200px;width:250px;margin:15px;" class="col-lg-4 col-md-4 col-sm-4">
          <span class="notification"><asp:Label ID="lblDET" ForeColor="Red" runat="server"></asp:Label></span>
        <a href="ViewDeactiveTeacher.aspx"><img src="../img/loginteacher.jpg" class="img-responsive img-thumbnail" style="padding:40px"></a>
    <h4 style="align:center">View Deactive Teacher</h4>
    </div>

   
      <div style="height:200px;width:250px;margin:15px;"  class="col-lg-4 col-md-4 col-sm-4">
         <span class="notification"><asp:Label ID="lblUVT" ForeColor="Red" runat="server"></asp:Label></span>
       <a href="ViewUnVerifiedTeacher.aspx"><img src="../img/loginteacher.jpg" class="img-responsive img-thumbnail" style="padding:40px"></a>
     <h4 style="align:center">View Unverified Teacher</h4>
    </div>


   
  </div>
       </div>
      </div>
    </section>
</asp:Content>

