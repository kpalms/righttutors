﻿<%@ Page Title="" Language="C#" MasterPageFile="~/rt/RTMasterPage.master" AutoEventWireup="true" CodeFile="ViewAllContactUs.aspx.cs" Inherits="rt_ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
 <ContentTemplate>
 <section id="contact">
      <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">View All ContactUs</h2>
              <span></span> 
            </div>
          </div>
       </div>
       <div class="row" >
<div class="container" id="parent">

  <div class="row">
    <div class="col-lg-12">
      <div class="row ">

        <div class="col-lg-12" id="child" >
 <div align="center" >
<asp:GridView ID="gvcontactus" runat="server" class="table table-hover" AutoGenerateColumns="False" EmptyDataText="No records Found"
AllowPaging="True" onpageindexchanging="gvcontactus_PageIndexChanging" PageSize="20"        onrowcommand="gvcontactus_RowCommand">
    <Columns>
        <asp:TemplateField HeaderText="ContactUsId" Visible="false">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ContactUsId") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblContactUsId" runat="server" Text='<%# Bind("ContactUsId") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Topics">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Topics") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblTopics" runat="server" Text='<%# Bind("Topics") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
         <asp:TemplateField HeaderText="Details">
            <ItemTemplate>
           <b>Name:</b> <asp:Label ID="lblName" runat="server" Text='<%# Bind("Name") %>'></asp:Label><br />
            <b>Mo :</b> <asp:Label ID="lblMobileNo" runat="server" Text='<%# Bind("MobileNo") %>'></asp:Label><br />
           <b> Email :</b> <asp:Label ID="lblEmailId" runat="server" Text='<%# Bind("EmailId") %>'></asp:Label><br />
           <b> Add : </b> <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>,
           <asp:Label ID="lblArea" runat="server" Text='<%# Bind("Area") %>'></asp:Label>,<asp:Label ID="lblCity" runat="server" Text='<%# Bind("City") %>'></asp:Label>
            <asp:Label ID="lblState" runat="server" Text='<%# Bind("State") %>'></asp:Label>,<asp:Label ID="lblCountry" runat="server" Text='<%# Bind("Country") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="Message" >
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Message") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblMessage" runat="server" Text='<%# Bind("Message") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
          
        <asp:TemplateField HeaderText="Action">
            <ItemTemplate>
                <asp:LinkButton ID="lnkdelete" CommandName="DeleteRow" class="btn btn-danger" CommandArgument='<%# Bind("ContactUsId") %>' OnClientClick="return conformbox();" runat="server"><i class="glyphicon glyphicon-trash icon-white"></i>Delete</asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
   


</div>
        </div>
      </div>
    </div>
  </div>



       </div>
      </div>
    </section>
     </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

