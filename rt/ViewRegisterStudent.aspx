﻿<%@ Page Title="" Language="C#" MasterPageFile="~/rt/RTMasterPage.master" AutoEventWireup="true" CodeFile="ViewRegisterStudent.aspx.cs" Inherits="rt_ViewRegisterStudent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<section id="contact">
      <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">View Register Students</h2>
              <span></span> 
            </div>
          </div>
       </div>

<div class="row>
 <div class="col-xs-3">
<form class="form-inline">
  <div class="form-group">
    <div class="input-group">
    <div class="input-group-addon"><asp:LinkButton ID="lnkcancel" style="text-decoration:none" runat="server" 
            onclick="lnkcancel_Click">RTS</asp:LinkButton></div>
      <asp:TextBox ID="txtsearch" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57'  placeholder="Search Student" runat="server"></asp:TextBox>
     
          <div class="input-group-addon"><asp:LinkButton ID="lnksearch" runat="server" onclick="lnksearch_Click" ValidationGroup="new"><span class="glyphicon glyphicon-search icon-white">Search</asp:LinkButton></div>

    </div>
  </div>
   <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Please Enter Student Id" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtsearch"></asp:RequiredFieldValidator>
</form>
</div>
</div>
</div>
<div class="container">
       <div class="row" >
        <div class="col-lg-12 col-md-12 col-sm-12">
        
<asp:ListView ID="lststudent" runat="server" class="table-striped" GroupItemCount="6">
<EmptyDataTemplate>
No Data found.
</EmptyDataTemplate>
<LayoutTemplate>
<table id="groupPlaceholderContainer"  runat="server" border="0" cellpadding="0" cellspacing="0">
<tr id="groupPlaceholder" runat="server">
</tr>
</table>
</LayoutTemplate>
<GroupTemplate>
<tr id="itemPlaceholderContainer" runat="server" style="height: 100px;">
<td id="itemPlaceholder" runat="server">
</td>
</tr>
</GroupTemplate>
<ItemTemplate>           
<td width="20%" align="left" height="200px">
<ul>
<li>

<a href="StudentDetails.aspx?key=<%# Eval("StudentsId") %>&Status=RS" style="text-decoration:none">
<img src="<%#Eval("ImageName") %>" alt="" class="img-responsive img-thumbnail" height="100px" width="100px"><br />
<asp:Label ID="lblsid" runat="server" Text='<%#Eval("SID")  %>'></asp:Label><br />
<asp:Label ID="lblName" runat="server" Text='<%#Eval("Name")  %>'></asp:Label><br />
<asp:Label ID="lblMobileNo" runat="server" Text='<%#Eval("MobileNo")  %>'></asp:Label>
</a>

&nbsp;&nbsp;&nbsp;</li>
</ul>
</td>                
</ItemTemplate>
</asp:ListView>

       </div>
             </div>
      </div>
    </section>
</asp:Content>

