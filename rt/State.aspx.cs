﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class RT_State : System.Web.UI.Page
{
    static int id;
    BL_State bl_state = new BL_State();
    Common common = new Common();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            common.BindCountry(ddlCountry);
            btnupdate.Enabled = false;
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        bl_state.State = txtState.Text;
        bl_state.CountryId = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        DataTable dt = bl_state.SelectStateName(bl_state);
       if (dt.Rows.Count == 0)
       {
           lblmsg.Visible = false;
        //bl_state.State = txtState.Text;
        //bl_state.CountryId = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        bl_state.StateId = 0;
        bl_state.Mode = "Insert";
        bl_state.AddState(bl_state);
        BindState();
        Clear();
       }
       else
       {
           lblmsg.Visible = true;
           lblmsg.Text = "Already existing this State !";
       }
    }

    private void Clear()
    {
        txtState.Text = "";
    }

    protected void gvState_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            lblmsg.Visible = false;
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblCountryId = (Label)row.FindControl("lblCountryId");
            Label lblState = (Label)row.FindControl("lblState");
            //Label lblCountry = (Label)row.FindControl("lblCountry");

            if (e.CommandName == "EditRow")
            {
               common.BindCountry(ddlCountry);
                id = Convert.ToInt32(e.CommandArgument.ToString());

                ddlCountry.Text = lblCountryId.Text;
                txtState.Text = lblState.Text;
                btnupdate.Enabled = true;
                btnsave.Enabled = false;
            }
            if (e.CommandName == "DeleteRow")
            {
                bl_state.State = "";
                bl_state.CountryId = 0;
                bl_state.StateId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_state.Mode = "Delete";
                bl_state.AddState(bl_state);
                BindState();
                btnupdate.Enabled = false;
                btnsave.Enabled = true;
                txtState.Text = "";
                ddlCountry.SelectedIndex = 0;
            }
        }
        catch
        {


        }
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        Clear();
        lblmsg.Visible = false;
        ddlCountry.SelectedIndex=0;
        btnupdate.Enabled = false;
        btnsave.Enabled = true;
        BindState();
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        lblmsg.Visible = false;
        bl_state.State = txtState.Text;
        bl_state.CountryId = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        bl_state.StateId = id;
        bl_state.Mode = "Update";
        bl_state.AddState(bl_state);
        Clear();
        BindState();
        btnupdate.Enabled = false;
        btnsave.Enabled = true;
        //ddlCountry.SelectedIndex = 0;
    }
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindState();
    }

    private void BindState()
    {
        bl_state.CountryId = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        DataTable dt = bl_state.SelectStateCountryId(bl_state);
        if (dt.Rows.Count > 0)
        {
            gvState.DataSource = dt;
            gvState.DataBind();
        }
        else
        {
            gvState.DataSource = null;
            gvState.DataBind();
        }
        txtState.Text = "";
    }


    protected void gvState_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvState.PageIndex = e.NewPageIndex;
        BindState();
    }
}