﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Data.SqlClient;
public partial class rt_News : System.Web.UI.Page
{
    static int id;
    static string imagename;
    Common common = new Common();
    BL_News bl_news = new BL_News();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            btnupdate.Enabled = false;
            BindNews();
        }
    }
    private void setfill()
    {
        bl_news.Title = txttitle.Text;
        bl_news.Description = txtdescription.Text;
        bl_news.Active = 1;
        bl_news.Date = common.GetDate();
    }

    private void clear()
    {
        txtdescription.Text = "";
        txttitle.Text = "";
    }

    private void BindNews()
    {
        DataTable dt = bl_news.SelectNews(bl_news);
        gvtodyapost.DataSource = dt;
        gvtodyapost.DataBind();
    }

  
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            setfill();
            bl_news.NewsId = 0;
            bl_news.Mode = "Insert";
            if (fileimage.HasFile)
            {
                string ext = Path.GetExtension(fileimage.FileName);
                string filename = common.GetImageName() + "" + ext;
                fileimage.PostedFile.SaveAs(Server.MapPath("Images/News/") + filename);
                bl_news.Image = filename;
            }
            else
            {
                bl_news.Image = "NoImage.png";
            }
            bl_news.AddNews(bl_news);
            clear();
            btnupdate.Enabled = false;
            btnsave.Enabled = true;
            BindNews();
        }
        catch
        {
        }
    }

    protected void btnclear_Click(object sender, EventArgs e)
    {
        clear();
        btnupdate.Enabled = false;
        btnsave.Enabled = true;
        imgedit.Visible = false;
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {
            string filename;
            setfill();
            bl_news.NewsId = id;
            bl_news.Mode = "Update";
            if (fileimage.HasFile)
            {
                string ext = Path.GetExtension(fileimage.FileName);

                if ("NoImage.jpg" == imagename)
                {
                     filename = common.GetImageName() + "" + ext;
                }
                else
                {
                    string fn = Path.GetFileNameWithoutExtension(imagename);
                    filename = fn + "" + ext;
                    string imageFilePath = Server.MapPath("Images/News/" + imagename);
                    File.Delete(imageFilePath);
                }
                fileimage.PostedFile.SaveAs(Server.MapPath("Images/News/") + filename);
                bl_news.Image = filename;
            }
            else
            {
                bl_news.Image = imagename;
            }
            bl_news.AddNews(bl_news);
            clear();
            btnupdate.Enabled = false;
            btnsave.Enabled = true;
            imgedit.Visible = false;
            BindNews();
        }
        catch
        {
        }
    }


    protected void gvtodyapost_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            string sub;
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblTitle = (Label)row.FindControl("lblTitle");
            Label lblDescription = (Label)row.FindControl("lblDescription");
            Label lblImage = (Label)row.FindControl("lblImage");
            LinkButton lnkactive = (LinkButton)row.FindControl("lnkactive");

            if (e.CommandName == "EditRow")
            {
                id = Convert.ToInt32(e.CommandArgument.ToString());
                txttitle.Text = lblTitle.Text;
                txtdescription.Text = lblDescription.Text;
                sub = lblImage.Text.Substring(12);
                imagename = sub;
                imgedit.Visible = true;
                imgedit.ImageUrl = lblImage.Text;
                btnupdate.Enabled = true;
                btnsave.Enabled = false;
            }

            if (e.CommandName == "DeleteRow")
            {
                bl_news.Title = "";
                bl_news.Description = "";
                bl_news.Date = DateTime.Now;
                bl_news.NewsId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_news.Mode = "Delete";
                bl_news.Image = "";
                bl_news.Active = 0;
                bl_news.AddNews(bl_news);
                BindNews();
                imgedit.Visible = false;
                sub = lblImage.Text.Substring(12);
                imagename = sub;
                if ("NoImage.png" != imagename)
                {
                    string imageFilePath = Server.MapPath(lblImage.Text);
                    File.Delete(imageFilePath);
                }
                clear();
                btnupdate.Enabled = false;
                btnsave.Enabled = true;
            }
            if (e.CommandName == "Active")
            {
                bl_news.Title = "";
                bl_news.Description = "";
                bl_news.Date = DateTime.Now;
                bl_news.NewsId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_news.Mode = "Active";
                bl_news.Image = "";
                if (lnkactive.Text != "Active")
                {
                    bl_news.Active = 0;
                }
                else
                {
                    bl_news.Active = 1;
                }

                bl_news.AddNews(bl_news);
                BindNews();
            }
        }
        catch
        {
        }
    }

    protected void gvtodyapost_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvtodyapost.PageIndex = e.NewPageIndex;
        BindNews();
    }
}