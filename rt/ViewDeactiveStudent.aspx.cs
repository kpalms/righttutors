﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class rt_ViewDeactiveStudent : System.Web.UI.Page
{
    BL_Students bl_student = new BL_Students();
    protected void Page_Load(object sender, EventArgs e)
    {
        BindStudent();
    }

    private void BindStudent()
    {
        DataTable dt = bl_student.SelectStudentDeactive(bl_student);
        lststudent.DataSource = dt;
        lststudent.DataBind();
    }
}