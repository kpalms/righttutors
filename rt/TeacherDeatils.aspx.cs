﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
public partial class rt_TeacherDeatils : System.Web.UI.Page
{
    BL_Teachers bl_teacher = new BL_Teachers();
    BL_SelectedSubject bl_selectedsubject = new BL_SelectedSubject();
    BL_ToDayPost bl_todaypost = new BL_ToDayPost();
    BL_Notification bl_notification = new BL_Notification();
    static string imagename;
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["Status"].ToString() == "UVT")
        {
            lnkactive.Visible = false;
        }

        if (Request.QueryString["Status"].ToString() == "DT")
        {
            lnkdelete.Visible = true;
        }
        if (!IsPostBack)
        {
            BindTeacher();
            btnupdate.Enabled = false;
        }
    }

    private void BindTeacher()
    {
        bl_teacher.TeachersId = Convert.ToInt32(Request.QueryString["key"].ToString());
        DataSet ds = bl_teacher.SelectTeachersById(bl_teacher);
        if (ds.Tables[0].Rows.Count > 0)
        {
            lblteacherid.Text = ds.Tables[0].Rows[0]["TID"].ToString();
            string mname = ds.Tables[0].Rows[0]["MiddleName"].ToString();
            if (mname =="")
            {
                lblfullname.Text = ds.Tables[0].Rows[0]["Name"].ToString() + " " + ds.Tables[0].Rows[0]["LastName"].ToString();

            }
            else
            {
                lblfullname.Text = ds.Tables[0].Rows[0]["Name"].ToString() + " " + ds.Tables[0].Rows[0]["MiddleName"].ToString() + " " + ds.Tables[0].Rows[0]["LastName"].ToString();

            }
            lbldob.Text = ds.Tables[0].Rows[0]["BirthYear"].ToString();
            lbltagline.Text = ds.Tables[0].Rows[0]["TagLine"].ToString();
            lblEducation.Text = ds.Tables[0].Rows[0]["Education"].ToString();
            lblExprience.Text = ds.Tables[0].Rows[0]["Experience"].ToString();
            lbladdress.Text = ds.Tables[0].Rows[0]["Address"].ToString() + ",<br>" + ds.Tables[0].Rows[0]["Area"].ToString() + "," + ds.Tables[0].Rows[0]["City"].ToString() + ",<br>" + ds.Tables[0].Rows[0]["State"].ToString() + "," + ds.Tables[0].Rows[0]["Country"].ToString() + ",<br>" + ds.Tables[0].Rows[0]["PinCode"].ToString();
            lblgender.Text = ds.Tables[0].Rows[0]["Genders"].ToString();
            lblmobileno.Text = ds.Tables[0].Rows[0]["MobileNo"].ToString();
            lblemail.Text = ds.Tables[0].Rows[0]["Email"].ToString();
            lblaboutme.Text = ds.Tables[0].Rows[0]["AboutMe"].ToString();
            string img = ds.Tables[0].Rows[0]["ImageName"].ToString();
            lnkactive.Text = ds.Tables[0].Rows[0]["Active"].ToString();
            imgprofile.ImageUrl = img.Substring(3);
        }

        if (ds.Tables[1].Rows.Count > 0)
        {
            rptteacherboardclass.DataSource = ds.Tables[1];
            rptteacherboardclass.DataBind();
        }
        else
        {
            rptteacherboardclass.DataSource = null;
            rptteacherboardclass.DataBind();
        }

        if (ds.Tables[2].Rows.Count > 0)
        {
            gvtodyapost.DataSource = ds.Tables[2];
            gvtodyapost.DataBind();
        }
        else
        {
            gvtodyapost.DataSource = null;
            gvtodyapost.DataBind();
        }

        if (ds.Tables[4].Rows.Count > 0)
        {
            GVNOTIFICATION.DataSource = ds.Tables[4];
            GVNOTIFICATION.DataBind();
        }
        else
        {
            GVNOTIFICATION.DataSource = null;
            GVNOTIFICATION.DataBind();
        }

        if (ds.Tables[5].Rows.Count > 0)
        {
            rptstudenttesto.DataSource = ds.Tables[5];
            rptstudenttesto.DataBind();
        }
    }

    protected void lnkactive_Click(object sender, EventArgs e)
    {
        bl_teacher.Mode = "Active";
        bl_teacher.TeachersId = Convert.ToInt32(Request.QueryString["key"].ToString());
        
        if (lnkactive.Text == "Deactive")
        {
            bl_teacher.Active = 0;
            bl_teacher.TeachersUpdates(bl_teacher);
            Response.Redirect("ViewRegisterTeacher.aspx");
        }
        else
        {
            bl_teacher.Active = 1;
            bl_teacher.TeachersUpdates(bl_teacher);
            Response.Redirect("ViewDeactiveTeacher.aspx");
        }
    }

    protected void gvtodyapost_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            string sub;
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblImage = (Label)row.FindControl("lblImage");
            LinkButton lnkactive = (LinkButton)row.FindControl("lnkactive");

            if (e.CommandName == "DeleteRow")
            {
                bl_todaypost.Title = "";
                bl_todaypost.Description = "";
                bl_todaypost.Date = DateTime.Now;
                bl_todaypost.TodayPostId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_todaypost.Mode = "Delete";
                bl_todaypost.Image = "";
                bl_todaypost.Active = 0;
                bl_todaypost.BoardId = 0;
                bl_todaypost.ClassId = 0;
                bl_todaypost.TeachersId = 0;
                bl_todaypost.AddTodayPost(bl_todaypost);
         
                sub = lblImage.Text.Substring(17);
                imagename = sub;
                if ("NoImage.png" != imagename)
                {
                    string imageFilePath = Server.MapPath(lblImage.Text);
                    File.Delete(imageFilePath);
                }

            }

            if (e.CommandName == "Active")
            {
                bl_todaypost.Title = "";
                bl_todaypost.Description = "";
                bl_todaypost.Date = DateTime.Now;
                bl_todaypost.TodayPostId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_todaypost.Mode = "Active";
                bl_todaypost.Image = "";
                bl_todaypost.BoardId = 0;
                bl_todaypost.ClassId = 0;
                bl_todaypost.TeachersId = 0;

                if (lnkactive.Text == "Deactive")
                {
                    bl_todaypost.Active = 0;
                }
                else
                {
                    bl_todaypost.Active = 1;
                }

                bl_todaypost.AddTodayPost(bl_todaypost);

            }
            BindTeacher();
        }
        catch
        {


        }
    }

    protected void gvtodyapost_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvtodyapost.PageIndex = e.NewPageIndex;
        BindTeacher();
    }

    private void Setfill()
    {
        bl_notification.TeachersId = Convert.ToInt32(Request.QueryString["key"].ToString());
        bl_notification.Message = txtmsg.Text;
        Common common = new Common();
        bl_notification.DateTime = common.GetDate();
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        Setfill();
        bl_notification.NotificationId = 0;
        bl_notification.Mode = "Insert";
        bl_notification.AddNotification(bl_notification);
        txtmsg.Text = "";
        BindTeacher();
        btnsubmit.Enabled = true;
        btnclear.Enabled = true;
        btnupdate.Enabled = false;
    }

    protected void GVNOTIFICATION_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GVNOTIFICATION.PageIndex = e.NewPageIndex;
        BindTeacher();
    }


    protected void GVNOTIFICATION_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblMessage = (Label)row.FindControl("lblMessage");
         
            if (e.CommandName == "DeleteRow")
            {
                bl_notification.TeachersId = 0;
                bl_notification.Message = "";
                bl_notification.DateTime = DateTime.Now;
                bl_notification.NotificationId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_notification.Mode = "Delete";
                bl_notification.AddNotification(bl_notification);
                btnsubmit.Enabled = true;
                btnclear.Enabled = true;
                btnupdate.Enabled = false;
            }

            if (e.CommandName == "EditRow")
            {
                id = Convert.ToInt32(e.CommandArgument.ToString());
                txtmsg.Text = lblMessage.Text;
                btnsubmit.Enabled = false;
                btnclear.Enabled = true;
                btnupdate.Enabled = true;
            }
            BindTeacher();
        }
        catch
        {


        }
    }

    protected void btnupdate_Click(object sender, EventArgs e)
    {
        Setfill();
        bl_notification.NotificationId = id;
        bl_notification.Mode = "Update";
        bl_notification.AddNotification(bl_notification);
        txtmsg.Text = "";
        BindTeacher();
        btnsubmit.Enabled = true;
        btnclear.Enabled = true;
        btnupdate.Enabled = false;
    }

    protected void btnclear_Click(object sender, EventArgs e)
    {
        txtmsg.Text = "";
        btnsubmit.Enabled = true;
        btnclear.Enabled = true;
        btnupdate.Enabled = false;
    }

    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        bl_teacher.TeachersId = Convert.ToInt32(Request.QueryString["key"].ToString());
        bl_teacher.DeleteTeacher(bl_teacher);
        Response.Redirect("ViewDeactiveTeacher.aspx");
    }

    protected void rptteacherboardclass_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        Repeater rpt = e.Item.FindControl("rptsubject") as Repeater;

        Label lblboard = e.Item.FindControl("lblboard") as Label;
        Label lblclass = e.Item.FindControl("lblclass") as Label;
        bl_teacher.BoardId = Convert.ToInt32(lblboard.Text);
        bl_teacher.ClassId = Convert.ToInt32(lblclass.Text);
        bl_teacher.TeachersId = Convert.ToInt32(Request.QueryString["key"].ToString());
        DataTable dt = bl_teacher.TechersubjectbyBoradIClassId(bl_teacher);
        rpt.DataSource = dt;
        rpt.DataBind();
    }
}