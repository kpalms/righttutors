﻿<%@ Page Title="" Language="C#" MasterPageFile="~/rt/RTMasterPage.master" AutoEventWireup="true" CodeFile="RTTestimonial.aspx.cs" Inherits="rt_RTTestimonial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<section id="contact">
      <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">RT Testimonials</h2>
              <span></span> 
            </div>
          </div>
       </div>
       <div class="row" >
      


        <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="single_sidebar"><h2 align="center">TEACHERS</h2></div>
              <asp:Repeater ID="rptteachertesto" runat="server">
              <ItemTemplate>
              <table width="100%" cellpadding="0px" cellspacing="0px">
              <tr>
              <td>
              
              
               <a href="TeacherDeatils.aspx?key=<%# Eval("TeachersId") %>&Status=UVT" style="text-decoration:none">
              <img src='<%#Eval("ImageName")  %>' class="img-responsive img-thumbnail" alt="" height="50px" width="50px">
              <asp:Label ID="lbltname" ForeColor="Blue" Font-Bold="true" runat="server" Text='<%#Eval("TName")  %>'></asp:Label></a>
<asp:Label ID="lblWrites" runat="server" Text='<%#Eval("Writes")  %>'></asp:Label>
             
             </td>
              </tr>
              </table>
              <br />
              </ItemTemplate>
              </asp:Repeater>
              </div>




 <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="single_sidebar"><h2 align="center">STUDENTS</h2></div>
               <asp:Repeater ID="rptstudenttesto" runat="server">
              <ItemTemplate>
               <table width="100%" cellpadding="0px" cellspacing="0px">
              <tr>
              <td>
              
             <a href="StudentDetails.aspx?key=<%# Eval("StudentsId") %>&Status=UVS" style="text-decoration:none">
<img src="<%#Eval("ImageName") %>" class="img-responsive img-thumbnail" alt="" height="50px" width="50px">
<asp:Label ID="lblSName" runat="server" ForeColor="Blue" Font-Bold="true" Text='<%#Eval("SName")  %>'></asp:Label></a>
<asp:Label ID="lblWrites" runat="server" Text='<%#Eval("Writes")  %>'></asp:Label>
 </td>
              </tr>
              </table>
              <br />
              </ItemTemplate>
              </asp:Repeater>
              </div>



              </div>
              </div>
              </section>
</asp:Content>

