﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="rt_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
		
		<link href="../css/bootstrap.min.css" rel="stylesheet">
		<link href="../css/login.css" rel="stylesheet">
         <title>Right Tutors | Login</title>
  <link rel="shortcut icon" href="../img/logo.jpg" type="image/png">
</head>
<body>
 
    <div>


<%--        <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label><br />
        Email Id :<asp:TextBox ID="txtusername" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please Enter Email Id" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtusername"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="EmailFormat" runat="server" ValidationGroup="new" Text="Please enter a valid Email" ToolTip="Please enter a valid Email" ControlToValidate="txtusername" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="Red"  />

        <br />
      Password :<asp:TextBox ID="txtpassword" runat="server" TextMode="Password"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter Password" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtpassword"></asp:RequiredFieldValidator>

        <br />
        <asp:Button ID="btnlogin" runat="server"  ValidationGroup="new" Text="Log In" 
            onclick="btnlogin_Click" />--%>
            

    </div>



    <!--login modal-->
<div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
         
          <h1 class="text-center">Login</h1>
         
      </div>
      <div class="modal-body">
          <form class="form col-md-12 center-block" id="form1" runat="server">
               <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
 <ContentTemplate>
            <div class="form-group">
           
             <asp:TextBox ID="txtusername" runat="server" class="form-control input-lg" placeholder="Email"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please Enter Email Id" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtusername"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="EmailFormat" runat="server" ValidationGroup="new" Text="Please enter a valid Email" ToolTip="Please enter a valid Email" ControlToValidate="txtusername" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="Red"  />
            </div>
            <div class="form-group">
           
             <asp:TextBox ID="txtpassword" runat="server" TextMode="Password" class="form-control input-lg" placeholder="Password"></asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter Password" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtpassword"></asp:RequiredFieldValidator>
            </div>
            <div class="form-group">
             <asp:Label ID="lblmsg" runat="server" ForeColor="Red" Visible="false"></asp:Label>
                 <asp:Button ID="btnlogin" runat="server" class="btn btn-primary btn-lg btn-block"  ValidationGroup="new" Text="Log In" 
            onclick="btnlogin_Click" />
            </div>
            </ContentTemplate>
</asp:UpdatePanel>
          </form>
      </div>
      <div class="modal-footer">
          <div class="col-md-12">
          
		  </div>	
      </div>
  </div>
  </div>
</div>
</body>
</html>
