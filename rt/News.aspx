﻿<%@ Page Title="" Language="C#" MasterPageFile="~/rt/RTMasterPage.master" AutoEventWireup="true" CodeFile="News.aspx.cs" Inherits="rt_News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <section id="contact">
      <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">News</h2>
              <span></span> 
            </div>
          </div>
       </div>
       <div class="row" >



<div class="container" id="parent">

  <div class="row">
    <div class="col-lg-12">
      <div class="row ">

        <div class="col-lg-12" id="child" >
 <div align="center" >
 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
 <ContentTemplate>
    <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label><br />
<asp:TextBox ID="txttitle" runat="server" class="wp-form-control wpcf7-text" placeholder="Enter Title"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please Enter Title" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txttitle"></asp:RequiredFieldValidator>
<br />
<asp:TextBox ID="txtdescription" runat="server" class="wp-form-control wpcf7-text" Height="100px" placeholder="Enter Description" TextMode="MultiLine"></asp:TextBox>

<br />
 <asp:FileUpload ID="fileimage" runat="server"/>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.bmp|.jpeg)$"
    ControlToValidate="fileimage" ValidationGroup="new" runat="server" ForeColor="Red" ErrorMessage="Please select a valid png,jpg,gif,bmp,jpeg file."
    Display="Dynamic" />
      <sapm style="font-size:10px;color:red" >(upload only png,jpg,gif,bmp,jpeg)</sapm>
<br />
 <asp:Image ID="imgedit" runat="server" Visible="false" Width="50px" Height="50px"/>
<asp:Button ID="btnsave" runat="server" ValidationGroup="new" class="wpcf7-submit" Text="Save" onclick="btnsave_Click" />
<asp:Button ID="btnupdate" runat="server" ValidationGroup="new" class="wpcf7-updated" Text="Update" onclick="btnupdate_Click" />
<asp:Button ID="btnclear" runat="server" class="wpcf7-clear" Text="Clear" onclick="btnclear_Click" />
<br />
<br />
<asp:GridView ID="gvtodyapost" runat="server" class="table table-hover" AutoGenerateColumns="False" EmptyDataText="No records Found"
AllowPaging="True" onpageindexchanging="gvtodyapost_PageIndexChanging" PageSize="20"        onrowcommand="gvtodyapost_RowCommand">
    <Columns>
        <asp:TemplateField HeaderText="AddId" Visible="false">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("NewsId") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblTodayPostId" runat="server" Text='<%# Bind("NewsId") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Title">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Title") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Description" >
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
         <asp:TemplateField HeaderText="ImageName" >
            <EditItemTemplate>
            </EditItemTemplate>
            <ItemTemplate>
               <asp:Label ID="lblImage" runat="server" Text='<%# Bind("Image") %>' Visible="false"></asp:Label>
               <img src="<%#Eval("Image")%>" alt="" height="100px" width="100px">
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Action">
            <ItemTemplate>
            <asp:LinkButton ID="lnkactive" CommandName="Active" class="btn btn-success" CommandArgument='<%# Bind("NewsId") %>' Text='<%# Bind("Active") %>' runat="server"><i class="glyphicon glyphicon-zoom-in icon-white"></i></asp:LinkButton>
                <asp:LinkButton ID="lnkEdit" class="btn btn-info" CommandName="EditRow" CommandArgument='<%# Bind("NewsId") %>' runat="server"><i class="glyphicon glyphicon-edit icon-white"></i>Edit</asp:LinkButton>
                <asp:LinkButton ID="lnkdelete" class="btn btn-danger" CommandName="DeleteRow" CommandArgument='<%# Bind("NewsId") %>' OnClientClick="return conformbox();" runat="server"><i class="glyphicon glyphicon-trash icon-white"></i>Delete</asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
    </ContentTemplate>
    <Triggers>
<asp:PostBackTrigger ControlID="btnsave" />
<asp:PostBackTrigger ControlID="btnupdate" />
</Triggers>
</asp:UpdatePanel>
</asp:Content>

