﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class rt_AboutUs : System.Web.UI.Page
{
    BL_AboutUs bl_aboutus = new BL_AboutUs();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
        BindAboutUs();
        }
    }

    private void BindAboutUs()
    {
        DataTable dt = bl_aboutus.SelectAboutUs();
        if (dt.Rows.Count > 0)
        {
            CKEditor1.Text = dt.Rows[0]["AboutUs"].ToString();
            id = Convert.ToInt32(dt.Rows[0]["AboutUsId"]);
            btnsave.Text = "Update";
        }
        else
        {
            btnsave.Text = "Save";
        }
       
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            bl_aboutus.Mode = "Insert";
            bl_aboutus.AboutUsId = 0;
        }
        else
        {
            bl_aboutus.Mode = "Update";
            bl_aboutus.AboutUsId = id;
        }
        bl_aboutus.AboutUs = CKEditor1.Text;
        bl_aboutus.AddAboutUs(bl_aboutus);
        BindAboutUs();
    }
}