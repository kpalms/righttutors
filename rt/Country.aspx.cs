﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class RT_Country : System.Web.UI.Page
{
    BL_Country bl_country = new BL_Country();
    static int id;

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            BindCountry();
            btnupdate.Enabled = false;
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        bl_country.Country = txtCountry.Text;
       DataTable dt= bl_country.SelectCountryName(bl_country);

       if (dt.Rows.Count == 0)
       {
           lblmsg.Visible = false;
           bl_country.Country = txtCountry.Text;
           bl_country.CountryId = 0;
           bl_country.Mode = "Insert";
           bl_country.AddCountry(bl_country);
           txtCountry.Text = "";
           BindCountry();
       }
       else
       {
           lblmsg.Visible = true;
           lblmsg.Text = "Already existing this country !";
       }
    }

    private void BindCountry()
    {
      DataTable dt= bl_country.SelectCountry();
      gvCountry.DataSource = dt;
      gvCountry.DataBind();
    }
    protected void gvCountry_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblCountry = (Label)row.FindControl("lblCountry");

            if (e.CommandName == "EditRow")
            {
                id = Convert.ToInt32(e.CommandArgument.ToString());
                txtCountry.Text = lblCountry.Text;
                btnupdate.Enabled = true;
                btnsave.Enabled = false;
            }
            if (e.CommandName == "DeleteRow")
            {
                bl_country.CountryId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_country.Country = "";
                bl_country.Mode = "Delete";
                bl_country.AddCountry(bl_country);
                BindCountry();
                btnupdate.Enabled = false;
                btnsave.Enabled = true;
                txtCountry.Text = "";
            }
        }
        catch
        {


        }
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        txtCountry.Text = "";
        btnupdate.Enabled = false;
        btnsave.Enabled = true;
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        //bl_country.Country = txtCountry.Text;
        //DataTable dt = bl_country.SelectCountryName(bl_country);
        //if (dt.Rows.Count == 0)
        //{
        //    lblmsg.Visible = false;
            bl_country.Country = txtCountry.Text;
            bl_country.CountryId = id;
            bl_country.Mode = "Update";
            bl_country.AddCountry(bl_country);
            txtCountry.Text = "";
            BindCountry();
            btnupdate.Enabled = false;
            btnsave.Enabled = true;
        //}
        //else
        //{
        //    lblmsg.Visible = true;
        //    lblmsg.Text = "Already existing this country !";
        //}
    }


    protected void gvCountry_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCountry.PageIndex = e.NewPageIndex;
        BindCountry();
    }
}