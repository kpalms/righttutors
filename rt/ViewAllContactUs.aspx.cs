﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class rt_ContactUs : System.Web.UI.Page
{
    BL_ContactUs bl_contactus = new BL_ContactUs();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindContactUs();
        }
    }


    private void BindContactUs()
    {
        DataTable dt = bl_contactus.SelectAllContactUs();
        gvcontactus.DataSource = dt;
        gvcontactus.DataBind();
    }


    protected void gvcontactus_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "DeleteRow")
            {
                bl_contactus.Topics = "";
                bl_contactus.Name = "";
                bl_contactus.EmailId = "";
                bl_contactus.Message = "";
                bl_contactus.Address = "";
                bl_contactus.DateTime = DateTime.Now;
                bl_contactus.ContactUsId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_contactus.Mode = "Delete";
                bl_contactus.MobileNo = "";
                bl_contactus.CountryId = 0;
                bl_contactus.StateId = 0;
                bl_contactus.CityId = 0;
                bl_contactus.AreaId = 0;
                bl_contactus.AddContactUs(bl_contactus);
            }
            BindContactUs();
        }
        catch
        {
        }
    }

    protected void gvcontactus_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvcontactus.PageIndex = e.NewPageIndex;
        BindContactUs();
    }
}