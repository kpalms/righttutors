﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
public partial class rt_Advertisement : System.Web.UI.Page
{
    static int id;
    static string imagename;
    BL_Advertisement bl_advertisement = new BL_Advertisement();
    Common common = new Common();
    protected void Page_Load(object sender, EventArgs e)
    {
        BindAdvertisement();
        btnupdate.Enabled = false;
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            Setfill();
            bl_advertisement.AddId = 0;
            bl_advertisement.Mode = "Insert";
            if (fileimage.HasFile)
            {
                string ext = Path.GetExtension(fileimage.FileName);
                string filename = common.GetImageName() + "" + ext;
                fileimage.PostedFile.SaveAs(Server.MapPath("Images/Advertisement/") + filename);
                bl_advertisement.ImageName = filename;
            }
            else
            {
                bl_advertisement.ImageName = "NoImage.jpg";
            }
            bl_advertisement.AddAdvertisement(bl_advertisement);
            BindAdvertisement();
            Clear();
            btnupdate.Enabled = false;
            btnsave.Enabled = true;
        }
        catch
        {
            
        }
    }

    private void Setfill()
    {
        bl_advertisement.Title = txttitle.Text;
        bl_advertisement.Description = txtdescription.Text;
        bl_advertisement.Url = txturl.Text;
    }

    private void Clear()
    {
        txturl.Text = "";
        txttitle.Text = "";
        txtdescription.Text = "";
        //ddlCountry.SelectedItem.Value = Convert.ToString(0);
    }

    private void BindAdvertisement()
    {
        DataTable dt = bl_advertisement.SelectAdvertisement();
        gvAdvertisement.DataSource = dt;
        gvAdvertisement.DataBind();
    }
    protected void gvAdvertisement_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            string sub;
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblTitle = (Label)row.FindControl("lblTitle");
            Label lblDescription = (Label)row.FindControl("lblDescription");
            Label lblUrl = (Label)row.FindControl("lblUrl");
            Label lblImageName = (Label)row.FindControl("lblImageName");
            if (e.CommandName == "EditRow")
            {
                id = Convert.ToInt32(e.CommandArgument.ToString());
                txttitle.Text = lblTitle.Text;
                txtdescription.Text = lblDescription.Text;
                txturl.Text=lblUrl.Text;
                sub = lblImageName.Text.Substring(21);
                imagename = sub;
                imgedit.Visible = true;
                imgedit.ImageUrl = lblImageName.Text;
                btnupdate.Enabled = true;
                btnsave.Enabled = false;
            }
            if (e.CommandName == "DeleteRow")
            {
                bl_advertisement.Title = "";
                bl_advertisement.Description = "";
                bl_advertisement.Url = "";
                bl_advertisement.AddId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_advertisement.Mode = "Delete";
                bl_advertisement.ImageName = "";
                bl_advertisement.AddAdvertisement(bl_advertisement);
                BindAdvertisement();
                imgedit.Visible = false;
                sub = lblImageName.Text.Substring(21);
                imagename = sub;
                if ("NoImage.jpg" != imagename)
                {
                    string imageFilePath = Server.MapPath(lblImageName.Text);
                    File.Delete(imageFilePath);
                }
                Clear();
                btnupdate.Enabled = false;
                btnsave.Enabled = true;
            }
        }
        catch
        {


        }
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        Clear();
        btnupdate.Enabled = false;
        btnsave.Enabled = true;
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {
            string filename;
            Setfill();
            bl_advertisement.AddId = id;
            bl_advertisement.Mode = "Update";
            if (fileimage.HasFile)
            {
                string ext = Path.GetExtension(fileimage.FileName);

                if ("NoImage.jpg" == imagename)
                {
                     filename = common.GetImageName() + "" + ext;
                }
                else
                {
                    string fn = Path.GetFileNameWithoutExtension(imagename);
                    filename = fn + "" + ext;
                    string imageFilePath = Server.MapPath("Images/Advertisement/" + imagename);
                    File.Delete(imageFilePath);
                }
                fileimage.PostedFile.SaveAs(Server.MapPath("Images/Advertisement/") + filename);
                bl_advertisement.ImageName = filename;
            }
            else
            {
                bl_advertisement.ImageName = imagename;
            }
            bl_advertisement.AddAdvertisement(bl_advertisement);
            BindAdvertisement();
            Clear();
            btnupdate.Enabled = false;
            btnsave.Enabled = true;
            imgedit.Visible = false;
        }
        catch
        {
            
        }

    }

    protected void gvAdvertisement_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvAdvertisement.PageIndex = e.NewPageIndex;
        BindAdvertisement();
    }

}