﻿<%@ Page Title="" Language="C#" MasterPageFile="~/rt/RTMasterPage.master" AutoEventWireup="true" CodeFile="ViewRegisterTeacher.aspx.cs" Inherits="rt_ViewRegisterTeacher" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<section id="contact">
      <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">View Register Teachers</h2>
              <span></span> 
            </div>
          </div>
       </div>
       <div class="row>
 <div class="col-xs-3">
<form class="form-inline">
  <div class="form-group">
    <div class="input-group">
    <div class="input-group-addon"><asp:LinkButton ID="lnkcancel" style="text-decoration:none" runat="server" 
            onclick="lnkcancel_Click">RT</asp:LinkButton></div>
      <asp:TextBox ID="txtsearch" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57'  placeholder="Search Tearch" runat="server"></asp:TextBox>
          <div class="input-group-addon"><asp:LinkButton ID="lnksearch" runat="server" onclick="lnksearch_Click" ValidationGroup="new"><span class="glyphicon glyphicon-search icon-white">Search</asp:LinkButton></div>

    </div>
  </div>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Please Enter Teacher Id" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtsearch"></asp:RequiredFieldValidator>
</form>
</div>
</div>
</div>
<div class="container">
       <div class="row" >
<asp:ListView ID="lstteacher" runat="server" GroupItemCount="6">
<EmptyDataTemplate>
No Data found.
</EmptyDataTemplate>
<LayoutTemplate>
<table id="groupPlaceholderContainer" runat="server" border="0" cellpadding="0" cellspacing="0"
width="100%">
<tr id="groupPlaceholder" runat="server">
</tr>
</table>
</LayoutTemplate>
<GroupTemplate>
<tr id="itemPlaceholderContainer" runat="server" style="height: 100px;">
<td id="itemPlaceholder" runat="server">
</td>
</tr>
</GroupTemplate>
<ItemTemplate>           
<td width="20%" align="left" height="200px">
<ul>
<li>
<a href="TeacherDeatils.aspx?key=<%# Eval("TeachersId") %>&Status=RT" style="text-decoration:none">
<img src='<%#Eval("ImageName")  %>' class="img-responsive img-thumbnail" alt="" height="100px" width="100px"><br />
<asp:Label ID="lbltids" runat="server" Text='<%#Eval("TID")  %>'></asp:Label><br />
<asp:Label ID="lblName" runat="server" Text='<%#Eval("Name")  %>'></asp:Label><br />
<asp:Label ID="lblMobileNo" runat="server" Text='<%#Eval("MobileNo")  %>'></asp:Label>
</a>
</li>
</ul>
</td>                
</ItemTemplate>
</asp:ListView>
   </div>
      </div>
    </section>
</asp:Content>

