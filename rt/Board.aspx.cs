﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class RT_Board : System.Web.UI.Page
{
    BL_Board bl_board = new BL_Board();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindBoard();
            btnupdate.Enabled = false;
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        bl_board.Board = txtBoard.Text;
       DataTable dt= bl_board.SelectBoardName(bl_board);
       if (dt.Rows.Count == 0)
       {
           lblmsg.Visible = false;
           bl_board.BoardId = 0;
           bl_board.Mode = "Insert";
           bl_board.AddBoard(bl_board);
           txtBoard.Text = "";
           BindBoard();
       }
       else
       {
           lblmsg.Visible = true;
           lblmsg.Text = "Already existing this country !";
       }


    }

    private void BindBoard()
    {
        DataTable dt = bl_board.SelectBoard();
        gvBoard.DataSource = dt;
        gvBoard.DataBind();
    }
    protected void gvBoard_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            lblmsg.Visible = false;
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblBoard = (Label)row.FindControl("lblBoard");

            if (e.CommandName == "EditRow")
            {
                id = Convert.ToInt32(e.CommandArgument.ToString());
                txtBoard.Text = lblBoard.Text;
                btnupdate.Enabled = true;
                btnsave.Enabled = false;
            }
            if (e.CommandName == "DeleteRow")
            {
                bl_board.BoardId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_board.Board = "";
                bl_board.Mode = "Delete";
                bl_board.AddBoard(bl_board);
                BindBoard();
                btnupdate.Enabled = false;
                btnsave.Enabled = true;
                txtBoard.Text = "";
            }
        }
        catch
        {


        }
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        lblmsg.Visible = false;
        txtBoard.Text = "";
        btnupdate.Enabled = false;
        btnsave.Enabled = true;
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        lblmsg.Visible = false;
        bl_board.Board = txtBoard.Text;
        bl_board.BoardId = id;
        bl_board.Mode = "Update";
        bl_board.AddBoard(bl_board);
        txtBoard.Text = "";
        BindBoard();
        btnupdate.Enabled = false;
        btnsave.Enabled = true;
    }


    protected void gvBoard_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvBoard.PageIndex = e.NewPageIndex;
        BindBoard();
    }
}