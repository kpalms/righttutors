﻿<%@ Page Title="" Language="C#" MasterPageFile="~/rt/RTMasterPage.master" AutoEventWireup="true" CodeFile="TeacherDeatils.aspx.cs" Inherits="rt_TeacherDeatils" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">
    
fieldset.scheduler-border {
    border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
}

legend.scheduler-border {
    font-size: 1.2em !important;
    font-weight: bold !important;
    text-align: left !important;
    width:inherit; /* Or auto */
    padding:0 10px; /* To give a bit of padding on the left and right */
    border-bottom:none;

}


</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<section id="contact">
      <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">Teacher Details</h2>
              <span></span> 
            </div>
          </div>
       </div>
       <div class="row" >

<div class="col-lg-6 col-md-6 col-sm-6">
              <div class="single_sidebar"><h2 align="center">TEACHER PROFILE</h2></div>
<div align="center"> <asp:Image ID="imgprofile" class="img-responsive img-thumbnail" runat="server" Height="100px" Width="100px" /></div>
  <br />
 <fieldset class="scheduler-border">
    <legend class="scheduler-border">Personal Information</legend>
    <table class="table table-striped">
     <tr>
    <td>Teacher Id</td>
    <td><asp:Label ID="lblteacherid" runat="server"></asp:Label></td>
    </tr>

    <tr>
    <td>Full Name</td>
    <td><asp:Label ID="lblfullname" runat="server"></asp:Label></td>
    </tr>
    
     <tr>
    <td>Date of Birth</td>
    <td> <asp:Label ID="lbldob" runat="server"></asp:Label></td>
    </tr>

     <tr>
    <td>Gender</td>
    <td><asp:Label ID="lblgender" runat="server"></asp:Label></td>
    </tr>

     <tr>
    <td>Contact No</td>
    <td><asp:Label ID="lblmobileno" runat="server"></asp:Label></td>
    </tr>

     <tr>
    <td>Email Id</td>
    <td> <asp:Label ID="lblemail" runat="server"></asp:Label></td>
    </tr>

     <tr>
    <td>Address</td>
    <td><asp:Label ID="lbladdress" runat="server"></asp:Label></td>
    </tr>
    </table>
   </fieldset>

   <fieldset class="scheduler-border">
    <legend class="scheduler-border">Education Information</legend>
     <table class="table table-striped">
    <tr>
    <td>Education</td>
    <td><asp:Label ID="lblEducation" runat="server"></asp:Label></td>
    </tr>
  
   <tr>
    <td>Exprience</td>
    <td><asp:Label ID="lblExprience" runat="server"></asp:Label></td>
    </tr>

     <tr>
    <td>Tag Lines</td>
    <td><asp:Label ID="lbltagline" runat="server"></asp:Label></td>
    </tr>

    <tr>
    <td>About Me</td>
    <td><asp:Label ID="lblaboutme" runat="server"></asp:Label></td>
    </tr>
    </table>
      </fieldset>

     <table width="100%" cellpadding="0px" cellspacing="0px">
    <tr>
    <td style="width:50%"><asp:LinkButton ID="lnkactive" class="wpcf7-active"  runat="server" onclick="lnkactive_Click"></asp:LinkButton></td>
   <td style="width:50%;float:right"><asp:LinkButton ID="lnkdelete"
           class="wpcf7-delete" OnClientClick="return conformbox();" Visible="false" runat="server" 
           onclick="lnkdelete_Click"><i class="glyphicon glyphicon-trash icon-white"></i>Delete</asp:LinkButton></td>
    </tr>
    </table>
    </div>


 <div class="col-lg-6 col-md-6 col-sm-6">
 <div style="float:left;height:890px;width:470px;overflow-y :auto">
              <div class="single_sidebar"><h2 align="center">SELECTED SUBJECT</h2></div>
              <br />
              <br />
              <br />
              <asp:Repeater ID="rptteacherboardclass" runat="server" 
                  onitemdatabound="rptteacherboardclass_ItemDataBound">
              <ItemTemplate>
              <asp:Label ID="lblboard" runat="server" Text='<%# Bind("BoardId") %>' Visible="false"></asp:Label>
              <asp:Label ID="lblclass" runat="server" Text='<%# Bind("ClassId") %>' Visible="false"></asp:Label>

               <asp:Repeater ID="rptsubject" runat="server">
              <ItemTemplate>
               <div class="panel panel-danger">
                    <div class="panel-heading">
                    <table border="0px" width="100%" cellpadding="0px" cellspacing="0px">
            <tr>
            <td> 
                    <h3 class="panel-title"> <asp:Label ID="lblboard" runat="server" Text='<%# Bind("Board") %>'></asp:Label> </h3></td>
                   <td><h3  class="panel-title" style="float:right"> <asp:Label ID="lblclass" runat="server" Text='<%# Bind("Class") %>'></asp:Label></h3>
                   </td>
                   </tr>
                   </table>
                    </div>
                    <div class="panel-body">  <asp:Label ID="lblsubject" runat="server" Text='<%# Bind("Subjects") %>'></asp:Label> </div>
                </div>
    
               </ItemTemplate>
              </asp:Repeater>
              <br>
              </ItemTemplate>
              </asp:Repeater>


                      <div class="single_sidebar"><h2 align="center">All Posts</h2></div>
                      <asp:GridView ID="gvtodyapost" runat="server" class="table table-hover" AutoGenerateColumns="False" EmptyDataText="No records Found"
AllowPaging="True" onpageindexchanging="gvtodyapost_PageIndexChanging" PageSize="5"        onrowcommand="gvtodyapost_RowCommand">
    <Columns>
        <asp:TemplateField HeaderText="AddId" Visible="false">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("TodayPostId") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblTodayPostId" runat="server" Text='<%# Bind("TodayPostId") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="BoardId" Visible="false">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("BoardId") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblBoardId" runat="server" Text='<%# Bind("BoardId") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
         <asp:TemplateField HeaderText="Board">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Board") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblBoard" runat="server" Text='<%# Bind("Board") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="ClassId" Visible="false">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("ClassId") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblClassId" runat="server" Text='<%# Bind("ClassId") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
         <asp:TemplateField HeaderText="Class">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Class") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblClass" runat="server" Text='<%# Bind("Class") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Title">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Title") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Description" >
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
         <asp:TemplateField HeaderText="Image" >
            <EditItemTemplate>
            </EditItemTemplate>
            <ItemTemplate>
               <asp:Label ID="lblImage" runat="server" Text='<%# Bind("Image") %>' Visible="false"></asp:Label>
               <img src="<%#Eval("Image")%>" alt="" height="30px" width="30px">
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Action">
            <ItemTemplate>
            <asp:LinkButton ID="lnkactive" CommandName="Active" class="btn btn-success" CommandArgument='<%# Bind("TodayPostId") %>' Text='<%# Bind("Active") %>' runat="server"></asp:LinkButton>
                <asp:LinkButton ID="lnkdelete" CommandName="DeleteRow" class="btn btn-danger" CommandArgument='<%# Bind("TodayPostId") %>' OnClientClick="return conformbox();" runat="server"><i class="glyphicon glyphicon-trash icon-white"></i>Delete</asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
   




    <div class="single_sidebar"><h2 align="center">SEND MESSAGE</h2></div>
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
 <ContentTemplate>
    <asp:TextBox ID="txtmsg" runat="server" TextMode="MultiLine" class="wp-form-control wpcf7-textarea" placeholder="Message"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter Message" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtmsg"></asp:RequiredFieldValidator>
    <br />
    <div style="margin-left:80px">
     <asp:Button ID="btnsubmit" runat="server" Text="Submit"  ValidationGroup="new"
        onclick="btnsubmit_Click" class="wpcf7-submit"/>
        <asp:Button ID="btnupdate" runat="server" ValidationGroup="new" Text="Update" class="wpcf7-updated" onclick="btnupdate_Click" />
        <asp:Button ID="btnclear" runat="server" Text="Clear" class="wpcf7-clear" onclick="btnclear_Click" /></div>
        <br />
  <asp:GridView ID="GVNOTIFICATION" class="table table-hover" onpageindexchanging="GVNOTIFICATION_PageIndexChanging" PageSize="5"  onrowcommand="GVNOTIFICATION_RowCommand" AutoGenerateColumns="False" EmptyDataText="No records Found" runat="server">
                    <Columns>
            <asp:TemplateField HeaderText="NotificationId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("NotificationId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblNotificationId" runat="server" Text='<%# Bind("NotificationId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="DateTime">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("DateTime") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblDateTime" runat="server" Text='<%# Bind("DateTime") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Message">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Message") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblMessage" runat="server" Text='<%# Bind("Message") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
            <ItemTemplate>
             <asp:LinkButton ID="lnkEdit" class="btn btn-info" CommandName="EditRow" CommandArgument='<%# Bind("NotificationId") %>' runat="server"><i class="glyphicon glyphicon-edit icon-white"></i>Edit</asp:LinkButton>
                <asp:LinkButton ID="lnkdelete" CommandName="DeleteRow" class="btn btn-danger" CommandArgument='<%# Bind("NotificationId") %>' OnClientClick="return conformbox();" runat="server"><i class="glyphicon glyphicon-trash icon-white"></i>Delete</asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
                       </Columns>
                    </asp:GridView>
                    </ContentTemplate>
</asp:UpdatePanel>


 <div class="single_sidebar"><h2 align="center">STUDENTS TESTIMONIAL</h2></div>
               <asp:Repeater ID="rptstudenttesto" runat="server">
              <ItemTemplate>
               <table width="100%" cellpadding="0px" cellspacing="0px">
              <tr>
              <td>
              
             <a href="StudentDetails.aspx?key=<%# Eval("StudentsId") %>&Status=UVS" style="text-decoration:none">
<img src="<%#Eval("ImageName") %>" class="img-responsive img-thumbnail" alt="" height="50px" width="50px">
<asp:Label ID="lblSName" runat="server" ForeColor="Blue" Font-Bold="true" Text='<%#Eval("SName")  %>'></asp:Label></a>
<asp:Label ID="lblWrites" runat="server" Text='<%#Eval("Writes")  %>'></asp:Label>
 </td>
              </tr>
              </table>
              <br />
              </ItemTemplate>
              </asp:Repeater>
                    </div> 
                      </div> 
      </div>
    </section>  
</asp:Content>

