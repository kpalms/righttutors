﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RT/RTMasterPage.master" AutoEventWireup="true" CodeFile="Board.aspx.cs" Inherits="RT_Board" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <section id="contact">
      <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">Board</h2>
              <span></span> 
            </div>
          </div>
       </div>
       <div class="row" >



<div class="container" id="parent">

  <div class="row">
    <div class="col-lg-12">
      <div class="row ">

        <div class="col-lg-12" id="child" >
 <div align="center" >
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
 <ContentTemplate>
 <asp:Label ID="lblmsg" runat="server" ForeColor="Red" Visible="false"></asp:Label><br />
<asp:TextBox ID="txtBoard" runat="server" class="wp-form-control wpcf7-text" placeholder="Enter Board"></asp:TextBox>
          <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please Enter Board" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtBoard"></asp:RequiredFieldValidator>

<br />
<asp:Button ID="btnsave" runat="server" ValidationGroup="new" class="wpcf7-submit" Text="Save" onclick="btnsave_Click" />
<asp:Button ID="btnupdate" runat="server" ValidationGroup="new" class="wpcf7-updated" Text="Update" onclick="btnupdate_Click" />
<asp:Button ID="btnclear" runat="server" Text="Clear" class="wpcf7-clear" onclick="btnclear_Click" />
<br />
<br />
<asp:GridView ID="gvBoard" runat="server" class="table table-hover" AutoGenerateColumns="False" EmptyDataText="No records Found" 
 AllowPaging="True" onpageindexchanging="gvBoard_PageIndexChanging" PageSize="20" onrowcommand="gvBoard_RowCommand">
    <Columns>
        <asp:TemplateField HeaderText="BoardId" Visible="false">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("BoardId") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblBoardId" runat="server" Text='<%# Bind("BoardId") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Board">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Board") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblBoard" runat="server" Text='<%# Bind("Board") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Action">
            <ItemTemplate>
                <asp:LinkButton ID="lnkEdit" CommandName="EditRow" class="btn btn-info" CommandArgument='<%# Bind("BoardId") %>' runat="server"><i class="glyphicon glyphicon-edit icon-white"></i>Edit</asp:LinkButton>
                <asp:LinkButton ID="lnkdelete" CommandName="DeleteRow" class="btn btn-danger"  CommandArgument='<%# Bind("BoardId") %>' OnClientClick="return conformbox();" runat="server"><i class="glyphicon glyphicon-trash icon-white"></i>Delete</asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>

</ContentTemplate>
</asp:UpdatePanel>
</div>
        </div>
      </div>
    </div>
  </div>



       </div>
      </div>
    </section>
</asp:Content>

