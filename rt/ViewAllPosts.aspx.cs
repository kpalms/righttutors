﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Data.SqlClient;
public partial class rt_ToDayPost : System.Web.UI.Page
{
    static int id;
    static string imagename;
    BL_ToDayPost bl_todaypost = new BL_ToDayPost();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindToDayPost();
        }
    }


    private void BindToDayPost()
    {
        DataTable dt = bl_todaypost.SelectAllTodayPost(bl_todaypost);
       gvtodyapost.DataSource = dt;
       gvtodyapost.DataBind();
    }

    
    protected void gvtodyapost_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            string sub;
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblImage = (Label)row.FindControl("lblImage");
            LinkButton lnkactive = (LinkButton)row.FindControl("lnkactive");

            if (e.CommandName == "DeleteRow")
            {
                bl_todaypost.Title = "";
                bl_todaypost.Description = "";
                bl_todaypost.Date = DateTime.Now;
                bl_todaypost.TodayPostId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_todaypost.Mode = "Delete";
                bl_todaypost.Image = "";
                bl_todaypost.Active = 0;
                bl_todaypost.BoardId = 0;
                bl_todaypost.ClassId = 0;
                bl_todaypost.TeachersId = 0;
                bl_todaypost.AddTodayPost(bl_todaypost);
                BindToDayPost();
                sub = lblImage.Text.Substring(17);
                imagename = sub;
                if ("NoImage.png" != imagename)
                {
                    string imageFilePath = Server.MapPath(lblImage.Text);
                    File.Delete(imageFilePath);
                }
                
            }

            if (e.CommandName == "Active")
            {
                bl_todaypost.Title = "";
                bl_todaypost.Description = "";
                bl_todaypost.Date = DateTime.Now;
                bl_todaypost.TodayPostId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_todaypost.Mode = "Active";
                bl_todaypost.Image = "";
                bl_todaypost.BoardId = 0;
                bl_todaypost.ClassId = 0;
                bl_todaypost.TeachersId = 0;

                if (lnkactive.Text == "Deactive")
                {
                    bl_todaypost.Active = 0;
                }
                else
                {
                    bl_todaypost.Active = 1;
                }

                bl_todaypost.AddTodayPost(bl_todaypost);

            }
            BindToDayPost();

        }
        catch
        {


        }
    }

    protected void gvtodyapost_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvtodyapost.PageIndex = e.NewPageIndex;
        BindToDayPost();
    }
}