﻿<%@ Page Title="" Language="C#" MasterPageFile="~/rt/RTMasterPage.master" AutoEventWireup="true" CodeFile="StudentDetails.aspx.cs" Inherits="rt_StudentDetails" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">
    
fieldset.scheduler-border {
    border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
}

legend.scheduler-border {
    font-size: 1.2em !important;
    font-weight: bold !important;
    text-align: left !important;
    width:inherit; /* Or auto */
    padding:0 10px; /* To give a bit of padding on the left and right */
    border-bottom:none;

}

</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<section id="contact">
      <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">Student Details</h2>
              <span></span> 
            </div>
          </div>
       </div>
       <div class="row" >
      
        <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="single_sidebar"><h2 align="center">STUDENT PROFILE</h2></div>
<div align="center"><asp:Image ID="imgprofile" runat="server" class="img-responsive img-thumbnail"  Height="100px" Width="100px"/></div>
 <br />
 <fieldset class="scheduler-border">
    <legend class="scheduler-border">Personal Information</legend>
    <table class="table table-striped">

     <tr>
    <td>Student Id</td>
    <td><asp:Label ID="lblstudentid" runat="server"></asp:Label></td>
    </tr>

    <tr>
    <td>Full Name</td>
    <td><asp:Label ID="lblfullname" runat="server"></asp:Label></td>
    </tr>
    
     <tr>
    <td>Date of Birth</td>
    <td> <asp:Label ID="lbldob" runat="server"></asp:Label></td>
    </tr>

     <tr>
    <td>Gender</td>
    <td><asp:Label ID="lblgender" runat="server"></asp:Label></td>
    </tr>

     <tr>
    <td>Contact No</td>
    <td><asp:Label ID="lblmobileno" runat="server"></asp:Label></td>
    </tr>

     <tr>
    <td>Email Id</td>
    <td> <asp:Label ID="lblemail" runat="server"></asp:Label></td>
    </tr>

     <tr>
    <td>Address</td>
    <td><asp:Label ID="lbladdress" runat="server"></asp:Label></td>
    </tr>
    </table>
   </fieldset>

   <fieldset class="scheduler-border">
    <legend class="scheduler-border">Education Information</legend>
     <table class="table table-striped">
    <tr>
    <td>Board</td>
    <td><asp:Label ID="lblboard" runat="server"></asp:Label></td>
    </tr>
  
   <tr>
    <td>Class</td>
    <td><asp:Label ID="lblclass" runat="server"></asp:Label></td>
    </tr>

     <tr>
    <td>Subjects</td>
    <td><asp:Label ID="lblsubject" runat="server"></asp:Label></td>
    </tr>
    </table>
      </fieldset>

    <table width="100%" cellpadding="0px" cellspacing="0px">
    <tr>
    <td style="width:50%"><asp:LinkButton ID="lnkactive" class="wpcf7-active"  runat="server" onclick="lnkactive_Click"></asp:LinkButton></td>
   <td style="width:50%;float:right"><asp:LinkButton ID="lnkdelete"
           class="wpcf7-delete" OnClientClick="return conformbox();" Visible="false" runat="server" 
           onclick="lnkdelete_Click"><i class="glyphicon glyphicon-trash icon-white"></i>Delete</asp:LinkButton></td>
    </tr>
    </table>
 
    </div>


 <div class="col-lg-6 col-md-6 col-sm-6">
<div class="single_sidebar"><h2 align="center">SUGGESTED TEACHERS</h2></div>
 <%--<div style="float:left;height:700px;width:470px;overflow-y :auto">--%>
<asp:ListView ID="lstteacher" runat="server" GroupItemCount="3">
<EmptyDataTemplate>
No Data found.
</EmptyDataTemplate>
<LayoutTemplate>
<table id="groupPlaceholderContainer" runat="server" border="0" cellpadding="0" cellspacing="0"
width="100%">
<tr id="groupPlaceholder" runat="server">
</tr>
</table>
</LayoutTemplate>
<GroupTemplate>
<tr id="itemPlaceholderContainer" runat="server" style="height: 100px;">
<td id="itemPlaceholder" runat="server">
</td>
</tr>
</GroupTemplate>
<ItemTemplate>           
<td width="50%" align="left" height="200px">
<ul>
<li>
<a href="TeacherDeatils.aspx?key=<%# Eval("TeachersId") %>&Status=UVT" style="text-decoration:none">
<img src='<%#Eval("ImageName")  %>' class="img-responsive img-thumbnail" alt="" height="100px" width="100px"><br />
<asp:Label ID="lblName" runat="server" Text='<%#Eval("Name")  %>'></asp:Label><br />
<asp:Label ID="lblMobileNo" runat="server" Text='<%#Eval("MobileNo")  %>'></asp:Label>
</a>
</li>
</ul>
</td>                
</ItemTemplate>
</asp:ListView>
<%--  </div>--%>


<div class="single_sidebar"><h2 align="center">TEACHERS TESTIMONIALS</h2></div>

 <asp:Repeater ID="rptteachertesto" runat="server">
              <ItemTemplate>
              <table width="100%" cellpadding="0px" cellspacing="0px">
              <tr>
              <td>
              
              
               <a href="TeacherDeatils.aspx?key=<%# Eval("TeachersId") %>&Status=UVT" style="text-decoration:none">
              <img src='<%#Eval("ImageName")  %>' class="img-responsive img-thumbnail" alt="" height="50px" width="50px">
              <asp:Label ID="lbltname" ForeColor="Blue" Font-Bold="true" runat="server" Text='<%#Eval("TName")  %>'></asp:Label></a>
<asp:Label ID="lblWrites" runat="server" Text='<%#Eval("Writes")  %>'></asp:Label>
             
             </td>
              </tr>
              </table>
              <br />
              </ItemTemplate>
              </asp:Repeater>
  </div>

      </div>
    </section>
</asp:Content>

