﻿<%@ Page Title="" Language="C#" MasterPageFile="~/rt/RTMasterPage.master" AutoEventWireup="true" CodeFile="Advertisement.aspx.cs" Inherits="rt_Advertisement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <section id="contact">
      <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">Advertisement</h2>
              <span></span> 
            </div>
          </div>
       </div>
       <div class="row" >
<div class="container" id="parent">
  <div class="row">
    <div class="col-lg-12">
      <div class="row ">
        <div class="col-lg-12" id="child" >
 <div align="center" >
 <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
 <ContentTemplate>
<asp:TextBox ID="txttitle" runat="server" class="wp-form-control wpcf7-text" placeholder="Enter Title"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please Enter Title" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txttitle"></asp:RequiredFieldValidator>

<br />
<asp:TextBox ID="txtdescription" runat="server" class="wp-form-control wpcf7-text" placeholder="Enter Description" TextMode="MultiLine"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter Description" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtdescription"></asp:RequiredFieldValidator>

<br />
<asp:TextBox ID="txturl" runat="server" class="wp-form-control wpcf7-text" placeholder="Enter Url"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please Enter Url" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txturl"></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ID="regUrl" runat="server" ControlToValidate="txturl" ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?" Text="Enter a valid URL" ValidationGroup="new" CssClass="errormesg" ForeColor="Red"/>   
<br />
<asp:FileUpload ID="fileimage" runat="server"/>
 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please Select Image File" ControlToValidate="fileimage" ValidationGroup="new"
    runat="server" Display="Dynamic" ForeColor="Red" />
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.bmp|.jpeg)$"
    ControlToValidate="fileimage" ValidationGroup="new" runat="server" ForeColor="Red" ErrorMessage="Please select a valid png,jpg,gif,bmp,jpeg file."
    Display="Dynamic" />
    <sapm style="font-size:10px;color:red" >(upload only png,jpg,gif,bmp,jpeg)</sapm>
<br />
 <asp:Image ID="imgedit" runat="server" Visible="false" Width="50px" Height="50px"/>
<asp:Button ID="btnsave" runat="server" class="wpcf7-submit" ValidationGroup="new" Text="Save" onclick="btnsave_Click" />
<asp:Button ID="btnupdate" runat="server" class="wpcf7-updated" ValidationGroup="new" Text="Update" onclick="btnupdate_Click" />
<asp:Button ID="btnclear" runat="server" class="wpcf7-clear" Text="Clear" onclick="btnclear_Click" />
<br />
<br />
<br />
<asp:GridView ID="gvAdvertisement" runat="server" class="table table-hover" AutoGenerateColumns="False" EmptyDataText="No records Found"
AllowPaging="True" onpageindexchanging="gvAdvertisement_PageIndexChanging" PageSize="20"        onrowcommand="gvAdvertisement_RowCommand">
    <Columns>
        <asp:TemplateField HeaderText="AddId" Visible="false">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("AddId") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblAddId" runat="server" Text='<%# Bind("AddId") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Title">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Title") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Description" >
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Url">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Url") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblUrl" runat="server" Text='<%# Bind("Url") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
         <asp:TemplateField HeaderText="ImageName" >
            <EditItemTemplate>
                <%--<asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ImageName") %>'></asp:TextBox>--%>
            </EditItemTemplate>
            <ItemTemplate>
               <asp:Label ID="lblImageName" runat="server" Text='<%# Bind("ImageName") %>' Visible="false"></asp:Label>
               <img src="<%#Eval("ImageName")%>" alt="" height="100px" width="100px">
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Action">
            <ItemTemplate>
                <asp:LinkButton ID="lnkEdit"  CommandName="EditRow" CommandArgument='<%# Bind("AddId") %>' runat="server" class="btn btn-info"><i class="glyphicon glyphicon-edit icon-white"></i>Edit</asp:LinkButton>
                <asp:LinkButton ID="lnkdelete"  CommandName="DeleteRow" CommandArgument='<%# Bind("AddId") %>' OnClientClick="return conformbox();" runat="server" class="btn btn-danger"><i class="glyphicon glyphicon-trash icon-white"></i>Delete</asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>

</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="btnsave" />
</Triggers>
</asp:UpdatePanel>

</div>
        </div>
      </div>
    </div>
  </div>



       </div>
      </div>
    </section>
</asp:Content>

