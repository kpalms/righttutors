﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class rt_ViewDeactiveStudents : System.Web.UI.Page
{
    BL_Teachers bl_teacher = new BL_Teachers();
    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable dt = bl_teacher.SelectTeachersDeactive(bl_teacher);
        lstteacher.DataSource = dt;
        lstteacher.DataBind();
    }
}