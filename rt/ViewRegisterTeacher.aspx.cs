﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class rt_ViewRegisterTeacher : System.Web.UI.Page
{
    BL_Teachers bl_teacher = new BL_Teachers();
    protected void Page_Load(object sender, EventArgs e)
    {
        BindTeachers();
    }

    private void BindTeachers()
    {
        DataTable dt = bl_teacher.SelectTeachers(bl_teacher);
        lstteacher.DataSource = dt;
        lstteacher.DataBind();
    }

    protected void lnksearch_Click(object sender, EventArgs e)
    {
        if (txtsearch.Text != "")
        {
            bl_teacher.TeachersId = Convert.ToInt32(txtsearch.Text);
            DataTable dt = bl_teacher.SearchTeachers(bl_teacher);
            lstteacher.DataSource = dt;
            lstteacher.DataBind();
        }
        else
        {
            BindTeachers();
            txtsearch.Text = "";
        }
    }
    protected void lnkcancel_Click(object sender, EventArgs e)
    {
        BindTeachers();
        txtsearch.Text = "";
    }
}