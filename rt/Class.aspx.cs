﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class RT_Class : System.Web.UI.Page
{
    BL_Class bl_class = new BL_Class();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindClass();
            btnupdate.Enabled = false;
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        bl_class.Class = txtClass.Text;

        DataTable dt = bl_class.SelectClassName(bl_class);
        if(dt.Rows.Count == 0)
        {
            lblmsg.Visible = false;
        bl_class.ClassId = 0;
        bl_class.Mode = "Insert";
        bl_class.AddClass(bl_class);
        txtClass.Text = "";
        BindClass();
        }
        else
        {
            lblmsg.Visible = true;
            lblmsg.Text = "Already existing this country !";
        }
    }

    private void BindClass()
    {
        DataTable dt = bl_class.SelectClass();
        gvClass.DataSource = dt;
        gvClass.DataBind();
    }
    protected void gvClass_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            lblmsg.Visible = false;
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblClass = (Label)row.FindControl("lblClass");

            if (e.CommandName == "EditRow")
            {
                id = Convert.ToInt32(e.CommandArgument.ToString());
                txtClass.Text = lblClass.Text;
                btnupdate.Enabled = true;
                btnsave.Enabled = false;
            }
            if (e.CommandName == "DeleteRow")
            {
                bl_class.ClassId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_class.Class = "";
                bl_class.Mode = "Delete";
                bl_class.AddClass(bl_class);
                BindClass();
                btnupdate.Enabled = false;
                btnsave.Enabled = true;
                txtClass.Text = "";
            }
        }
        catch
        {


        }
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        lblmsg.Visible = false;
        txtClass.Text = "";
        btnupdate.Enabled = false;
        btnsave.Enabled = true;
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        lblmsg.Visible = false;
        bl_class.Class = txtClass.Text;
        bl_class.ClassId = id;
        bl_class.Mode = "Update";
        bl_class.AddClass(bl_class);
        txtClass.Text = "";
        BindClass();
        btnupdate.Enabled = false;
        btnsave.Enabled = true;
    }

    protected void gvClass_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvClass.PageIndex = e.NewPageIndex;
        BindClass();
    }
}