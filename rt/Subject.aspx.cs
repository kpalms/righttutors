﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class RT_Subject : System.Web.UI.Page
{
    static int id;
    BL_Subject bl_subject = new BL_Subject();
    Common common = new Common();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            common.BindBoard(ddlBoard);
            common.BindClass(ddlClass);
            BindSubject();
            btnupdate.Enabled = false;
        }
    }


    protected void btnsave_Click(object sender, EventArgs e)
    {
        bl_subject.Subject = txtSubject.Text;
        bl_subject.BoardId = Convert.ToInt32(ddlBoard.SelectedItem.Value);
        bl_subject.ClassId = Convert.ToInt32(ddlClass.SelectedItem.Value);
        DataTable dt = bl_subject.SelectSubjectName(bl_subject);
        if(dt.Rows.Count == 0)
        {
         lblmsg.Visible = false;
        bl_subject.SubjectId = 0;
        bl_subject.Mode = "Insert";
        bl_subject.AddSubject(bl_subject);
        BindSubject();
        Clear();
        btnupdate.Enabled = false;
        btnsave.Enabled = true;
        }
        else
        {
            lblmsg.Visible = true;
            lblmsg.Text = "Already existing this country !";
        }


    }

    private void Clear()
    {
        txtSubject.Text = "";
    }

    private void BindSubject()
    {
        bl_subject.BoardId = Convert.ToInt32(ddlBoard.SelectedItem.Value);
        bl_subject.ClassId = Convert.ToInt32(ddlClass.SelectedItem.Value);
        DataTable dt = bl_subject.SelectSubject(bl_subject);
        gvSubject.DataSource = dt;
        gvSubject.DataBind();
    }
    protected void gvSubject_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            lblmsg.Visible = false;

            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblBoardId = (Label)row.FindControl("lblBoardId");
            Label lblClassId = (Label)row.FindControl("lblClassId");
            Label lblSubject = (Label)row.FindControl("lblSubject");

            if (e.CommandName == "EditRow")
            {
                common.BindBoard(ddlBoard);
                common.BindClass(ddlClass);
                id = Convert.ToInt32(e.CommandArgument.ToString());
                ddlBoard.Text = lblBoardId.Text;
                ddlClass.Text = lblClassId.Text;
                txtSubject.Text = lblSubject.Text;
                btnupdate.Enabled = true;
                btnsave.Enabled = false;
            }
            if (e.CommandName == "DeleteRow")
            {
                bl_subject.Subject = "";
                bl_subject.BoardId = 0;
                bl_subject.ClassId = 0;
                bl_subject.SubjectId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_subject.Mode = "Delete";
                bl_subject.AddSubject(bl_subject);
                BindSubject();
                    Clear();
                btnupdate.Enabled = false;
                btnsave.Enabled = true;
            }
        }
        catch
        {


        }
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        lblmsg.Visible = false;
        Clear();
        ddlBoard.SelectedIndex = 0;
        ddlClass.SelectedIndex = 0;
        btnupdate.Enabled = false;
        btnsave.Enabled = true;
        BindSubject();
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        lblmsg.Visible = false;
       bl_subject.Subject = txtSubject.Text;
        bl_subject.BoardId = Convert.ToInt32(ddlBoard.SelectedItem.Value);
        bl_subject.ClassId = Convert.ToInt32(ddlClass.SelectedItem.Value);
        bl_subject.SubjectId = id;
        bl_subject.Mode = "Update";
        bl_subject.AddSubject(bl_subject);
        BindSubject();
        Clear();
        btnupdate.Enabled = false;
        btnsave.Enabled = true;
    }

    protected void gvSubject_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSubject.PageIndex = e.NewPageIndex;
        BindSubject();
    }

    protected void ddlClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindSubject();
    }
    protected void ddlBoard_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindSubject();
    }
}