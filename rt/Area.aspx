﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RT/RTMasterPage.master" AutoEventWireup="true" CodeFile="Area.aspx.cs" Inherits="RT_Area" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<section id="contact">
      <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">Area</h2>
              <span></span> 
            </div>
          </div>
       </div>
       <div class="row" >



<div class="container" id="parent">

  <div class="row">
    <div class="col-lg-12">
      <div class="row ">

        <div class="col-lg-12" id="child" >
 <div align="center" >
 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
 <ContentTemplate>
  <asp:Label ID="lblmsg" runat="server" ForeColor="Red" Visible="false"></asp:Label><br />
<asp:DropDownList ID="ddlCountry" runat="server" class="wp-form-control wpcf7-text"
        AutoPostBack="True" onselectedindexchanged="ddlCountry_SelectedIndexChanged">
    </asp:DropDownList>
    <asp:RequiredFieldValidator ControlToValidate="ddlCountry" ID="RequiredFieldValidator5"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a Country"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
    <br />
<asp:DropDownList ID="ddlState" runat="server" class="wp-form-control wpcf7-text"
        AutoPostBack="True" onselectedindexchanged="ddlState_SelectedIndexChanged">
    </asp:DropDownList>
    <asp:RequiredFieldValidator ControlToValidate="ddlState" ID="RequiredFieldValidator1"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a State"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
    <br />
<asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" class="wp-form-control wpcf7-text"
         onselectedindexchanged="ddlCity_SelectedIndexChanged">
    </asp:DropDownList>
    <asp:RequiredFieldValidator ControlToValidate="ddlCity" ID="RequiredFieldValidator2"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a City"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
    <br />
<asp:TextBox ID="txtArea" runat="server" class="wp-form-control wpcf7-text" placeholder="Enter Area"></asp:TextBox>
          <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please Enter Area" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtArea"></asp:RequiredFieldValidator>

<br />
<asp:Button ID="btnsave" runat="server" ValidationGroup="new" Text="Save"  class="wpcf7-submit" onclick="btnsave_Click" />
<asp:Button ID="btnupdate" runat="server" ValidationGroup="new" Text="Update" class="wpcf7-updated" onclick="btnupdate_Click" />
<asp:Button ID="btnclear" runat="server" Text="Clear" class="wpcf7-clear" onclick="btnclear_Click" />
<br />
<br />
<asp:GridView ID="gvArea" runat="server" AutoGenerateColumns="False" class="table table-hover" EmptyDataText="No records Found"
 AllowPaging="True" onpageindexchanging="gvArea_PageIndexChanging" PageSize="20"       onrowcommand="gvArea_RowCommand">
    <Columns>
        <asp:TemplateField HeaderText="CountryId" Visible="false">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CountryId") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblCountryId" runat="server" Text='<%# Bind("CountryId") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="StateId" Visible="false">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("StateId") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblStateId" runat="server" Text='<%# Bind("StateId") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
         <asp:TemplateField HeaderText="CityId" Visible="false">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CityId") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblCityId" runat="server" Text='<%# Bind("CityId") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="AreaId" Visible="false">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("AreaId") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblAreaId" runat="server" Text='<%# Bind("AreaId") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Area">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Area") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblArea" runat="server" Text='<%# Bind("Area") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Action">
            <ItemTemplate>
                <asp:LinkButton ID="lnkEdit" CommandName="EditRow" class="btn btn-info" CommandArgument='<%# Bind("AreaId") %>' runat="server" ><i class="glyphicon glyphicon-edit icon-white"></i>Edit</asp:LinkButton>
                <asp:LinkButton ID="lnkdelete" CommandName="DeleteRow" class="btn btn-danger" CommandArgument='<%# Bind("AreaId") %>' runat="server" OnClientClick="return conformbox();"><i class="glyphicon glyphicon-trash icon-white"></i>Delete</asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>

</ContentTemplate>
</asp:UpdatePanel>
</div>
        </div>
      </div>
    </div>
  </div>



       </div>
      </div>
    </section>
</asp:Content>

