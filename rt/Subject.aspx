﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RT/RTMasterPage.master" AutoEventWireup="true" CodeFile="Subject.aspx.cs" Inherits="RT_Subject" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <section id="contact">
      <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">Subject</h2>
              <span></span> 
            </div>
          </div>
       </div>
       <div class="row" >
<div class="container" id="parent">
  <div class="row">
    <div class="col-lg-12">
      <div class="row ">
        <div class="col-lg-12" id="child" >
 <div align="center" >
 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
 <ContentTemplate>
 <asp:Label ID="lblmsg" runat="server" ForeColor="Red" Visible="false"></asp:Label><br />
<asp:DropDownList ID="ddlBoard" class="wp-form-control wpcf7-text" runat="server" AutoPostBack="True" 
         onselectedindexchanged="ddlBoard_SelectedIndexChanged">
    </asp:DropDownList>
    <asp:RequiredFieldValidator ControlToValidate="ddlBoard" ID="RequiredFieldValidator5"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a Board"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
    <br />
<asp:DropDownList ID="ddlClass" runat="server" class="wp-form-control wpcf7-text" AutoPostBack="True" 
         onselectedindexchanged="ddlClass_SelectedIndexChanged">
    </asp:DropDownList>
    <asp:RequiredFieldValidator ControlToValidate="ddlClass" ID="RequiredFieldValidator1"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a Class"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
    <br />
<asp:TextBox ID="txtSubject" runat="server" class="wp-form-control wpcf7-text" placeholder="Enter Subject"></asp:TextBox>
         <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please Enter Subject" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtSubject"></asp:RequiredFieldValidator>

<br />
<asp:Button ID="btnsave" runat="server" class="wpcf7-submit" ValidationGroup="new"  Text="Save" onclick="btnsave_Click" />
<asp:Button ID="btnupdate" runat="server" class="wpcf7-updated" ValidationGroup="new"  Text="Update" onclick="btnupdate_Click" />
<asp:Button ID="btnclear" runat="server" class="wpcf7-clear" Text="Clear" onclick="btnclear_Click" />
<br />
<br />
<asp:GridView ID="gvSubject" runat="server" class="table table-hover" AutoGenerateColumns="False" EmptyDataText="No records Found" 
AllowPaging="True" onpageindexchanging="gvSubject_PageIndexChanging" PageSize="20"        onrowcommand="gvSubject_RowCommand">
    <Columns>
        <asp:TemplateField HeaderText="BoardId" Visible="false">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("BoardId") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblBoardId" runat="server" Text='<%# Bind("BoardId") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="ClassId" Visible="false">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ClassId") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblClassId" runat="server" Text='<%# Bind("ClassId") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
         <asp:TemplateField HeaderText="SubjectId" Visible="false">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("SubjectId") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblSubjectId" runat="server" Text='<%# Bind("SubjectId") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Subject">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Subject") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("Subject") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Action">
            <ItemTemplate>
                <asp:LinkButton ID="lnkEdit" class="btn btn-info" CommandName="EditRow" CommandArgument='<%# Bind("SubjectId") %>' runat="server"><i class="glyphicon glyphicon-edit icon-white"></i>Edit</asp:LinkButton>
                <asp:LinkButton ID="lnkdelete" class="btn btn-danger" CommandName="DeleteRow" CommandArgument='<%# Bind("SubjectId") %>' OnClientClick="return conformbox();" runat="server"><i class="glyphicon glyphicon-trash icon-white"></i>Delete</asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</ContentTemplate>
</asp:UpdatePanel>

</div>
        </div>
      </div>
    </div>
  </div>



       </div>
      </div>
    </section>
</asp:Content>

