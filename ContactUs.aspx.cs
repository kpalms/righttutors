﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Web.Script.Services;
public partial class ContactUs : System.Web.UI.Page
{
    Common common = new Common();
    BL_ContactUs bl_contactus = new BL_ContactUs();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            common.BindCountry(ddlCountry);
            common.SelectStateCountryId(ddlState, 0);
            common.SelectCityStateId(ddlCity, 0);
            common.SelectAreaCityId(ddlArea, 0);
        }
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        int cid = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        common.SelectStateCountryId(ddlState, cid);

        common.SelectCityStateId(ddlCity, 0);

        common.SelectAreaCityId(ddlArea, 0);
    }
    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
        int sid = Convert.ToInt32(ddlState.SelectedItem.Value);
        common.SelectCityStateId(ddlCity, sid);
        common.SelectAreaCityId(ddlArea, 0);
    }
    protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        int ciid = Convert.ToInt32(ddlCity.SelectedItem.Value);
        common.SelectAreaCityId(ddlArea, ciid);
    }


    private void Clear()
    {
        txtName.Text = "";
        txtAddress.Text = "";
        txtMobileNo.Text = "";
        txtEmail.Text = "";
        ddlArea.SelectedIndex = 0;
        ddlCity.SelectedIndex = 0;
        ddlCountry.SelectedIndex = 0;
        ddlState.SelectedIndex = 0;
        ddltopics.SelectedIndex = 0;
        txtmsg.Text = "";
    }

    private void SetFill()
    {
        bl_contactus.Topics = ddltopics.SelectedItem.Text;
        bl_contactus.CountryId = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        bl_contactus.StateId = Convert.ToInt32(ddlState.SelectedItem.Value);
        bl_contactus.CityId = Convert.ToInt32(ddlCity.SelectedItem.Value);
        bl_contactus.AreaId = Convert.ToInt32(ddlArea.SelectedItem.Value);
        bl_contactus.Address = txtAddress.Text;
        bl_contactus.Name = txtName.Text;
        bl_contactus.EmailId = txtEmail.Text;
        bl_contactus.DateTime = Convert.ToDateTime(common.GetDate());
        bl_contactus.MobileNo = txtMobileNo.Text;
        bl_contactus.Message = txtmsg.Text;
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        SetFill();
        bl_contactus.ContactUsId = 0;
        bl_contactus.Mode = "Insert";
        bl_contactus.AddContactUs(bl_contactus);

        EmailSms email = new EmailSms();
        string body = "<table><tr><td>Topics</td><td>" + ddltopics.SelectedItem.Text + "</td></tr><tr><td>Name</td><td>" + txtName.Text + "</td></tr><tr><td>Mobile No</td><td>" + txtMobileNo.Text + "</td></tr><tr><td>Email Id</td><td>" + txtEmail.Text + "</td></tr><tr><td>Country</td><td>" + ddlCountry.SelectedItem.Text + "</td></tr><tr><td>State</td><td>" + ddlState.SelectedItem.Text + "</td></tr><tr><td>City</td><td>" + ddlCity.SelectedItem.Text + "</td></tr><tr><td>Area</td><td>" + ddlArea.SelectedItem.Text + "</td></tr><tr><td>Address</td><td>" + txtAddress.Text + "</td></tr><tr><td>Message</td><td>" + txtmsg.Text + "</td></tr></table>";
        //email.SendMail("Enquire", body, txtEmail.Text);

        Clear();

        this.ModalPopupExtender1.Show();
    }
   
}