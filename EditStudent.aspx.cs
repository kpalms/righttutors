﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Web.Script.Services;
using System.IO;
public partial class EditStudent : System.Web.UI.Page
{
    Common common = new Common();
    BL_Students bl_student = new BL_Students();
    BL_Subject bl_subject = new BL_Subject();
    static string imagename;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["StudentsId"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                common.BindCountry(ddlCountry);
                BindStudent();
            }
        }
    }

    private void SetFill()
    {
        bl_student.Name = txtName.Text;
        bl_student.MiddleName = txtMiddleName.Text;
        bl_student.LastName = txtLastName.Text;
        bl_student.DOB = txtDob.Text;
        bl_student.CountryId = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        bl_student.StateId = Convert.ToInt32(ddlState.SelectedItem.Value);
        bl_student.CityId = Convert.ToInt32(ddlCity.SelectedItem.Value);
        bl_student.AreaId = Convert.ToInt32(ddlArea.SelectedItem.Value);
        bl_student.Address = txtAddress.Text;
        bl_student.PinCode = Convert.ToInt32(txtpincode.Text);
        bl_student.Gender = Convert.ToInt32(ddlGender.SelectedItem.Value);
        bl_student.MobileNo = txtMobileNo.Text;
        bl_student.Email = txtEmail.Text;
        bl_student.Password = "";
        bl_student.DateTime = Convert.ToDateTime(DateTime.Now.ToString());
    }

    private void BindStudent()
    {
        bl_student.StudentsId = Convert.ToInt32(Session["StudentsId"]);
        DataSet ds = bl_student.SelectStudentsById(bl_student);
        if (ds.Tables[0].Rows.Count > 0)
        {
            string img = ds.Tables[0].Rows[0]["ImageName"].ToString();
            imagename = img.Substring(18);

            txtName.Text = ds.Tables[0].Rows[0]["Name"].ToString();

           txtMiddleName.Text = ds.Tables[0].Rows[0]["MiddleName"].ToString();
            txtLastName.Text = ds.Tables[0].Rows[0]["LastName"].ToString();
            txtDob.Text = ds.Tables[0].Rows[0]["DOB"].ToString();
            ddlCountry.Text = ds.Tables[0].Rows[0]["CountryId"].ToString();
            common.SelectStateCountryId(ddlState, Convert.ToInt32(ds.Tables[0].Rows[0]["CountryId"]));
            ddlState.Text = ds.Tables[0].Rows[0]["StateId"].ToString();
            common.SelectCityStateId(ddlCity, Convert.ToInt32(ds.Tables[0].Rows[0]["StateId"]));
            ddlCity.Text = ds.Tables[0].Rows[0]["CityId"].ToString();
            common.SelectAreaCityId(ddlArea, Convert.ToInt32(ds.Tables[0].Rows[0]["CityId"]));
            ddlArea.Text = ds.Tables[0].Rows[0]["AreaId"].ToString();
            txtAddress.Text = ds.Tables[0].Rows[0]["Address"].ToString();
            txtpincode.Text = ds.Tables[0].Rows[0]["PinCode"].ToString();
            ddlGender.Text = ds.Tables[0].Rows[0]["Gender"].ToString();
            txtMobileNo.Text = ds.Tables[0].Rows[0]["MobileNo"].ToString();
            txtEmail.Text = ds.Tables[0].Rows[0]["Email"].ToString();
            imgprofile.ImageUrl = ds.Tables[0].Rows[0]["ImageName"].ToString();
        }
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        string filename="";
            SetFill();
            bl_student.StudentsId = Convert.ToInt32(Session["StudentsId"]);
            bl_student.Mode = "Update";

            if (fileimage.HasFile)
            {
                string ext = Path.GetExtension(fileimage.FileName);

                if ("NoImage.png" == imagename)
                {
                  filename = common.GetImageName() + "" + ext;
                }
                else
                {
                    filename = common.GetImageName() + "" + ext;
                    string imageFilePath = Server.MapPath("rt/Images/Student/" + imagename);
                    File.Delete(imageFilePath);
                }
                fileimage.PostedFile.SaveAs(Server.MapPath("rt/Images/Student/") + filename);
                bl_student.ImageName = filename;
            }
            else
            {
                bl_student.ImageName = imagename;
            }


            bl_student.UpdateStudents(bl_student);
            BindStudent();
            this.ModalPopupExtender1.Show();
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        int cid = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        common.SelectStateCountryId(ddlState, cid);
    }

    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
        int sid = Convert.ToInt32(ddlState.SelectedItem.Value);
        common.SelectCityStateId(ddlCity, sid);
    }

    protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        int ciid = Convert.ToInt32(ddlCity.SelectedItem.Value);
        common.SelectAreaCityId(ddlArea, ciid);
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        this.ModalPopupExtender1.Hide();
        Response.Redirect("StudentProfile.aspx"); 
    }
}