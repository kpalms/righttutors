﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EditStudent.aspx.cs" Inherits="EditStudent" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">
         .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=90);
        opacity: 0.8;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        border-width: 3px;
        border-style: solid;
        border-color: black;
        padding-top: 10px;
        padding-left: 10px;
        width: 300px;
        height: 140px;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <!--=========== BEGIN CONTACT SECTION ================-->
    <section id="contact">
      <div class="container">
       <div class="row" >
<div class="container" id="parent">
  <div class="row">
    <div class="col-lg-12">
      


<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
<ContentTemplate>
<asp:Image ID="imgprofile" class="img-responsive img-thumbnail" runat="server" Height="100px" Width="100px" /><br />
<br />

<span style="font-size:12px;color:Red"> * Required Fields </span>
<br />
<br />
<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<div align="center" >
<asp:TextBox ID="txtName" runat="server" class="wp-form-control wpcf7-text" placeholder="Name"></asp:TextBox>
</div>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtName"></asp:RequiredFieldValidator>
</div>
</div>

<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<div align="center" >
<asp:TextBox ID="txtMiddleName" runat="server" class="wp-form-control wpcf7-text" placeholder="Middle Name "></asp:TextBox>
</div>
</div>
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
</div>

<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<div align="center" >
<asp:TextBox ID="txtLastName" runat="server" class="wp-form-control wpcf7-text" placeholder="Last Name "></asp:TextBox>
</div>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtLastName"></asp:RequiredFieldValidator>
</div>
</div>

<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<div align="center" >
<asp:TextBox ID="txtDob" runat="server" class="wp-form-control wpcf7-text" placeholder="Date of Birth"></asp:TextBox>
 <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="txtDob"
        Mask="99/99/9999" MaskType="Date"   MessageValidatorTip="true" InputDirection="RightToLeft" >
        </ajaxToolkit:MaskedEditExtender>
</div>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
</div>
</div>

<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<div align="center" >
<asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True" 
onselectedindexchanged="ddlCountry_SelectedIndexChanged" class="wp-form-control wpcf7-text" placeholder="Country">
</asp:DropDownList>
</div>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ControlToValidate="ddlCountry" ID="RequiredFieldValidator5"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="*"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
</div>
</div>

<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<div align="center" >
<asp:DropDownList ID="ddlState" runat="server" 
AutoPostBack="True" onselectedindexchanged="ddlState_SelectedIndexChanged" class="wp-form-control wpcf7-text" placeholder="State">
</asp:DropDownList>
</div>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ControlToValidate="ddlState" ID="RequiredFieldValidator6"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="*"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
</div>
</div>


<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<div align="center" >
<asp:DropDownList ID="ddlCity" runat="server" 
AutoPostBack="True" onselectedindexchanged="ddlCity_SelectedIndexChanged" class="wp-form-control wpcf7-text" placeholder="City">
</asp:DropDownList>
</div>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ControlToValidate="ddlCity" ID="RequiredFieldValidator7"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="*"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
</div>
</div>


<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<div align="center" >
<asp:DropDownList ID="ddlArea" runat="server" class="wp-form-control wpcf7-text" placeholder="Area">
</asp:DropDownList>
</div>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ControlToValidate="ddlArea" ID="RequiredFieldValidator8"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="*"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
</div>
</div>

<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<div align="center" >
<asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" class="wp-form-control wpcf7-textarea" placeholder="Address"></asp:TextBox>
</div>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtAddress"></asp:RequiredFieldValidator>
</div>
</div>


<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<div align="center" >
<asp:TextBox ID="txtpincode" onkeypress='return event.charCode >= 48 && event.charCode <= 57' runat="server" MaxLength="6" class="wp-form-control wpcf7-text" placeholder="PinCode"></asp:TextBox>
<asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="please enter 6 Digit Pin Code"   ValidationExpression="[0-9]{6}" ControlToValidate="txtpincode" ValidationGroup="new" ForeColor="Red"></asp:RegularExpressionValidator>
</div>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="*" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtpincode"></asp:RequiredFieldValidator>
</div>
</div>


<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<div align="center" >
<asp:DropDownList ID="ddlGender" runat="server" class="wp-form-control wpcf7-text" placeholder="Gender">
<asp:ListItem Value="0">Selet Gender</asp:ListItem>
<asp:ListItem Value="1">Male</asp:ListItem>
<asp:ListItem Value="2">Female</asp:ListItem>
</asp:DropDownList>
</div>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ControlToValidate="ddlGender" ID="RequiredFieldValidator4"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="*"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
</div>
</div>

<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<div align="center" >
<asp:TextBox ID="txtMobileNo" runat="server" onkeypress='return event.charCode >= 48 && event.charCode <= 57' MaxLength="10" class="wp-form-control wpcf7-text" placeholder="Mobile No"></asp:TextBox>
<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="please enter 10 Digit mobile no"   ValidationExpression="[0-9]{10}" ControlToValidate="txtMobileNo" ValidationGroup="new" ForeColor="Red"></asp:RegularExpressionValidator>
</div>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="*" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtMobileNo"></asp:RequiredFieldValidator>
</div>
</div>


<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<div align="center" >
<asp:TextBox ID="txtEmail" runat="server" ReadOnly="true" class="wp-form-control wpcf7-text" placeholder="Email"></asp:TextBox>
</div>
</div>
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
</div>



<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<div align="center" >

<div style="font-size:10px;color:red" >select your profile pic (png,jpg,gif,bmp,jpeg)</div>
<br />
<asp:FileUpload ID="fileimage" runat="server"  />
<br />
<asp:RegularExpressionValidator ID="RegularExpressionValidator5" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.bmp|.jpeg)$"
ControlToValidate="fileimage" ValidationGroup="new" runat="server" ForeColor="Red" Font-Size="10px" ErrorMessage="Please select a valid png,jpg,gif,bmp,jpeg file."
Display="Dynamic" />
</div>
</div>
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
</div>

<br />
<br />
<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>

<div class="col-lg-4 col-md-4 col-sm-4">
<asp:Button ID="btnsubmit" runat="server" Text="Update Profile" ValidationGroup="new"
onclick="btnsubmit_Click" class="wpcf7-submit"/>
</div>

<div class="col-lg-4 col-md-4 col-sm-4">

<a style="text-decoration:none;margin-top:100px" href="StudentProfile.aspx" class="wpcf7-clear">Cancel</a>
</div>

<div class="col-lg-2 col-md-2 col-sm-2">
</div>
</div>


</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="btnsubmit" />
</Triggers>
</asp:UpdatePanel>

<asp:HiddenField ID="hfHidden" runat="server" />
    <asp:Panel ID="Panel1" CssClass="modalPopup" align="center" style = "display:none" runat="server">
    <table align="center">
    <tr>
   <b> Your Profile is Updated !</b>
    </tr>
    <br />
    <br />
    <br />
    <tr>
    <asp:Button ID="Button2" OnClick="Button2_Click" class="LoginButton"  runat="server" Text="Ok"/>
    </tr>
    </table>
    
    </asp:Panel>
   
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" BackgroundCssClass="modalBackground"
    CancelControlID="hfHidden"  PopupControlID="Panel1" TargetControlID="hfHidden" runat="server">
    </ajaxToolkit:ModalPopupExtender>

       
    </div>
  </div>



       </div>
      </div>
    </section>
    <!--=========== END CONTACT SECTION ================-->

 
  
   
  

</asp:Content>

