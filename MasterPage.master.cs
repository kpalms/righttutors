﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Student_StudenMasterPage : System.Web.UI.MasterPage
{
    Common common = new Common();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["StudentsId"] != null)
        {
            lnklogout.Visible = true;
            lnkhome.Visible = false;
            lnkstudentprofile.Visible = true;
            lnkteacherprofile.Visible = false;
            lnkaccount.Visible = true;
            lnkaddsubject.Visible = false;
            lnksubjectenquiryss.Visible = true;
            lnknotification.Visible = false;    
        }
        else if (Session["TeachersId"] != null)
        {
            lnkaddsubject.Visible = true;
            lnksubjectenquiryss.Visible = false;
            lnklogout.Visible = true;
            lnkhome.Visible = false;
            lnkstudentprofile.Visible = false;
            lnkteacherprofile.Visible = true;
            lnkaccount.Visible = true;
            lnkaddsubject.Visible = true;
            lnknotification.Visible = true;    
            lnksubjectenquiryss.Visible = false;
           
            lblnotification.Text = Convert.ToString(common.ReturnOneValue("select count(*) from Notification where Status=0 and TeachersId='" + Session["TeachersId"] + "'"));
           
            if(lblnotification.Text!="0")
            {
                lblnotification.Visible = true;
                lblnotification.ToolTip = "Notifications";
            }
            else
            {
                lblnotification.Visible = false;
            }
        }
        else
        {
            lnklogout.Visible = false;
            lnkhome.Visible = true;
            lnkaccount.Visible = false;
            lnknotification.Visible = false;    
            lnkstudentprofile.Visible = false;
            lnkteacherprofile.Visible = false;
        }

        lblstudent.Text = common.GetTotalStudent();
        lbltutors.Text = common.GetTotalTutor();

        if (Request.QueryString["pg"] == "sp" || Request.QueryString["pg"] == "tp")
        {
            pnlfooter.Visible = false;
        }
        else
        {
            pnlfooter.Visible = true;
        }


    }

    protected void lnklogout_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Session.Clear();
        Response.Redirect("Default.aspx");
    }

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["StudentsId"] != null)
        {
            Response.Redirect("StudentProfile.aspx");
        }
        else if (Session["TeachersId"] != null)
        {
            Response.Redirect("TeacherProfile.aspx");
        }
        else
        {
            Response.Redirect("Default.aspx");
        }
    }

    protected void lblnotification_Click(object sender, EventArgs e)
    {
        Response.Redirect("Notification.aspx");

    }

}
