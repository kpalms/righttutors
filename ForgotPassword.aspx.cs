﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class ForgotPassword : System.Web.UI.Page
{
    BL_Students bl_student = new BL_Students();
    BL_Teachers bl_teacher = new BL_Teachers();

    protected void Page_Load(object sender, EventArgs e)
    {
        Session["StudentsId"]= null ;
        Session["TeachersId"] = null;
        rdostudent.Checked = true;
    }

    protected void btnlogin_Click(object sender, EventArgs e)
    {
        if (rdostudent.Checked == false && rdoteacher.Checked == false)
        {
            lblmsg.Text = "Please Select Log In Type !";
        }
        else
        {
            if (rdostudent.Checked == true)
            {
                bl_student.Email = txtemailid.Text;
                DataTable dt = bl_student.CheckemailStudent(bl_student);

                if (dt.Rows.Count > 0)
                {
                    string password = dt.Rows[0]["Password"].ToString();

                    EmailSms email = new EmailSms();
                    string body = "<table><tr><td>Password :</td><td>'" + password + "'</td></tr></table>";
                    //email.SendMail("Your Passowrd", body, txtemailid.Text);

                   
                    lblmsg.ForeColor = System.Drawing.Color.Green;
                    lblmsg.Visible = true;
                    lblmsg.Text = "Email Send Sucessfully !";
                    lblmsg.Text = "";
                    this.ModalPopupExtender1.Show();
                }
                else
                {
                    lblmsg.ForeColor = System.Drawing.Color.Red;
                    lblmsg.Visible = true;
                    lblmsg.Text = "Email Not Found";
                }
            }
            else
            {
                bl_teacher.Email = txtemailid.Text;
                DataTable dt = bl_teacher.CheckemailTeachers(bl_teacher);

                if (dt.Rows.Count > 0)
                {
                    string password = dt.Rows[0]["Password"].ToString();

                    EmailSms email = new EmailSms();
                    string body = "<table><tr><td>Password :</td><td>'" + password + "'</td></tr></table>";
                    //email.SendMail("Your Passowrd", body, txtemailid.Text);
                   
                    lblmsg.Visible = true;
                    lblmsg.Text = "Email Send Sucessfully !";
                    lblmsg.Text = "";
                    this.ModalPopupExtender1.Show();
                }
                else
                {
                    lblmsg.Visible = true;
                    lblmsg.Text = "Email Not Found";
                }
            }
            txtemailid.Text = "";
        }
    }
}