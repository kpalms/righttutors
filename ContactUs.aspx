﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ContactUs.aspx.cs" Inherits="ContactUs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">
         .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=90);
        opacity: 0.8;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        border-width: 3px;
        border-style: solid;
        border-color: black;
        padding-top: 10px;
        padding-left: 10px;
        width: 300px;
        height: 140px;
    }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
 <ContentTemplate>
 
 


    <!--=========== BEGIN CONTACT SECTION ================-->
    <section id="contact">
      <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">Contact Us</h2>
              <span></span> 
              <p>JOIN US to begin finding TUTOR JOBS. <br/>Whether you want to home tutors, First Tutors can help you connect with students in your local area or online and begin private tuition</p>
            </div>
          </div>
       </div>
       <div class="row" >
<div class="col-lg-3 col-md-3 col-sm-3">
<b style="color:#0099ff;font-size:15px">Quick Links</b>
<br />
<a href="StudentRegistration.aspx" style="color:#00ccff;font-size:15px">Looking for tutors</a>
<br />
<a href="TeacherRegistration.aspx" style="color:#00ccff;font-size:15px">Looking for tution job</a>
</div>

<div class="col-lg-9 col-md-9 col-sm-9">
<span style="font-size:12px;color:Red"> * Required Fields </span>
<br />
<br />
<div class="row">

<div class="col-lg-11 col-md-11 col-sm-11">
<asp:DropDownList ID="ddltopics" runat="server" class="wp-form-control wpcf7-text" >
<asp:ListItem Value="0">Selet Topics</asp:ListItem>
<asp:ListItem Value="1">I Need a home tutor</asp:ListItem>
<asp:ListItem Value="2">I want to join as tutor</asp:ListItem>
    </asp:DropDownList>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ControlToValidate="ddltopics" ID="RequiredFieldValidator4"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="*"
InitialValue="0" runat="server" style="text-align:left"  Display="Dynamic"></asp:RequiredFieldValidator>
</div>
</div>


<div class="row">
<div class="col-lg-11 col-md-11 col-sm-11">
<asp:TextBox ID="txtName" runat="server" class="wp-form-control wpcf7-text" placeholder="Name"></asp:TextBox>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtName"></asp:RequiredFieldValidator>
</div>
</div>

<div class="row">
<div class="col-lg-11 col-md-11 col-sm-11">
<asp:TextBox ID="txtMobileNo" runat="server" MaxLength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="wp-form-control wpcf7-text" placeholder="Mobile No"></asp:TextBox>
<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="please enter 10 Digit mobile no"   ValidationExpression="[0-9]{10}" ControlToValidate="txtMobileNo" ValidationGroup="new" ForeColor="Red"></asp:RegularExpressionValidator>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="*" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtMobileNo"></asp:RequiredFieldValidator></div>
</div>

<div class="row">
<div class="col-lg-11 col-md-11 col-sm-11">
<asp:TextBox ID="txtEmail" runat="server" class="wp-form-control wpcf7-text" placeholder="Email"></asp:TextBox>
<asp:RegularExpressionValidator id="regEmail" ValidationGroup="new" ForeColor="Red" ControlToValidate="txtEmail" Text="Invalid email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Runat="server" />   
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="*" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
</div>
</div>

<div class="row">
<div class="col-lg-11 col-md-11 col-sm-11">
<asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True" 
        onselectedindexchanged="ddlCountry_SelectedIndexChanged" class="wp-form-control wpcf7-text" >
    </asp:DropDownList>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ControlToValidate="ddlCountry" ID="RequiredFieldValidator5"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="*"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
</div>
</div>

<div class="row">
<div class="col-lg-11 col-md-11 col-sm-11">
<asp:DropDownList ID="ddlState" runat="server" 
        AutoPostBack="True" onselectedindexchanged="ddlState_SelectedIndexChanged" class="wp-form-control wpcf7-text" >
    </asp:DropDownList>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ControlToValidate="ddlState" ID="RequiredFieldValidator6"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="*"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
</div>
</div>

<div class="row">
<div class="col-lg-11 col-md-11 col-sm-11">
 <asp:DropDownList ID="ddlCity" runat="server" 
        AutoPostBack="True" onselectedindexchanged="ddlCity_SelectedIndexChanged" class="wp-form-control wpcf7-text">
    </asp:DropDownList>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ControlToValidate="ddlCity" ID="RequiredFieldValidator7"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="*"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
</div>
</div>



<div class="row">
<div class="col-lg-11 col-md-11 col-sm-11">
<asp:DropDownList ID="ddlArea" runat="server" class="wp-form-control wpcf7-text" >
    </asp:DropDownList>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ControlToValidate="ddlArea" ID="RequiredFieldValidator8"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="*"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
</div>
</div>



<div class="row">
<div class="col-lg-11 col-md-11 col-sm-11">
<asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" class="wp-form-control wpcf7-text" placeholder="Address"></asp:TextBox>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtAddress"></asp:RequiredFieldValidator>
</div>
</div>



<div class="row">
<div class="col-lg-11 col-md-11 col-sm-11">
<asp:TextBox ID="txtmsg" runat="server" TextMode="MultiLine" class="wp-form-control wpcf7-textarea" placeholder="Message"></asp:TextBox>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtmsg"></asp:RequiredFieldValidator>
</div>
</div>

<div align="center">
<asp:Button ID="btnsubmit" runat="server" Text="Submit"  ValidationGroup="new"
        onclick="btnsubmit_Click" class="wpcf7-submit"/>
</div>

</div>

<asp:HiddenField ID="hfHidden" runat="server" />
    <asp:Panel ID="Panel1" CssClass="modalPopup" align="center" style = "display:none" runat="server">
    <table align="center">
    <tr>
   <b>Thank You !</b>
   <p>Your message has been successfully sent. We will contact you very soon!</p>
    </tr>
    <br />
    <tr>
    <asp:Button ID="Button2" class="LoginButton" runat="server" Text="Ok"/>
    </tr>
    </table>
    
    </asp:Panel>
   
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" BackgroundCssClass="modalBackground"
    CancelControlID="Button2" PopupControlID="Panel1" TargetControlID="hfHidden" runat="server">
    </ajaxToolkit:ModalPopupExtender>




 

      </div>
      </div>
    </section>
    <!--=========== END CONTACT SECTION ================-->

</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

