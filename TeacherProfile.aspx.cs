﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Data.SqlClient;
public partial class TeacherProfile : System.Web.UI.Page
{
    BL_Teachers bl_teacher = new BL_Teachers();
    BL_SelectedSubject bl_selectedsubject = new BL_SelectedSubject();
    BL_ToDayPost bl_todaypost = new BL_ToDayPost();
    BL_News bl_news = new BL_News();
    Common common = new Common();
    static int id;
    static string imagename;
 
    protected void Page_Load(object sender, EventArgs e)
    {
  
        if (Request.QueryString["TID"] != null)
        {
            bl_teacher.Mode = "Verified";
            bl_teacher.Active = 0;
            bl_teacher.TeachersId = Convert.ToInt32(Request.QueryString["TID"].ToString());
            bl_teacher.TeachersUpdates(bl_teacher);
            Session["TeachersId"] = Request.QueryString["TID"].ToString();
        }

        if (Session["TeachersId"] == null)
        {
            Response.Redirect("Default.aspx");
        }

        if (!IsPostBack)
        {
            BindTeacher();
            common.BindBoard(ddlBoard);
            common.BindClass(ddlClass);
            btnupdate.Enabled = false;
            btnupdate.CssClass = "wpcf7-updated";
            lblstudent.Text = common.GetTotalStudent();
            lbltutors.Text = common.GetTotalTutor();
        }

    }

    private void BindTeacher()
    {
        bl_teacher.TeachersId = Convert.ToInt32(Session["TeachersId"]);
        DataSet ds = bl_teacher.SelectTeachersById(bl_teacher);
        if (ds.Tables[0].Rows.Count > 0)
        {
            lblteacherid.Text = ds.Tables[0].Rows[0]["TID"].ToString();
 
            lblnamehead.Text = ds.Tables[0].Rows[0]["Name"].ToString();
            lbllastname.Text = ds.Tables[0].Rows[0]["LastName"].ToString();

            lbldob.Text = ds.Tables[0].Rows[0]["BirthYear"].ToString();
            lbltagline.Text = ds.Tables[0].Rows[0]["TagLine"].ToString();
            lblEducation.Text = ds.Tables[0].Rows[0]["Education"].ToString();
            lblExprience.Text = ds.Tables[0].Rows[0]["Experience"].ToString();
            lbladdress.Text = ds.Tables[0].Rows[0]["Address"].ToString() + "," + ds.Tables[0].Rows[0]["Area"].ToString() + "," + ds.Tables[0].Rows[0]["City"].ToString() + "," + ds.Tables[0].Rows[0]["State"].ToString() + "," + ds.Tables[0].Rows[0]["Country"].ToString() + "," + ds.Tables[0].Rows[0]["PinCode"].ToString();
            lblgender.Text = ds.Tables[0].Rows[0]["Genders"].ToString();
            lblmobileno.Text = ds.Tables[0].Rows[0]["MobileNo"].ToString();
            lblemail.Text = ds.Tables[0].Rows[0]["Email"].ToString();
            lblaboutme.Text = ds.Tables[0].Rows[0]["AboutMe"].ToString();
            imgprofile.ImageUrl = ds.Tables[0].Rows[0]["ImageName"].ToString();
        }

        if (ds.Tables[1].Rows.Count > 0)
        {
            rptteacherboardclass.DataSource = ds.Tables[1];
            rptteacherboardclass.DataBind();
        }
        else
        {
            rptteacherboardclass.DataSource = null;
            rptteacherboardclass.DataBind();
        }

        if (ds.Tables[2].Rows.Count > 0)
        {
            rpttodaypost.DataSource = ds.Tables[2];
            rpttodaypost.DataBind();
        }
        else
        {
            rpttodaypost.DataSource = null;
            rpttodaypost.DataBind();
        }

        if (ds.Tables[3].Rows.Count > 0)
        {
            rptnews.DataSource = ds.Tables[3];
            rptnews.DataBind();
        }
        else
        {
            rptnews.DataSource = null;
            rptnews.DataBind();
        }
    }

    protected void lnklogout_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Session.Clear();
        Response.Redirect("Default.aspx");
    }

    private void setfill()
    {
        bl_todaypost.TeachersId = Convert.ToInt32(Session["TeachersId"]);
        bl_todaypost.Title = txttitle.Text;
        bl_todaypost.Description = txtdescription.Text;
        bl_todaypost.BoardId = Convert.ToInt32(ddlBoard.SelectedItem.Value);
        bl_todaypost.ClassId = Convert.ToInt32(ddlClass.SelectedItem.Value);
        bl_todaypost.Active = 1;
        bl_todaypost.Date = common.GetDate();
    }

    private void clear()
    {
        txtdescription.Text = "";
        txttitle.Text = "";
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            setfill();
            bl_todaypost.TodayPostId = 0;
            bl_todaypost.Mode = "Insert";
            if (fileimage.HasFile)
            {
                string ext = Path.GetExtension(fileimage.FileName);
                string filename = common.GetImageName() + "" + ext;
                fileimage.PostedFile.SaveAs(Server.MapPath("rt/Images/TodayPost/") + filename);
                bl_todaypost.Image = filename;
            }
            else
            {
                bl_todaypost.Image = "NoImage.png";
            }
            bl_todaypost.AddTodayPost(bl_todaypost);
            lblmsgdata.Text = "Today Post Saved successfully !";
            this.ModalPopupExtender1.Show();
            clear();
            btnupdate.Enabled = false;
            btnsave.Enabled = true;
            BindTeacher();
        }
        catch
        {

        }
    }

    protected void ddlClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindTeacher();
    }
    protected void ddlBoard_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindTeacher();
    }

    protected void btnclear_Click(object sender, EventArgs e)
    {
        clear();
        ddlBoard.SelectedIndex = 0;
        ddlClass.SelectedIndex = 0;
        btnupdate.Enabled = false;
        btnsave.Enabled = true;
        imgedit.Visible = false;
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {
            string filename;
            setfill();
            bl_todaypost.TodayPostId = id;
            bl_todaypost.Mode = "Update";
            if (fileimage.HasFile)
            {
                string ext = Path.GetExtension(fileimage.FileName);

                if ("NoImage.png" == imagename)
                {
                    filename = common.GetImageName() + "" + ext;
                }
                else
                {
                    filename = common.GetImageName() + "" + ext;
                    string imageFilePath = Server.MapPath("rt/Images/TodayPost/" + imagename);
                    File.Delete(imageFilePath);
                }
                fileimage.PostedFile.SaveAs(Server.MapPath("rt/Images/TodayPost/") + filename);
                bl_todaypost.Image = filename;
            }
            else
            {
                bl_todaypost.Image = imagename;
            }
            bl_todaypost.AddTodayPost(bl_todaypost);
            lblmsgdata.Text = "Today Post Updated successfully !";
            this.ModalPopupExtender1.Show();
            clear();
            BindTeacher();
           
            btnupdate.Enabled = false;
            btnsave.Enabled = true;
            imgedit.Visible = false;
        }
        catch
        {

        }
    }


    protected void rpttodaypost_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        try
        {
            string sub;
            Label lblTitle = (Label)e.Item.FindControl("lblTitle");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblImage = (Label)e.Item.FindControl("lblImage");
            Label lblBoardId = (Label)e.Item.FindControl("lblBoardId");
            Label lblClassId = (Label)e.Item.FindControl("lblClassId");
            LinkButton lnkactive = (LinkButton)e.Item.FindControl("lnkactive");
         
            if (e.CommandName == "EditRow")
            {
                id = Convert.ToInt32(e.CommandArgument.ToString());
                txttitle.Text = lblTitle.Text;
                txtdescription.Text = lblDescription.Text;
                ddlClass.Text = lblClassId.Text;
                ddlBoard.Text = lblBoardId.Text;
                sub = lblImage.Text.Substring(17);
                imagename = sub;
                imgedit.Visible = true;
                imgedit.ImageUrl = "rt/" + lblImage.Text;
                btnupdate.Enabled = true;
                btnsave.Enabled = false;
                btnsave.CssClass = "wpcf7-submit";
            }

            if (e.CommandName == "DeleteRow")
            {
                bl_todaypost.Title = "";
                bl_todaypost.Description = "";
                bl_todaypost.Date = DateTime.Now;
                bl_todaypost.TodayPostId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_todaypost.Mode = "Delete";
                bl_todaypost.Image = "";
                bl_todaypost.Active = 0;
                bl_todaypost.BoardId = 0;
                bl_todaypost.ClassId = 0;
                bl_todaypost.Role = 0;
                bl_todaypost.AddTodayPost(bl_todaypost);
                imgedit.Visible = false;
                sub = lblImage.Text.Substring(17);
                imagename = sub;
                if ("NoImage.png" != imagename)
                {
                    string imageFilePath = Server.MapPath("rt/"+lblImage.Text);
                    File.Delete(imageFilePath);
                }
                clear();
                btnupdate.Enabled = false;
                btnsave.Enabled = true;
                BindTeacher();
            }

            if (e.CommandName == "Active")
            {
                bl_todaypost.Title = "";
                bl_todaypost.Description = "";
                bl_todaypost.Date = DateTime.Now;
                bl_todaypost.TodayPostId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_todaypost.Mode = "Active";
                bl_todaypost.Image = "";
                bl_todaypost.BoardId = 0;
                bl_todaypost.ClassId = 0;
                bl_todaypost.Role = 0;

                if (lnkactive.Text == "Deactive")
                {
                    bl_todaypost.Active = 0;
                    bl_todaypost.AddTodayPost(bl_todaypost);
                   
                    lblmsgdata.Text = "Subject Is Deactived !";
                    this.ModalPopupExtender1.Show();
                    BindTeacher();
                }
                else
                {
                    bl_todaypost.Active = 1;
                    bl_todaypost.AddTodayPost(bl_todaypost);
                    lblmsgdata.Text = "Subject Is Actived !";
                    this.ModalPopupExtender1.Show();
                    BindTeacher();
                }
                
            }
            
        }
        catch
        {
        }
    }

    protected void rptteacherboardclass_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        Repeater rpt = e.Item.FindControl("rptsubject") as Repeater;

        Label lblboard = e.Item.FindControl("lblboard") as Label;
        Label lblclass = e.Item.FindControl("lblclass") as Label;
        bl_teacher.BoardId = Convert.ToInt32(lblboard.Text);
        bl_teacher.ClassId = Convert.ToInt32(lblclass.Text);
        bl_teacher.TeachersId = Convert.ToInt32(Session["TeachersId"]);
        DataTable dt = bl_teacher.TechersubjectbyBoradIClassId(bl_teacher);
        rpt.DataSource = dt;
        rpt.DataBind();
    }
}