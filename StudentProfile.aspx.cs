﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class StudentProfile : System.Web.UI.Page
{
    BL_Students bl_student = new BL_Students();
    BL_ToDayPost bl_todaypost = new BL_ToDayPost();
    Common common = new Common();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.QueryString["SID"] !=null)
        {
            Session["StudentsId"]=Request.QueryString["SID"].ToString();
        }

        if (Session["StudentsId"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                BindStudent();
            }
        }

        lblstudent.Text = common.GetTotalStudent();
        lbltutors.Text = common.GetTotalTutor();
    }

    private void BindStudent()
    {
        bl_student.StudentsId = Convert.ToInt32(Session["StudentsId"]);
        DataSet ds = bl_student.SelectStudentsById(bl_student);
        if(ds.Tables[0].Rows.Count > 0)
        {
            lblstudentid.Text = ds.Tables[0].Rows[0]["SID"].ToString();
            lblnamehead.Text = ds.Tables[0].Rows[0]["Name"].ToString() ;
            lbllastname.Text = ds.Tables[0].Rows[0]["LastName"].ToString();
            

            lbldob.Text = ds.Tables[0].Rows[0]["DOB"].ToString();
            lbladdress.Text = ds.Tables[0].Rows[0]["Address"].ToString() + "," + ds.Tables[0].Rows[0]["Area"].ToString() + "," + ds.Tables[0].Rows[0]["City"].ToString() + "," + ds.Tables[0].Rows[0]["State"].ToString() + "," + ds.Tables[0].Rows[0]["Country"].ToString() + "," + ds.Tables[0].Rows[0]["PinCode"].ToString();
            lblgender.Text = ds.Tables[0].Rows[0]["Genders"].ToString();
            lblmobileno.Text = ds.Tables[0].Rows[0]["MobileNo"].ToString();
            lblemail.Text = ds.Tables[0].Rows[0]["Email"].ToString();
            imgprofile.ImageUrl = ds.Tables[0].Rows[0]["ImageName"].ToString();
        }

        if (ds.Tables[1].Rows.Count > 0)
        {
            lblboard.Text = ds.Tables[1].Rows[0]["Board"].ToString();
            lblclass.Text = ds.Tables[1].Rows[0]["Class"].ToString();

            bl_student.BoardId = Convert.ToInt32(ds.Tables[1].Rows[0]["BoardId"].ToString());
            bl_student.ClassId = Convert.ToInt32(ds.Tables[1].Rows[0]["ClassId"].ToString());
            bl_student.StudentsId = Convert.ToInt32(Session["StudentsId"]);
           DataTable dt= bl_student.StudentsubjectbyBoradIClassId(bl_student);
            lblsubject.Text = "";

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                lblsubject.Text = lblsubject.Text + dt.Rows[i]["Subject"].ToString() + ",";
            }
        }

        if (ds.Tables[2].Rows.Count > 0)
        {
            rptnews.DataSource = ds.Tables[2];
            rptnews.DataBind();
        }

        if (ds.Tables[3].Rows.Count > 0)
        {
            rpttodyapost.DataSource = ds.Tables[3];
            rpttodyapost.DataBind();
        }
    } 
}