﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_AboutUs
/// </summary>
public class BL_AboutUs
{
	public BL_AboutUs()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DL_AboutUs dl_aboutus = new DL_AboutUs();

    public int AboutUsId { get; set; }
    public string AboutUs { get; set; }
    public string Mode { get; set; }

    public void AddAboutUs(BL_AboutUs bl_aboutus)
    {
        dl_aboutus.AddAboutUs(bl_aboutus);
    }

    public DataTable SelectAboutUs()
    {
        DataTable dt = dl_aboutus.SelectAboutUs();
        return dt;
    }
}