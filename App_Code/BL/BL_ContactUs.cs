﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_ContactUs
/// </summary>
public class BL_ContactUs
{
	public BL_ContactUs()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DL_ContactUs dl_contactus = new DL_ContactUs();

    public string Name { get; set; }
    public string Mode { get; set; }
    public string Topics { get; set; }
    public string Message { get; set; }
    public string Address { get; set; }
    public string MobileNo { get; set; }
    public string EmailId { get; set; }
    public int CountryId { get; set; }
    public int StateId { get; set; }
    public int CityId { get; set; }
    public int AreaId { get; set; }
    public int ContactUsId { get; set; }
    public DateTime DateTime { get; set; }

    public void AddContactUs(BL_ContactUs bl_contactus)
    {
        dl_contactus.AddContactUs(bl_contactus);
    }

    public DataTable SelectAllContactUs()
    {
        DataTable dt = dl_contactus.SelectAllContactUs();
        return dt;
    }

}