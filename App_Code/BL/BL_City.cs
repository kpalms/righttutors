﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_City
/// </summary>
public class BL_City
{
	public BL_City()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_City dl_city = new DL_City();

    public string City { get; set; }
    public int CountryId { get; set; }
    public int StateId { get; set; }
    public int CityId { get; set; }
    public string Mode { get; set; }


    public void AddCity(BL_City bl_city)
    {
        dl_city.AddCity(bl_city);
    }

    public DataTable SelectCity(BL_City bl_city)
    {
        DataTable dt = dl_city.SelectCity(bl_city);
        return dt;
    }

    public DataTable SelectCityName(BL_City bl_city)
    {
        DataTable dt = dl_city.SelectCityName(bl_city);
        return dt;
    }

    public DataTable SelectCityStateId(BL_City bl_city)
    {
        DataTable dt = dl_city.SelectCityStateId(bl_city);
        return dt;
    }

    

}