﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_Testimonial
/// </summary>
public class BL_Testimonial
{
    DL_Testimonial dl_testomonial = new DL_Testimonial();
	public BL_Testimonial()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public int TestimonialId { get; set; }
    public int StudentId { get; set; }
    public int TeachersId { get; set; }
    public string Writes { get; set; }
    public string Role { get; set; }
    public DateTime DateTime { get; set; }

    public void AddTestimonial(BL_Testimonial bl_testomonial)
    {
        dl_testomonial.AddTestimonial(bl_testomonial);
    }

    public DataSet SelectRTTestimonial()
    {
        DataSet ds = dl_testomonial.SelectRTTestimonial();
        return ds;
    }
}