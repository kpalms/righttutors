﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_AdminLogin
/// </summary>
public class BL_AdminLogin
{
	public BL_AdminLogin()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_AdminLogin dl_adminlogin = new DL_AdminLogin();

    public string UserName { get; set; }
    public int LoginId { get; set; }
    public string Password { get; set; }

    public DataTable CheckAdminLogin(BL_AdminLogin bl_AdminLogin)
    {
        DataTable dt = dl_adminlogin.CheckAdminLogin(bl_AdminLogin);
        return dt;
    }

    public void ChangeAdminPassword(BL_AdminLogin bl_AdminLogin)
    {
        dl_adminlogin.ChangeAdminPassword(bl_AdminLogin);
    }


}