﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_SelectedSubject
/// </summary>
public class BL_SelectedSubject
{
    DL_SelectedSubject dl_selectedsubject = new DL_SelectedSubject();
	public BL_SelectedSubject()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public int SubjectId { get; set; }
    public int BoardId { get; set; }
    public int ClassId { get; set; }
    public string Mode { get; set; }
    public int TeacherSubjectId { get; set; }
    public int TeachersId { get; set; }
    public int Active { get; set; }

    public void AddTeacherSubject(BL_SelectedSubject bl_selectedsubject)
    {
        dl_selectedsubject.AddTeacherSubject(bl_selectedsubject);
    }

    public DataTable SelectTeacherSubject(BL_SelectedSubject bl_selectedsubject)
    {
        DataTable dt = dl_selectedsubject.SelectTeacherSubject(bl_selectedsubject);
        return dt;
    }

    public DataTable SelectTeacherSubjectById(BL_SelectedSubject bl_selectedsubject)
    {
        DataTable dt = dl_selectedsubject.SelectTeacherSubjectById(bl_selectedsubject);
        return dt;
    }

}