﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_Advertisement
/// </summary>
public class BL_Advertisement
{
	public BL_Advertisement()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DL_Advertisement dl_advertisement = new DL_Advertisement();

    public string Title { get; set; }
    public int AddId { get; set; }
    public string Description { get; set; }
    public string Url { get; set; }
    public string ImageName { get; set; }
    public string Mode { get; set; }


    public void AddAdvertisement(BL_Advertisement bl_advertisement)
    {
        dl_advertisement.AddAdvertisement(bl_advertisement);
    }

    public DataTable SelectAdvertisement()
    {
        DataTable dt = dl_advertisement.SelectAdvertisement();
        return dt;
    }
}