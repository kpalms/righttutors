﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_Subject
/// </summary>
public class BL_Subject
{
	public BL_Subject()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Subject dl_subject = new DL_Subject();

    public string Subject { get; set; }
    public int SubjectId { get; set; }
    public int BoardId { get; set; }
    public int ClassId { get; set; }
    public string Mode { get; set; }


    public void AddSubject(BL_Subject bl_subject)
    {
        dl_subject.AddSubject(bl_subject);
    }

    public DataTable SelectSubject(BL_Subject bl_subject)
    {
        DataTable dt = dl_subject.SelectSubject(bl_subject);
        return dt;
    }

    public DataTable SelectSubjectName(BL_Subject bl_subject)
    {
        DataTable dt = dl_subject.SelectSubjectName(bl_subject);
        return dt;
    }


    public DataTable SelectSubjectByBoardIdClassId(BL_Subject bl_subject)
    {
        DataTable dt = dl_subject.SelectSubjectByBoardIdClassId(bl_subject);
        return dt;
    }

}