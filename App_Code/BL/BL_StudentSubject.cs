﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_StudentSubject
/// </summary>
public class BL_StudentSubject
{
    DL_StudentSubject dl_studentsubject = new DL_StudentSubject();
	public BL_StudentSubject()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public int SubjectId { get; set; }
    public int BoardId { get; set; }
    public int ClassId { get; set; }
    public string Mode { get; set; }
    public int StudentSubjectId { get; set; }
    public int StudentsId { get; set; }
    public int Active { get; set; }

    public void AddStudentSubject(BL_StudentSubject bl_studentsubject)
    {
        dl_studentsubject.AddStudentSubject(bl_studentsubject);
    }

    public DataTable SelectStudentSubject(BL_StudentSubject bl_studentsubject)
    {
        DataTable dt = dl_studentsubject.SelectStudentSubject(bl_studentsubject);
        return dt;
    }

    public DataTable SelectStudentSubjectById(BL_StudentSubject bl_studentsubject)
    {
        DataTable dt = dl_studentsubject.SelectStudentSubjectById(bl_studentsubject);
        return dt;
    }
}