﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_Country
/// </summary>
public class BL_Country
{
	public BL_Country()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DL_Country dl_country = new DL_Country();

    public string Country { get; set; }
    public int CountryId { get; set; }
    public string Mode { get; set; }


    public void AddCountry(BL_Country bl_country)
    {
        dl_country.AddCountry(bl_country);
    }

    public DataTable SelectCountry()
    {
        DataTable dt = dl_country.SelectCountry();
        return dt;
    }

    public DataTable SelectCountryName(BL_Country bl_country)
    {
        DataTable dt = dl_country.SelectCountryName(bl_country);
        return dt;
    }

}