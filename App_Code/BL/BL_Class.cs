﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_Class
/// </summary>
public class BL_Class
{
	public BL_Class()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Class dl_class = new DL_Class();

    public string Class { get; set; }
    public int ClassId { get; set; }
    public string Mode { get; set; }


    public void AddClass(BL_Class bl_class)
    {
        dl_class.AddClass(bl_class);
    }

    public DataTable SelectClass()
    {
        DataTable dt = dl_class.SelectClass();
        return dt;
    }

    public DataTable SelectClassName(BL_Class bl_class)
    {
        DataTable dt = dl_class.SelectClassName(bl_class);
        return dt;
    }
}