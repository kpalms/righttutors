﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_News
/// </summary>
public class BL_News
{
	public BL_News()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DL_News dl_news= new DL_News();

    public string Title { get; set; }
    public int NewsId { get; set; }
    public int Active { get; set; }
    public string Description { get; set; }
    public string Image { get; set; }
    public string Mode { get; set; }
    public DateTime Date { get; set; }

    public void AddNews(BL_News bl_news)
    {
        dl_news.AddNews(bl_news);
    }

    public DataTable SelectNews(BL_News bl_news)
    {
        DataTable dt = dl_news.SelectNews(bl_news);
        return dt;
    }

    public DataSet SelectNewsST(int val)
    {
        DataSet ds = dl_news.SelectNewsST(val);
        return ds;
    }

}