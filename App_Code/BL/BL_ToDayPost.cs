﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_ToDayPost
/// </summary>
public class BL_ToDayPost
{
	public BL_ToDayPost()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DL_ToDayPost dl_todaypost = new DL_ToDayPost();

    public string Title { get; set; }
    public int TodayPostId { get; set; }
    public int BoardId { get; set; }
    public int ClassId { get; set; }
    public int Active { get; set; }
    public string Description { get; set; }
    public int Role { get; set; }
    public int TeachersId { get; set; }
    public string Image { get; set; }
    public string Mode { get; set; }
    public DateTime Date { get; set; }

    public void AddTodayPost(BL_ToDayPost bl_todaypost)
    {
        dl_todaypost.AddTodayPost(bl_todaypost);
    }

    public DataTable SelectAllTodayPost(BL_ToDayPost bl_todaypost)
    {
        DataTable dt = dl_todaypost.SelectAllTodayPost(bl_todaypost);
        return dt;
    }  
}