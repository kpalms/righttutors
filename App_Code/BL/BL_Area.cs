﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_Area
/// </summary>
public class BL_Area
{
	public BL_Area()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Area dl_area = new DL_Area();

    public string Area { get; set; }
    public int AreaId { get; set; }
    public int CountryId { get; set; }
    public int StateId { get; set; }
    public int CityId { get; set; }
    public string Mode { get; set; }


    public void AddArea(BL_Area bl_area)
    {
        dl_area.AddArea(bl_area);
    }

    public DataTable SelectArea(BL_Area bl_area)
    {
        DataTable dt = dl_area.SelectArea(bl_area);
        return dt;
    }

    public DataTable SelectAreaName(BL_Area bl_area)
    {
        DataTable dt = dl_area.SelectAreaName(bl_area);
        return dt;
    }

    public DataTable SelectAreaCityId(BL_Area bl_area)
    {
        DataTable dt = dl_area.SelectAreaCityId(bl_area);
        return dt;
    }
}