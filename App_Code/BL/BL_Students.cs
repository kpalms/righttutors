﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_Students
/// </summary>
public class BL_Students
{
	public BL_Students()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Students dl_students = new DL_Students();

    public string Name { get; set; }
    public string Mode { get; set; }
    public string MiddleName { get; set; }
    public string LastName { get; set; }
    public string DOB { get; set; }
    public string Address { get; set; }
    public string MobileNo { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    public int StudentsId { get; set; }
    public int CountryId { get; set; }
    public int StateId { get; set; }
    public int CityId { get; set; }
    public int AreaId { get; set; }
    public int PinCode { get; set; }
    public int Gender { get; set; }
    public int Active { get; set; }
    public int BoardId { get; set; }
    public int ClassId { get; set; }
    public string SubjectId { get; set; }
    public DateTime DateTime { get; set; }
    public string ImageName { get; set; }
    public int TeachersId { get; set; }

    public int AddStudents(BL_Students bl_student)
    {
      int sid= dl_students.AddStudents(bl_student);
      return sid;
    }

    public void UpdateStudents(BL_Students bl_student)
    {
        dl_students.UpdateStudents(bl_student);
    }

    public void DeleteStudent(BL_Students bl_student)
    {
        dl_students.DeleteStudent(bl_student);
    }

    public void StudentsUpdates(BL_Students bl_student)
    {
        dl_students.StudentsUpdates(bl_student);
    }

    public DataTable CheckemailStudent(BL_Students bl_students)
    {
        DataTable dt = dl_students.CheckemailStudent(bl_students);
        return dt;
    }

    public DataSet SelectStudentsById(BL_Students bl_students)
    {
        DataSet ds = dl_students.SelectStudentsById(bl_students);
        return ds;
    }

    public DataTable SelectStudent(BL_Students bl_students)
    {
        DataTable dt = dl_students.SelectStudent(bl_students);
        return dt;
    }

    public DataTable SelectStudentDeactive(BL_Students bl_students)
    {
        DataTable dt = dl_students.SelectStudentDeactive(bl_students);
        return dt;
    }

    public DataTable SelectStudentUnVerified(BL_Students bl_students)
    {
        DataTable dt = dl_students.SelectStudentUnVerified(bl_students);
        return dt;
    }

    public void ChangePassword(BL_Students bl_student)
    {
        dl_students.ChangePassword(bl_student);
    }

    public DataTable SearchStudent(BL_Students bl_students)
    {
        DataTable dt = dl_students.SearchStudent(bl_students);
        return dt;
    }


    public DataSet SelectAllCount(BL_Students bl_students)
    {
        DataSet ds = dl_students.SelectAllCount(bl_students);
        return ds;
    }

    public DataTable LogInStudent(BL_Students bl_students)
    {
        DataTable dt = dl_students.LogInStudent(bl_students);
        return dt;
    }

    public DataTable StudentsubjectbyBoradIClassId(BL_Students bl_students)
    {
        DataTable dt = dl_students.StudentsubjectbyBoradIClassId(bl_students);
        return dt;
    }
    
}