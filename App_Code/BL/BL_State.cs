﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_State
/// </summary>
public class BL_State
{
	public BL_State()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_State dl_state = new DL_State();

    public string State { get; set; }
    public int StateId { get; set; }
    public int CountryId { get; set; }
    public string Mode { get; set; }

    public void AddState(BL_State bl_state)
    {
        dl_state.AddState(bl_state);
    }


    public DataTable SelectStateCountryId(BL_State bl_state)
    {
        DataTable dt = dl_state.SelectStateCountryId(bl_state);
        return dt;
    }

    public DataTable SelectStateName(BL_State bl_state)
    {
        DataTable dt = dl_state.SelectStateName(bl_state);
        return dt;
    }
}