﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_Teachers
/// </summary>
public class BL_Teachers
{
	public BL_Teachers()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Teachers dl_teachers= new DL_Teachers();

    public string Name { get; set; }
    public string Mode { get; set; }
    public string MiddleName { get; set; }
    public string LastName { get; set; }
    public string BirthYear { get; set; }
    public string Education { get; set; }
    public string TagLine { get; set; }
    public string Address { get; set; }
    public string MobileNo { get; set; }
    public string Email { get; set; }
    public string AboutMe { get; set; }
    public string Password { get; set; }
    public int TeachersId { get; set; }
    public int CountryId { get; set; }
    public int StateId { get; set; }
    public int CityId { get; set; }
    public int AreaId { get; set; }
    public int PinCode { get; set; }
    public int Gender { get; set; }
    public int Active { get; set; }
    public int Experience { get; set; }
    public DateTime DateTime { get; set; }
    public string ImageName { get; set; }
    public int BoardId { get; set; }
    public int ClassId { get; set; }
    public string SubjectId { get; set; }

    public int AddTeachers(BL_Teachers bl_teachers)
    {
        int tid = dl_teachers.AddTeachers(bl_teachers);
        return tid;
    }

    public void UpdateTeachers(BL_Teachers bl_teachers)
    {
         dl_teachers.UpdateTeachers(bl_teachers);
    }

    public void TeachersUpdates(BL_Teachers bl_teachers)
    {
        dl_teachers.TeachersUpdates(bl_teachers);
    }



    public void DeleteTeacher(BL_Teachers bl_teachers)
    {
        dl_teachers.DeleteTeacher(bl_teachers);
    }

     public DataTable CheckemailTeachers(BL_Teachers bl_teachers)
    {
        DataTable dt = dl_teachers.CheckemailTeachers(bl_teachers);
        return dt;
    }

     public DataSet SelectTeachersById(BL_Teachers bl_teachers)
    {
        DataSet ds = dl_teachers.SelectTeachersById(bl_teachers);
        return ds;
    }

    public DataTable SelectTeachers(BL_Teachers bl_teachers)
    {
        DataTable dt = dl_teachers.SelectTeachers(bl_teachers);
        return dt;
    }

    public DataTable SelectTeachersUnverified(BL_Teachers bl_teachers)
    {
        DataTable dt = dl_teachers.SelectTeachersUnverified(bl_teachers);
        return dt;
    }

    public DataTable SelectTeachersDeactive(BL_Teachers bl_teachers)
    {
        DataTable dt = dl_teachers.SelectTeachersDeactive(bl_teachers);
        return dt;
    }

    public DataTable SearchTeachers(BL_Teachers bl_teachers)
    {
        DataTable dt = dl_teachers.SearchTeachers(bl_teachers);
        return dt;
    }

    public DataTable LogInTeacher(BL_Teachers bl_teachers)
    {
        DataTable dt = dl_teachers.LogInTeacher(bl_teachers);
        return dt;
    }

    public DataTable TechersubjectbyBoradIClassId(BL_Teachers bl_teachers)
    {
        DataTable dt = dl_teachers.TechersubjectbyBoradIClassId(bl_teachers);
        return dt;
    }
    
}