﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_Board
/// </summary>
public class BL_Board
{
	public BL_Board()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DL_Board dl_board = new DL_Board();

    public string Board { get; set; }
    public int BoardId { get; set; }
    public string Mode { get; set; }


    public void AddBoard(BL_Board bl_board)
    {
        dl_board.AddBoard(bl_board);
    }

    public DataTable SelectBoard()
    {
        DataTable dt = dl_board.SelectBoard();
        return dt;
    }

    public DataTable SelectBoardName(BL_Board bl_board)
    {
        DataTable dt = dl_board.SelectBoardName( bl_board);
        return dt;
    }
    
}