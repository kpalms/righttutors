﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_Notification
/// </summary>
public class BL_Notification
{
	public BL_Notification()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DL_Notification dl_notification = new DL_Notification();
    public int NotificationId { get; set; }
    public DateTime DateTime { get; set; }
    public int TeachersId { get; set; }
    public string Message { get; set; }
    public string Mode { get; set; }
    public int Status { get; set; }

    public void AddNotification(BL_Notification bl_notification)
    {
        dl_notification.AddNotification(bl_notification);
    }

    //public DataSet selectNotificationbyTeacherId(BL_Notification bl_notification, int val, int techerid)
    //{
    //    DataSet ds = dl_notification.selectNotificationbyTeacherId(bl_notification, val, techerid);
    //    return ds;
    //}

    public DataTable selectNotificationbyTeacherId(BL_Notification bl_notification)
    {
        DataTable ds = dl_notification.selectNotificationbyTeacherId(bl_notification);
        return ds;
    }

    public void UpdateNotification(BL_Notification bl_notification)
    {
        dl_notification.UpdateNotification(bl_notification);
    }
}