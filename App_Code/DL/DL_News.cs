﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_News
/// </summary>
public class DL_News
{
	public DL_News()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    DL_Connection dl_connection = new DL_Connection();

    public void AddNews(BL_News bl_news)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_news.Mode);
        SqlParameter NewsId = new SqlParameter("@NewsId", bl_news.NewsId);
        SqlParameter Title = new SqlParameter("@Title", bl_news.Title);
        SqlParameter Description = new SqlParameter("@Description", bl_news.Description);
        SqlParameter Image = new SqlParameter("@Image", bl_news.Image);
        SqlParameter Date = new SqlParameter("@Date", bl_news.Date);
        SqlParameter Active = new SqlParameter("@Active", bl_news.Active);
        DL_Connection.UseExecuteNonQuery("AddNews", Mode, NewsId, Title, Description, Image, Date, Active);
    }

    public DataTable SelectNews(BL_News bl_news)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectNews");
        return dt;
    }

    public DataSet SelectNewsST(int val)
    {
        SqlCommand cmd = new SqlCommand("SelectNewsST");
        cmd.Parameters.AddWithValue("@PageIndex", val);
        cmd.Parameters.AddWithValue("@PageSize", 10);
        cmd.Parameters.Add("@PageCount", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
        DataSet ds = dl_connection.UseDatasetRestoPro(cmd, val);
        return ds;
    }
}