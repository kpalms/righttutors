﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_Subject
/// </summary>
public class DL_Subject
{
	public DL_Subject()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Connection dl_connection = new DL_Connection();

    public void AddSubject(BL_Subject bl_subject)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_subject.Mode);
        SqlParameter BoardId = new SqlParameter("@BoardId", bl_subject.BoardId);
        SqlParameter ClassId = new SqlParameter("@ClassId", bl_subject.ClassId);
        SqlParameter SubjectId = new SqlParameter("@SubjectId", bl_subject.SubjectId);
        SqlParameter Subject = new SqlParameter("@Subject", bl_subject.Subject);
        DL_Connection.UseExecuteNonQuery("AddSubject", Mode, BoardId, ClassId, Subject, SubjectId);
    }

    public DataTable SelectSubject(BL_Subject bl_subject)
    {
        SqlParameter BoardId = new SqlParameter("@BoardId", bl_subject.BoardId);
        SqlParameter ClassId = new SqlParameter("@ClassId", bl_subject.ClassId);
        DataTable dt = dl_connection.UseDataTablePro("SelectSubject", BoardId, ClassId);
        return dt;
    }

    public DataTable SelectSubjectName(BL_Subject bl_subject)
    {
        SqlParameter Subject = new SqlParameter("@Subject", bl_subject.Subject);
        SqlParameter BoardId = new SqlParameter("@BoardId", bl_subject.BoardId);
        SqlParameter ClassId = new SqlParameter("@ClassId", bl_subject.ClassId);
        DataTable dt = dl_connection.UseDataTablePro("SelectSubjectName", BoardId, ClassId, Subject);
        return dt;
    }

    public DataTable SelectSubjectByBoardIdClassId(BL_Subject bl_subject)
    {
        SqlParameter BoardId = new SqlParameter("@BoardId", bl_subject.BoardId);
        SqlParameter ClassId = new SqlParameter("@ClassId", bl_subject.ClassId);
        DataTable dt = dl_connection.UseDataTablePro("SelectSubjectByBoardIdClassId", BoardId, ClassId);
        return dt;
    }
}