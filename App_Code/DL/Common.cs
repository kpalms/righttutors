﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Collections;
using System.Drawing.Printing;
using System.Web.UI.WebControls;
/// <summary>
/// Summary description for Common
/// </summary>
public class Common
{
	public Common()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DL_Connection dl_connection = new DL_Connection();
    BL_Country bl_country = new BL_Country();
    static int id;
    BL_State bl_state = new BL_State();
    BL_City bl_city = new BL_City();
    BL_Class bl_class = new BL_Class();
    BL_Board bl_board = new BL_Board();
    BL_Area bl_area = new BL_Area();
    BL_Subject bl_subject = new BL_Subject();
    BL_Students bl_student = new BL_Students();
    BL_Teachers bl_teachers = new BL_Teachers();

    public void BindCountry(DropDownList ddl)
    {
        DataTable dt = bl_country.SelectCountry();
        ddl.DataSource = dt;
        ddl.DataValueField = "CountryId";
        ddl.DataTextField = "Country";
        ddl.DataBind();
        // ddl.Items.Insert(0, "-- Select Owner --");
        ddl.Items.Insert(0, new ListItem("--Select Country--", "0"));
        ddl.SelectedIndex = 0;
    }

    public void SelectStateCountryId(DropDownList ddl, int countryid)
    {
        bl_state.CountryId = countryid;
        DataTable dt = bl_state.SelectStateCountryId(bl_state);
        ddl.DataSource = dt;
        ddl.DataValueField = "StateId";
        ddl.DataTextField = "State";
        ddl.DataBind();
        // ddl.Items.Insert(0, "-- Select Offer --");
        ddl.Items.Insert(0, new ListItem("--Select State--", "0"));
        ddl.SelectedIndex = 0;
    }


    public void SelectCityStateId(DropDownList ddl, int stateid)
    {
        bl_city.StateId = stateid;
        DataTable dt = bl_city.SelectCityStateId(bl_city);
        ddl.DataSource = dt;
        ddl.DataValueField = "CityId";
        ddl.DataTextField = "City";
        ddl.DataBind();
        // ddl.Items.Insert(0, "-- Select Offer --");
        ddl.Items.Insert(0, new ListItem("--Select City--", "0"));
        ddl.SelectedIndex = 0;
    }


    public void SelectAreaCityId(DropDownList ddl, int cityid)
    {
        bl_area.CityId = cityid;
        DataTable dt = bl_area.SelectAreaCityId(bl_area);
        ddl.DataSource = dt;
        ddl.DataValueField = "AreaId";
        ddl.DataTextField = "Area";
        ddl.DataBind();
        // ddl.Items.Insert(0, "-- Select Offer --");
        ddl.Items.Insert(0, new ListItem("--Select Area--", "0"));
        ddl.SelectedIndex = 0;
    }


    public void BindBoard(DropDownList ddl)
    {
        DataTable dt = bl_board.SelectBoard();
        ddl.DataSource = dt;
        ddl.DataValueField = "BoardId";
        ddl.DataTextField = "Board";
        ddl.DataBind();
        // ddl.Items.Insert(0, "-- Select Owner --");
        ddl.Items.Insert(0, new ListItem("--Select Board--", "0"));
        ddl.SelectedIndex = 0;
    }


    public void BindClass(DropDownList ddl)
    {
        DataTable dt = bl_class.SelectClass();
        ddl.DataSource = dt;
        ddl.DataValueField = "ClassId";
        ddl.DataTextField = "Class";
        ddl.DataBind();
        // ddl.Items.Insert(0, "-- Select Owner --");
        ddl.Items.Insert(0, new ListItem("--Select Class--", "0"));
        ddl.SelectedIndex = 0;
    }


    public void BindSubjects(DropDownList ddl,int boardid,int classid)
    {
        bl_subject.BoardId = boardid;
        bl_subject.ClassId = classid;
        DataTable dt = bl_subject.SelectSubjectByBoardIdClassId(bl_subject);
        ddl.DataSource = dt;
        ddl.DataValueField = "SubjectId";
        ddl.DataTextField = "Subject";
        ddl.DataBind();
        // ddl.Items.Insert(0, "-- Select Owner --");
        ddl.Items.Insert(0, new ListItem("--Select Subject--", "0"));
        ddl.SelectedIndex = 0;
    }


    public void BindStudent(DropDownList ddl)
    {
        DataTable dt = bl_student.SelectStudent(bl_student);
        ddl.DataSource = dt;
        ddl.DataValueField = "StudentsId";
        ddl.DataTextField = "Name";
        ddl.DataBind();
        // ddl.Items.Insert(0, "-- Select Owner --");
        ddl.Items.Insert(0, new ListItem("--Select Students--", "0"));
        ddl.SelectedIndex = 0;
    }


    public void BindTeacher(DropDownList ddl)
    {
        DataTable dt = bl_teachers.SelectTeachers(bl_teachers);
        ddl.DataSource = dt;
        ddl.DataValueField = "TeachersId";
        ddl.DataTextField = "Name";
        ddl.DataBind();
        // ddl.Items.Insert(0, "-- Select Owner --");
        ddl.Items.Insert(0, new ListItem("--Select Teachers--", "0"));
        ddl.SelectedIndex = 0;
    }

    public DateTime GetDate()
    {
        TimeZoneInfo timzoe = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
        DateTime indiaTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timzoe);
        return indiaTime;

    }

    public string GetRandomString()
    {
        int length = 4;
        const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder res = new StringBuilder();
        Random rnd = new Random();
        while (0 < length--)
        {
            res.Append(valid[rnd.Next(valid.Length)]);
        }
        return res.ToString();
    }


    public string GetImageName()
    {
        int length = 4;
        const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder res = new StringBuilder();
        Random rnd = new Random();
        while (0 < length--)
        {
            res.Append(valid[rnd.Next(valid.Length)]);
        }
        return (res + DateTime.Now.ToString("ddMMyyyyHHmmss")).ToString();
    }

    public string ReturnOneValue(string query)
    {
        string OneValue = "";
        string id = dl_connection.UseExcuteScaler(query);
        if (id == "")
        {
            OneValue = "0";

        }
        else
        {
            OneValue = id;
        }
        return OneValue;

    }

    public string GetTotalStudent()
    {
        string totstudnet;
        totstudnet = Convert.ToString(130000 + Convert.ToInt32((ReturnOneValue("Select count(*) from Students where Verified='True' and Active='True'"))));
        return totstudnet;
    }

    public string GetTotalTutor()
    {
        string tottutor;
        tottutor = Convert.ToString(47096 + Convert.ToInt32((ReturnOneValue("Select count(*) from Teachers where Verified='True' and Active='True'"))));
        return tottutor;
    }

}