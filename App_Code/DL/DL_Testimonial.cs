﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_Testimonial
/// </summary>
public class DL_Testimonial
{
	public DL_Testimonial()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    DL_Connection dl_connection = new DL_Connection();

    public void AddTestimonial(BL_Testimonial bl_testomonial)
    {
        SqlParameter StudentId = new SqlParameter("@StudentId", bl_testomonial.StudentId);
        SqlParameter TeachersId = new SqlParameter("@TeachersId", bl_testomonial.TeachersId);
        SqlParameter Writes = new SqlParameter("@Writes", bl_testomonial.Writes);
        SqlParameter Role = new SqlParameter("@Role", bl_testomonial.Role);
        SqlParameter DateTime = new SqlParameter("@DateTime", bl_testomonial.DateTime);
        DL_Connection.UseExecuteNonQuery("AddTestimonial", StudentId, TeachersId, Writes, Role, DateTime);
    }

    public DataSet SelectRTTestimonial()
    {
        DataSet ds = dl_connection.UseDatasetPro("SelectRTTestimonial");
        return ds;
    }

}