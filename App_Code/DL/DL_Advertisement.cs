﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_Advertisement
/// </summary>
public class DL_Advertisement
{
	public DL_Advertisement()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Connection dl_connection = new DL_Connection();

    public void AddAdvertisement(BL_Advertisement bl_advertisement)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_advertisement.Mode);
        SqlParameter AddId = new SqlParameter("@AddId", bl_advertisement.AddId);
        SqlParameter Title = new SqlParameter("@Title", bl_advertisement.Title);
        SqlParameter Description = new SqlParameter("@Description", bl_advertisement.Description);
        SqlParameter Url = new SqlParameter("@Url", bl_advertisement.Url);
        SqlParameter ImageName = new SqlParameter("@ImageName", bl_advertisement.ImageName);
        DL_Connection.UseExecuteNonQuery("AddAdvertisement", Mode, AddId, Title, Description, Url, ImageName);
    }

    public DataTable SelectAdvertisement()
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectAdvertisement");
        return dt;
    }
}