﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_Students
/// </summary>
public class DL_Students
{
	public DL_Students()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Connection dl_connection = new DL_Connection();

    public int AddStudents(BL_Students bl_students)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_students.Mode);
        SqlParameter StudentsId = new SqlParameter("@StudentsId", bl_students.StudentsId);
        SqlParameter Name = new SqlParameter("@Name", bl_students.Name);
        SqlParameter MiddleName = new SqlParameter("@MiddleName", bl_students.MiddleName);
        SqlParameter LastName = new SqlParameter("@LastName", bl_students.LastName);
        SqlParameter DOB = new SqlParameter("@DOB", bl_students.DOB);
        SqlParameter CountryId = new SqlParameter("@CountryId", bl_students.CountryId);
        SqlParameter StateId = new SqlParameter("@StateId", bl_students.StateId);
        SqlParameter CityId = new SqlParameter("@CityId", bl_students.CityId);
        SqlParameter AreaId = new SqlParameter("@AreaId", bl_students.AreaId);
        SqlParameter Address = new SqlParameter("@Address", bl_students.Address);
        SqlParameter PinCode = new SqlParameter("@PinCode", bl_students.PinCode);
        SqlParameter Gender = new SqlParameter("@Gender", bl_students.Gender);
        SqlParameter MobileNo = new SqlParameter("@MobileNo", bl_students.MobileNo);
        SqlParameter Email = new SqlParameter("@Email", bl_students.Email);
        SqlParameter BoardId = new SqlParameter("@BoardId", bl_students.BoardId);
        SqlParameter ClassId = new SqlParameter("@ClassId", bl_students.ClassId);
        SqlParameter SubjectId = new SqlParameter("@SubjectId", bl_students.SubjectId);
        SqlParameter Password = new SqlParameter("@Password", bl_students.Password);
        SqlParameter DateTime = new SqlParameter("@DateTime", bl_students.DateTime);
        SqlParameter ImageName = new SqlParameter("@ImageName", bl_students.ImageName);
        SqlParameter Verified = new SqlParameter("@Verified", 1);
        SqlParameter i = new SqlParameter("@i", "0");
        StudentsId.Direction = ParameterDirection.Output;
        DL_Connection.UseExecuteNonQuery("AddStudents", Mode, StudentsId, Name, MiddleName, LastName, DOB, CountryId, StateId, CityId, AreaId, Address, PinCode, Gender, MobileNo, Email, BoardId, ClassId, SubjectId, Password, DateTime, ImageName,Verified, i);
        int sid = Convert.ToInt32(StudentsId.Value);
        return sid;
    }

    public void UpdateStudents(BL_Students bl_students)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_students.Mode);
        SqlParameter StudentsId = new SqlParameter("@StudentsId", bl_students.StudentsId);
        SqlParameter Name = new SqlParameter("@Name", bl_students.Name);
        SqlParameter MiddleName = new SqlParameter("@MiddleName", bl_students.MiddleName);
        SqlParameter LastName = new SqlParameter("@LastName", bl_students.LastName);
        SqlParameter DOB = new SqlParameter("@DOB", bl_students.DOB);
        SqlParameter CountryId = new SqlParameter("@CountryId", bl_students.CountryId);
        SqlParameter StateId = new SqlParameter("@StateId", bl_students.StateId);
        SqlParameter CityId = new SqlParameter("@CityId", bl_students.CityId);
        SqlParameter AreaId = new SqlParameter("@AreaId", bl_students.AreaId);
        SqlParameter Address = new SqlParameter("@Address", bl_students.Address);
        SqlParameter PinCode = new SqlParameter("@PinCode", bl_students.PinCode);
        SqlParameter Gender = new SqlParameter("@Gender", bl_students.Gender);
        SqlParameter MobileNo = new SqlParameter("@MobileNo", bl_students.MobileNo);
        SqlParameter Email = new SqlParameter("@Email", bl_students.Email);
        SqlParameter Password = new SqlParameter("@Password", bl_students.Password);
        SqlParameter DateTime = new SqlParameter("@DateTime", bl_students.DateTime);
        SqlParameter ImageName = new SqlParameter("@ImageName", bl_students.ImageName);
        DL_Connection.UseExecuteNonQuery("UpdateStudents", Mode, StudentsId, Name, MiddleName, LastName, DOB, CountryId, StateId, CityId, AreaId, Address, PinCode, Gender, MobileNo, Email,Password, DateTime, ImageName);
       
    }


    public void DeleteStudent(BL_Students bl_students)
    {
        SqlParameter StudentsId = new SqlParameter("@StudentsId", bl_students.StudentsId);
        DL_Connection.UseExecuteNonQuery("DeleteStudent", StudentsId);
    }


    public void StudentsUpdates(BL_Students bl_students)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_students.Mode);
        SqlParameter StudentsId = new SqlParameter("@StudentsId", bl_students.StudentsId);
        SqlParameter Active = new SqlParameter("@Active", bl_students.Active);
        DL_Connection.UseExecuteNonQuery("StudentsUpdates", Mode, StudentsId, Active);
    }

    public DataTable CheckemailStudent(BL_Students bl_students)
    {
        SqlParameter Email = new SqlParameter("@Email", bl_students.Email);
       DataTable dt= dl_connection.UseDataTablePro("CheckemailStudent", Email);
       return dt;
    }

    public DataSet SelectStudentsById(BL_Students bl_students)
    {
        SqlParameter StudentsId = new SqlParameter("@StudentsId", bl_students.StudentsId);
        DataSet ds = dl_connection.UseDatasetPro("SelectStudentsById", StudentsId);
        return ds;
    }

    public DataTable SelectStudent(BL_Students bl_students)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectStudent");
        return dt;
    }

    public DataTable SelectStudentDeactive(BL_Students bl_students)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectStudentDeactive");
        return dt;
    }

    public DataTable SelectStudentUnVerified(BL_Students bl_students)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectStudentUnVerified");
        return dt;
    }

    public void ChangePassword(BL_Students bl_students)
    {
        SqlParameter StudentsId = new SqlParameter("@StudentsId", bl_students.StudentsId);
        SqlParameter Password = new SqlParameter("@Password", bl_students.Password);
        SqlParameter Mode = new SqlParameter("@Mode", bl_students.Mode);
        SqlParameter TeachersId = new SqlParameter("@TeachersId", bl_students.TeachersId);
        DL_Connection.UseExecuteNonQuery("ChangePassword", StudentsId, Password,Mode,TeachersId);
    }

    public DataTable SearchStudent(BL_Students bl_students)
    {
        SqlParameter StudentsId = new SqlParameter("@StudentsId", bl_students.StudentsId);
        DataTable dt = dl_connection.UseDataTablePro("SearchStudent", StudentsId);
        return dt;
    }

    public DataSet SelectAllCount(BL_Students bl_students)
    {
        DataSet ds = dl_connection.UseDatasetPro("SelectAllCount");
        return ds;
    }

    public DataTable LogInStudent(BL_Students bl_students)
    {
        SqlParameter Email = new SqlParameter("@Email", bl_students.Email);
        SqlParameter Password = new SqlParameter("@Password", bl_students.Password);
        DataTable dt = dl_connection.UseDataTablePro("LogInStudent", Email, Password);
        return dt;
    }


    public DataTable StudentsubjectbyBoradIClassId(BL_Students bl_students)
    {
        SqlParameter BoardId = new SqlParameter("@BoardId", bl_students.BoardId);
        SqlParameter ClassId = new SqlParameter("@ClassId", bl_students.ClassId);
        SqlParameter StudentsId = new SqlParameter("@StudentsId", bl_students.StudentsId);
        DataTable dt = dl_connection.UseDataTablePro("StudentsubjectbyBoradIClassId", BoardId, ClassId, StudentsId);
        return dt;
    }

}