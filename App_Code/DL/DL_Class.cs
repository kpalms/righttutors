﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_Class
/// </summary>
public class DL_Class
{
	public DL_Class()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Connection dl_connection = new DL_Connection();

    public void AddClass(BL_Class bl_class)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_class.Mode);
        SqlParameter ClassId = new SqlParameter("@ClassId", bl_class.ClassId);
        SqlParameter Class = new SqlParameter("@Class", bl_class.Class);
        DL_Connection.UseExecuteNonQuery("AddClass", Mode, ClassId, Class);
    }

    public DataTable SelectClass()
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectClass");
        return dt;
    }

    public DataTable SelectClassName(BL_Class bl_class)
    {
        SqlParameter Class = new SqlParameter("@Class", bl_class.Class);
        DataTable dt = dl_connection.UseDataTablePro("SelectClassName", Class);
        return dt;
    }
}