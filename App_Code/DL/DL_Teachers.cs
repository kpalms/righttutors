﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_Teachers
/// </summary>
public class DL_Teachers
{
	public DL_Teachers()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    DL_Connection dl_connection = new DL_Connection();

    public int AddTeachers(BL_Teachers bl_teachers)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_teachers.Mode);
        SqlParameter TeachersId = new SqlParameter("@TeachersId", bl_teachers.TeachersId);
        SqlParameter Name = new SqlParameter("@Name", bl_teachers.Name);
        SqlParameter MiddleName = new SqlParameter("@MiddleName", bl_teachers.MiddleName);
        SqlParameter LastName = new SqlParameter("@LastName", bl_teachers.LastName);
        SqlParameter BirthYear = new SqlParameter("@BirthYear", bl_teachers.BirthYear);
        SqlParameter CountryId = new SqlParameter("@CountryId", bl_teachers.CountryId);
        SqlParameter StateId = new SqlParameter("@StateId", bl_teachers.StateId);
        SqlParameter TagLine = new SqlParameter("@TagLine", bl_teachers.TagLine);
        SqlParameter Education = new SqlParameter("@Education", bl_teachers.Education);
        SqlParameter Experience = new SqlParameter("@Experience", bl_teachers.Experience);
        SqlParameter CityId = new SqlParameter("@CityId", bl_teachers.CityId);
        SqlParameter AreaId = new SqlParameter("@AreaId", bl_teachers.AreaId);
        SqlParameter Address = new SqlParameter("@Address", bl_teachers.Address);
        SqlParameter PinCode = new SqlParameter("@PinCode", bl_teachers.PinCode);
        SqlParameter Gender = new SqlParameter("@Gender", bl_teachers.Gender);
        SqlParameter MobileNo = new SqlParameter("@MobileNo", bl_teachers.MobileNo);
        SqlParameter Email = new SqlParameter("@Email", bl_teachers.Email);
        SqlParameter AboutMe = new SqlParameter("@AboutMe", bl_teachers.AboutMe);
        SqlParameter Password = new SqlParameter("@Password", bl_teachers.Password);
        SqlParameter DateTime = new SqlParameter("@DateTime", bl_teachers.DateTime);
        SqlParameter ImageName = new SqlParameter("@ImageName", bl_teachers.ImageName);
        SqlParameter BoardId = new SqlParameter("@BoardId", bl_teachers.BoardId);
        SqlParameter ClassId = new SqlParameter("@ClassId", bl_teachers.ClassId);
        SqlParameter SubjectId = new SqlParameter("@SubjectId", bl_teachers.SubjectId);
        SqlParameter Verified = new SqlParameter("@Verified",1);

        SqlParameter i = new SqlParameter("@i", "0");
        TeachersId.Direction = ParameterDirection.Output;
        DL_Connection.UseExecuteNonQuery("AddTeachers", Mode, i, TeachersId, Name, MiddleName, LastName, BirthYear, TagLine, Education, Experience, CountryId, StateId, CityId, AreaId, BoardId, ClassId, SubjectId, Address, PinCode, Gender, MobileNo, Email, AboutMe, Password, DateTime, ImageName, Verified);
       int tid = Convert.ToInt32(TeachersId.Value);
        return tid;
    }

    public void UpdateTeachers(BL_Teachers bl_teachers)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_teachers.Mode);
        SqlParameter TeachersId = new SqlParameter("@TeachersId", bl_teachers.TeachersId);
        SqlParameter Name = new SqlParameter("@Name", bl_teachers.Name);
        SqlParameter MiddleName = new SqlParameter("@MiddleName", bl_teachers.MiddleName);
        SqlParameter LastName = new SqlParameter("@LastName", bl_teachers.LastName);
        SqlParameter BirthYear = new SqlParameter("@BirthYear", bl_teachers.BirthYear);
        SqlParameter CountryId = new SqlParameter("@CountryId", bl_teachers.CountryId);
        SqlParameter StateId = new SqlParameter("@StateId", bl_teachers.StateId);
        SqlParameter TagLine = new SqlParameter("@TagLine", bl_teachers.TagLine);
        SqlParameter Education = new SqlParameter("@Education", bl_teachers.Education);
        SqlParameter Experience = new SqlParameter("@Experience", bl_teachers.Experience);
        SqlParameter CityId = new SqlParameter("@CityId", bl_teachers.CityId);
        SqlParameter AreaId = new SqlParameter("@AreaId", bl_teachers.AreaId);
        SqlParameter Address = new SqlParameter("@Address", bl_teachers.Address);
        SqlParameter PinCode = new SqlParameter("@PinCode", bl_teachers.PinCode);
        SqlParameter Gender = new SqlParameter("@Gender", bl_teachers.Gender);
        SqlParameter MobileNo = new SqlParameter("@MobileNo", bl_teachers.MobileNo);
        SqlParameter Email = new SqlParameter("@Email", bl_teachers.Email);
        SqlParameter AboutMe = new SqlParameter("@AboutMe", bl_teachers.AboutMe);
        SqlParameter Password = new SqlParameter("@Password", bl_teachers.Password);
        SqlParameter DateTime = new SqlParameter("@DateTime", bl_teachers.DateTime);
        SqlParameter ImageName = new SqlParameter("@ImageName", bl_teachers.ImageName);
        DL_Connection.UseExecuteNonQuery("UpdateTeachers", Mode, TeachersId, Name, MiddleName, LastName, BirthYear, TagLine, Education, Experience, CountryId, StateId, CityId, AreaId, Address, PinCode, Gender, MobileNo, Email, AboutMe, Password, DateTime, ImageName);
    }

    public void TeachersUpdates(BL_Teachers bl_teachers)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_teachers.Mode);
        SqlParameter TeachersId = new SqlParameter("@TeachersId", bl_teachers.TeachersId);
        SqlParameter Active = new SqlParameter("@Active", bl_teachers.Active);
        DL_Connection.UseExecuteNonQuery("TeachersUpdates", Mode, TeachersId, Active);
    }

    public void DeleteTeacher(BL_Teachers bl_teachers)
    {
        SqlParameter TeachersId = new SqlParameter("@TeachersId", bl_teachers.TeachersId);
        DL_Connection.UseExecuteNonQuery("DeleteTearcher", TeachersId);
    }
    
    public DataTable CheckemailTeachers(BL_Teachers bl_teachers)
    {
        SqlParameter Email = new SqlParameter("@Email", bl_teachers.Email);
        DataTable dt = dl_connection.UseDataTablePro("CheckemailTeachers", Email);
        return dt;
    }

    public DataSet SelectTeachersById(BL_Teachers bl_teachers)
    {
        SqlParameter TeachersId = new SqlParameter("@TeachersId", bl_teachers.TeachersId);
        DataSet ds = dl_connection.UseDatasetPro("SelectTeachersById", TeachersId);
        return ds;
    }

    public DataTable SelectTeachers(BL_Teachers bl_teachers)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectTeachers");
        return dt;
    }

    public DataTable SelectTeachersUnverified(BL_Teachers bl_teachers)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectTeachersUnverified");
        return dt;
    }

    public DataTable SelectTeachersDeactive(BL_Teachers bl_teachers)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectTeachersDeactive");
        return dt;
    }

    public DataTable SearchTeachers(BL_Teachers bl_teachers)
    {
        SqlParameter TeachersId = new SqlParameter("@TeachersId", bl_teachers.TeachersId);
        DataTable dt = dl_connection.UseDataTablePro("SearchTeachers", TeachersId);
        return dt;
    }

    public DataTable LogInTeacher(BL_Teachers bl_teachers)
    {
        SqlParameter Email = new SqlParameter("@Email", bl_teachers.Email);
        SqlParameter Password = new SqlParameter("@Password", bl_teachers.Password);
        DataTable dt = dl_connection.UseDataTablePro("LogInTeacher", Email, Password);
        return dt;
    }

    public DataTable TechersubjectbyBoradIClassId(BL_Teachers bl_teachers)
    {
        SqlParameter BoardId = new SqlParameter("@BoardId", bl_teachers.BoardId);
        SqlParameter ClassId = new SqlParameter("@ClassId", bl_teachers.ClassId);
        SqlParameter TeachersId = new SqlParameter("@TeachersId", bl_teachers.TeachersId);
        DataTable dt = dl_connection.UseDataTablePro("TechersubjectbyBoradIClassId", BoardId, ClassId, TeachersId);
        return dt;
    }
}