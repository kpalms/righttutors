﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_Notification
/// </summary>
public class DL_Notification
{
	public DL_Notification()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Connection dl_connection = new DL_Connection();

    public void AddNotification(BL_Notification bl_notification)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_notification.Mode);
        SqlParameter NotificationId = new SqlParameter("@NotificationId", bl_notification.NotificationId);
        SqlParameter TeachersId = new SqlParameter("@TeachersId", bl_notification.TeachersId);
        SqlParameter DateTime = new SqlParameter("@DateTime", bl_notification.DateTime);
        SqlParameter Message = new SqlParameter("@Message", bl_notification.Message);
        DL_Connection.UseExecuteNonQuery("AddNotification", Mode, NotificationId, TeachersId, DateTime, Message);
    }

    //public DataSet selectNotificationbyTeacherId(BL_Notification bl_notification, int val, int techerid)
    //{
    //    SqlCommand cmd = new SqlCommand("selectNotificationbyTeacherId");
    //    cmd.Parameters.AddWithValue("@PageIndex", val);
    //    cmd.Parameters.AddWithValue("@PageSize", 10);
    //    cmd.Parameters.AddWithValue("@TeachersId", techerid);
    //    cmd.Parameters.Add("@PageCount", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
    //    DataSet ds = dl_connection.UseDatasetRestoPro(cmd, val);
    //    return ds;
    //}

    public DataTable selectNotificationbyTeacherId(BL_Notification bl_notification)
    {
        SqlParameter TeachersId = new SqlParameter("@TeachersId", bl_notification.TeachersId);
        DataTable dt = dl_connection.UseDataTablePro("selectNotificationbyTeacherId",TeachersId);
        return dt;
    }


    public void UpdateNotification(BL_Notification bl_notification)
    {
        SqlParameter TeachersId = new SqlParameter("@TeachersId", bl_notification.TeachersId);
        DL_Connection.UseExecuteNonQuery("UpdateNotification",TeachersId);
    }

}