﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_City
/// </summary>
public class DL_City
{
	public DL_City()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    DL_Connection dl_connection = new DL_Connection();

    public void AddCity(BL_City bl_city)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_city.Mode);
        SqlParameter CountryId = new SqlParameter("@CountryId", bl_city.CountryId);
        SqlParameter StateId = new SqlParameter("@StateId", bl_city.StateId);
        SqlParameter CityId = new SqlParameter("@CityId", bl_city.CityId);
        SqlParameter City = new SqlParameter("@City", bl_city.City);
        DL_Connection.UseExecuteNonQuery("AddCity", Mode, CountryId, StateId, City, CityId);
    }

    public DataTable SelectCity(BL_City bl_city)
    {
        SqlParameter CountryId = new SqlParameter("@CountryId", bl_city.CountryId);
        SqlParameter StateId = new SqlParameter("@StateId", bl_city.StateId);
        DataTable dt = dl_connection.UseDataTablePro("SelectCity", CountryId, StateId);
        return dt;
    }

    public DataTable SelectCityName(BL_City bl_city)
    {
        SqlParameter CountryId = new SqlParameter("@CountryId", bl_city.CountryId);
        SqlParameter StateId = new SqlParameter("@StateId", bl_city.StateId);
        SqlParameter City = new SqlParameter("@City", bl_city.City);
        DataTable dt = dl_connection.UseDataTablePro("SelectCityName", CountryId, StateId, City);
        return dt;
    }

    public DataTable SelectCityStateId(BL_City bl_city)
    {
        SqlParameter StateId = new SqlParameter("@StateId", bl_city.StateId);
        DataTable dt = dl_connection.UseDataTablePro("SelectCityStateId", StateId);
        return dt;
    }
}