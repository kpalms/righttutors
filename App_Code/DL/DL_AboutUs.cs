﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_AboutUs
/// </summary>
public class DL_AboutUs
{
	public DL_AboutUs()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Connection dl_connection = new DL_Connection();

    public void AddAboutUs(BL_AboutUs bl_aboutus)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_aboutus.Mode);
        SqlParameter AboutUsId = new SqlParameter("@AboutUsId", bl_aboutus.AboutUsId);
        SqlParameter AboutUs = new SqlParameter("@AboutUs", bl_aboutus.AboutUs);
        DL_Connection.UseExecuteNonQuery("AddAboutUs", Mode, AboutUsId, AboutUs);
    }

    public DataTable SelectAboutUs()
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectAboutUs");
        return dt;
    }
}