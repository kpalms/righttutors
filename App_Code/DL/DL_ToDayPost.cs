﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// <summary>
/// Summary description for DL_ToDayPost
/// </summary>
public class DL_ToDayPost
{
	public DL_ToDayPost()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    DL_Connection dl_connection = new DL_Connection();

    public void AddTodayPost(BL_ToDayPost bl_todaypost)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_todaypost.Mode);
        SqlParameter TodayPostId = new SqlParameter("@TodayPostId", bl_todaypost.TodayPostId);
        SqlParameter TeachersId = new SqlParameter("@TeachersId", bl_todaypost.TeachersId);
        SqlParameter Title = new SqlParameter("@Title", bl_todaypost.Title);
        SqlParameter Description = new SqlParameter("@Description", bl_todaypost.Description);
        SqlParameter Image = new SqlParameter("@Image", bl_todaypost.Image);
        SqlParameter Date = new SqlParameter("@Date", bl_todaypost.Date);
        SqlParameter BoardId = new SqlParameter("@BoardId", bl_todaypost.BoardId);
        SqlParameter ClassId = new SqlParameter("@ClassId", bl_todaypost.ClassId);
        SqlParameter Active = new SqlParameter("@Active", bl_todaypost.Active);
        DL_Connection.UseExecuteNonQuery("AddTodayPost", Mode, TodayPostId, Title, Description, Image, Date, BoardId, ClassId,TeachersId, Active);
    }

    public DataTable SelectAllTodayPost(BL_ToDayPost bl_todaypost)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectAllTodayPost");
        return dt;
    }
}