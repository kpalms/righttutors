﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_Area
/// </summary>
public class DL_Area
{
	public DL_Area()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Connection dl_connection = new DL_Connection();

    public void AddArea(BL_Area bl_area)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_area.Mode);
        SqlParameter CountryId = new SqlParameter("@CountryId", bl_area.CountryId);
        SqlParameter StateId = new SqlParameter("@StateId", bl_area.StateId);
        SqlParameter CityId = new SqlParameter("@CityId", bl_area.CityId);
        SqlParameter AreaId = new SqlParameter("@AreaId", bl_area.AreaId);
        SqlParameter Area = new SqlParameter("@Area", bl_area.Area);
        DL_Connection.UseExecuteNonQuery("AddArea", Mode, CountryId, StateId, AreaId, Area, CityId);
    }

    public DataTable SelectArea(BL_Area bl_area)
    {
        SqlParameter CountryId = new SqlParameter("@CountryId", bl_area.CountryId);
        SqlParameter StateId = new SqlParameter("@StateId", bl_area.StateId);
        SqlParameter CityId = new SqlParameter("@CityId", bl_area.CityId);
        DataTable dt = dl_connection.UseDataTablePro("SelectArea", CountryId, StateId, CityId);
        return dt;
    }

    public DataTable SelectAreaCityId(BL_Area bl_area)
    {
        SqlParameter CityId = new SqlParameter("@CityId", bl_area.CityId);
        DataTable dt = dl_connection.UseDataTablePro("SelectAreaCityId", CityId);
        return dt;
    }


    public DataTable SelectAreaName(BL_Area bl_area)
    {
        SqlParameter CountryId = new SqlParameter("@CountryId", bl_area.CountryId);
        SqlParameter StateId = new SqlParameter("@StateId", bl_area.StateId);
        SqlParameter CityId = new SqlParameter("@CityId", bl_area.CityId);
        SqlParameter Area = new SqlParameter("@Area", bl_area.Area);
        DataTable dt = dl_connection.UseDataTablePro("SelectAreaName", CountryId, StateId, Area, CityId);
        return dt;
    }
    
}