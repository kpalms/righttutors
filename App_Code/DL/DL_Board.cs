﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_Board
/// </summary>
public class DL_Board
{
	public DL_Board()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    DL_Connection dl_connection = new DL_Connection();

    public void AddBoard(BL_Board bl_board)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_board.Mode);
        SqlParameter BoardId = new SqlParameter("@BoardId", bl_board.BoardId);
        SqlParameter Board = new SqlParameter("@Board", bl_board.Board);
        DL_Connection.UseExecuteNonQuery("AddBoard", Mode, BoardId, Board);
    }

    public DataTable SelectBoard()
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectBoard");
        return dt;
    }

    public DataTable SelectBoardName(BL_Board bl_board)
    {
        SqlParameter Board = new SqlParameter("@Board", bl_board.Board);
        DataTable dt = dl_connection.UseDataTablePro("SelectBoardName", Board);
        return dt;
    }
}