﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_Country
/// </summary>
public class DL_Country
{
	public DL_Country()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Connection dl_connection = new DL_Connection();

    public void AddCountry(BL_Country bl_country)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_country.Mode);
        SqlParameter CountryId = new SqlParameter("@CountryId", bl_country.CountryId);
        SqlParameter Country = new SqlParameter("@Country", bl_country.Country);
        DL_Connection.UseExecuteNonQuery("AddCountry", Mode, CountryId, Country);
    }

    public DataTable SelectCountry()
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectCountry");
        return dt;
    }

    public DataTable SelectCountryName(BL_Country bl_country)
    {
        SqlParameter Country = new SqlParameter("@Country", bl_country.Country);
        DataTable dt = dl_connection.UseDataTablePro("SelectCountryName", Country);
        return dt;
    }


}