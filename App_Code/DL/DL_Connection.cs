﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

/// <summary>
/// Summary description for DL_Connection
/// </summary>
public class DL_Connection
{
	public DL_Connection()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public static string ErroMsg;


    public static string constr = ConfigurationManager.ConnectionStrings["RightTutors"].ConnectionString;
    SqlConnection con = new SqlConnection(constr);

    private void checkCon()
    {
        try
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
                SqlConnection.ClearPool(con);

                //con.Dispose();
            }
            con.Open();
        }
        catch
        {
            //MessageBox.Show(ex.ToString());
        }
    }

    public void UseExcuteNonQuery(string query)
    {
        try
        {
            checkCon();
            con.Open();
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch
        {


        }

    }

    public static int UseExecuteNonQuery(string SPName, params SqlParameter[] Parameters)
    {
        int intErr = 0;
        SqlConnection cn = new SqlConnection(constr);
        SqlCommand cmd = new SqlCommand(SPName, cn);

        cmd.CommandText = SPName;
        cmd.CommandType = CommandType.StoredProcedure;

        if (Parameters != null)
            foreach (SqlParameter item in Parameters)
                cmd.Parameters.Add(item);

        cn.Open();
        try
        {
            cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            intErr = 1;
            if (cn != null) cn.Close();
            throw ex;
        }
        finally
        {
            cn.Close();
        }
        cmd = null;
        cn = null;

        return intErr;
    }


    public string UseExcuteScallerPro(string Sname, params SqlParameter[] parameters)
    {
        object id;
        SqlConnection con = new SqlConnection(constr);
        SqlCommand cmd = new SqlCommand(Sname, con);
        cmd.CommandType = CommandType.StoredProcedure;
        if (parameters != null)
        {
            foreach (SqlParameter item in parameters)
                cmd.Parameters.Add(item);
        }
        con.Open();
        try
        {
            id = cmd.ExecuteScalar();
            //dr = Convert.ToInt32(str);

        }
        catch (Exception ex)
        {
            throw ex;
        }

        //con = null;
        con.Close();
        return Convert.ToString(id);
    }

    public SqlDataReader GetFromReader(string SPName, params SqlParameter[] Parameters)
    {
        SqlDataReader dr = null;
        SqlConnection cn = new SqlConnection(constr);
        SqlCommand cmd = new SqlCommand(SPName, cn);

        cmd.CommandType = CommandType.StoredProcedure;

        if (Parameters != null)
            foreach (SqlParameter item in Parameters)
                cmd.Parameters.Add(item);

        cn.Open();

        try
        {
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
        }
        catch
        {
            if (cn != null) cn.Close();
            throw;
        }

        cmd = null;
        // cn = null;
        con.Close();
        return dr;
    }


    public DataSet UseDatasetPro(string Sname, params SqlParameter[] parameters)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection con = new SqlConnection(constr);
            SqlCommand cmd = new SqlCommand(Sname, con);
            cmd.CommandType = CommandType.StoredProcedure;
            if (parameters != null)
            {
                foreach (SqlParameter item in parameters)
                    cmd.Parameters.Add(item);
            }
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataTable UseDataTablePro(string SPName, params SqlParameter[] parameters)
    {
        SqlConnection cn = new SqlConnection(constr);
        SqlCommand cmd = new SqlCommand(SPName, cn);
        if (parameters != null)
        {
            foreach (SqlParameter item in parameters)
                cmd.Parameters.Add(item);
        }
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        cmd.CommandType = CommandType.StoredProcedure;
        da.Fill(dt);
        return dt;
    }

    public String queryMaxId(String queMStr)
    {
        checkCon();
        SqlCommand max_id = new SqlCommand(queMStr, con);
        SqlDataReader MxDR = max_id.ExecuteReader();
        string strid = "";// "0";
        while (MxDR.Read())
        {
            strid = MxDR["ids"].ToString();
        }
        return strid;
    }

    private void CheckCon()
    {
        try
        {
            if (con.State.ToString() == con.State.ToString())
            {
                con.Close();
            }
        }
        catch
        {


        }
    }

    public string UseExcuteScaler(string str)
    {
        String closing = "";
        try
        {
            CheckCon();
            con.Open();
            SqlCommand cmd = new SqlCommand(str, con);
            Object tbl = cmd.ExecuteScalar();
            if (tbl != null)
            {
                closing = tbl.ToString();
            }
        }
        catch (Exception ex)
        {
            // MessageBox.Show("Error Occured?Please Contact Administrator" + ex);
        }
        return closing;
    }


    public DataSet UseDatasetAutoLoadPro(SqlCommand cmd, int val)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection con = new SqlConnection(constr);

            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter sda = new SqlDataAdapter();
            cmd.Connection = con;
            sda.SelectCommand = cmd;
            sda.Fill(ds, "dsname");
            //sda.Fill(ds, val, 5, "authors");

            return ds;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }




    public DataSet UseDatasetRestoPro(SqlCommand cmd, int val)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection con = new SqlConnection(constr);

            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter sda = new SqlDataAdapter();
            cmd.Connection = con;
            sda.SelectCommand = cmd;
            //sda.Fill(ds, val, 5, "authors");
            sda.Fill(ds, "dsname");
            return ds;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}