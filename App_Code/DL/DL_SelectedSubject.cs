﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_SelectedSubject
/// </summary>
public class DL_SelectedSubject
{
	public DL_SelectedSubject()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Connection dl_connection = new DL_Connection();

    public void AddTeacherSubject(BL_SelectedSubject bl_selectedsubject)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_selectedsubject.Mode);
        SqlParameter BoardId = new SqlParameter("@BoardId", bl_selectedsubject.BoardId);
        SqlParameter ClassId = new SqlParameter("@ClassId", bl_selectedsubject.ClassId);
        SqlParameter SubjectId = new SqlParameter("@SubjectId", bl_selectedsubject.SubjectId);
        SqlParameter TeachersId = new SqlParameter("@TeachersId", bl_selectedsubject.TeachersId);
        SqlParameter TeacherSubjectId = new SqlParameter("@TeacherSubjectId", bl_selectedsubject.TeacherSubjectId);
        SqlParameter Active = new SqlParameter("@Active", bl_selectedsubject.Active);
        DL_Connection.UseExecuteNonQuery("AddTeacherSubject", Mode, BoardId, ClassId, TeachersId, SubjectId, TeacherSubjectId, Active);
    }

    public DataTable SelectTeacherSubject(BL_SelectedSubject bl_selectedsubject)
    {
        SqlParameter TeachersId = new SqlParameter("@TeachersId", bl_selectedsubject.TeachersId);
        DataTable dt = dl_connection.UseDataTablePro("SelectTeacherSubject", TeachersId);
        return dt;
    }

    public DataTable SelectTeacherSubjectById(BL_SelectedSubject bl_selectedsubject)
    {
        SqlParameter TeachersId = new SqlParameter("@TeachersId", bl_selectedsubject.TeachersId);
        SqlParameter SubjectId = new SqlParameter("@SubjectId", bl_selectedsubject.SubjectId);
        DataTable dt = dl_connection.UseDataTablePro("SelectTeacherSubjectById", TeachersId, SubjectId);
        return dt;
    }
}