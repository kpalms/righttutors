﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_ContactUs
/// </summary>
public class DL_ContactUs
{
	public DL_ContactUs()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Connection dl_connection = new DL_Connection();

    public void AddContactUs(BL_ContactUs bl_contactus)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_contactus.Mode);
        SqlParameter ContactUsId = new SqlParameter("@ContactUsId", bl_contactus.ContactUsId);
        SqlParameter Name = new SqlParameter("@Name", bl_contactus.Name);
        SqlParameter Topics = new SqlParameter("@Topics", bl_contactus.Topics);
        SqlParameter Message = new SqlParameter("@Message", bl_contactus.Message);
        SqlParameter CountryId = new SqlParameter("@CountryId", bl_contactus.CountryId);
        SqlParameter StateId = new SqlParameter("@StateId", bl_contactus.StateId);
        SqlParameter CityId = new SqlParameter("@CityId", bl_contactus.CityId);
        SqlParameter AreaId = new SqlParameter("@AreaId", bl_contactus.AreaId);
        SqlParameter Address = new SqlParameter("@Address", bl_contactus.Address);
        SqlParameter MobileNo = new SqlParameter("@MobileNo", bl_contactus.MobileNo);
        SqlParameter EmailId = new SqlParameter("@EmailId", bl_contactus.EmailId);
        SqlParameter DateTime = new SqlParameter("@DateTime", bl_contactus.DateTime);
        DL_Connection.UseExecuteNonQuery("AddContactUs", Mode, ContactUsId, Name, Topics, Message, CountryId, StateId, CityId, AreaId, Address, MobileNo, EmailId, DateTime);
    }

    public DataTable SelectAllContactUs()
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectAllContactUs");
        return dt;
    }
}