﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_StudentSubject
/// </summary>
public class DL_StudentSubject
{
	public DL_StudentSubject()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Connection dl_connection = new DL_Connection();

    public void AddStudentSubject(BL_StudentSubject bl_studentsubject)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_studentsubject.Mode);
        SqlParameter BoardId = new SqlParameter("@BoardId", bl_studentsubject.BoardId);
        SqlParameter ClassId = new SqlParameter("@ClassId", bl_studentsubject.ClassId);
        SqlParameter SubjectId = new SqlParameter("@SubjectId", bl_studentsubject.SubjectId);
        SqlParameter StudentsId = new SqlParameter("@StudentsId", bl_studentsubject.StudentsId);
        SqlParameter StudentSubjectId = new SqlParameter("@StudentSubjectId", bl_studentsubject.StudentSubjectId);
        SqlParameter Active = new SqlParameter("@Active", bl_studentsubject.Active);
        DL_Connection.UseExecuteNonQuery("AddStudentSubject", Mode, BoardId, ClassId, StudentsId, SubjectId, StudentSubjectId, Active);
    }

    public DataTable SelectStudentSubject(BL_StudentSubject bl_studentsubject)
    {
        SqlParameter StudentsId = new SqlParameter("@StudentsId", bl_studentsubject.StudentsId);
        DataTable dt = dl_connection.UseDataTablePro("SelectStudentSubject", StudentsId);
        return dt;
    }


    public DataTable SelectStudentSubjectById(BL_StudentSubject bl_studentsubject)
    {
        SqlParameter StudentsId = new SqlParameter("@StudentsId", bl_studentsubject.StudentsId);
        SqlParameter SubjectId = new SqlParameter("@SubjectId", bl_studentsubject.SubjectId);
        DataTable dt = dl_connection.UseDataTablePro("SelectStudentSubjectById", StudentsId, SubjectId);
        return dt;
    }
}