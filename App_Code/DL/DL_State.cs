﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_State
/// </summary>
public class DL_State
{
	public DL_State()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DL_Connection dl_connection = new DL_Connection();

    public void AddState(BL_State bl_state)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_state.Mode);
        SqlParameter CountryId = new SqlParameter("@CountryId", bl_state.CountryId);
        SqlParameter StateId = new SqlParameter("@StateId", bl_state.StateId);
        SqlParameter State = new SqlParameter("@State", bl_state.State);
         DL_Connection.UseExecuteNonQuery("AddState", Mode, CountryId, StateId, State);
    }

    public DataTable SelectStateCountryId(BL_State bl_state)
    {
        SqlParameter CountryId = new SqlParameter("@CountryId", bl_state.CountryId);
        DataTable dt = dl_connection.UseDataTablePro("SelectStateCountryId", CountryId);
        return dt;
    }

    public DataTable SelectStateName(BL_State bl_state)
    {
        SqlParameter CountryId = new SqlParameter("@CountryId", bl_state.CountryId);
        SqlParameter State = new SqlParameter("@State", bl_state.State);
        DataTable dt = dl_connection.UseDataTablePro("SelectStateName", CountryId, State);
        return dt;
    }
   
}