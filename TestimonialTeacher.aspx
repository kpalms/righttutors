﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TestimonialTeacher.aspx.cs" Inherits="TestimonialTeacher" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <script type="text/javascript" src="highslide/highslide-with-html.js"></script>
<link rel="stylesheet" type="text/css" href="highslide/highslide.css" />
<script type="text/javascript">
    hs.graphicsDir = 'highslide/graphics/';

    hs.outlineType = 'rounded-white';

    hs.showCredits = false;

    hs.wrapperClassName = 'draggable-header';

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <!--=========== BEGIN WHY US SECTION ================-->
    <section id="whyUs">
      <!-- Start why us top -->
      <div class="row">        
        <div class="col-lg-12 col-sm-12">
          <div class="whyus_top">
            <div class="container">
              <!-- Why us top titile -->
              <div class="row">
                <div class="col-lg-12 col-md-12"> 
                  <div class="title_area">
                    <h2 class="title_two">writes testimonials</h2>
                    <span></span> 
                  </div>
                </div>
              </div>
              <!-- End Why us top titile -->
              <!-- Start Why us top content  -->
              <div class="row">


                <div style="width:50%;float:left">
                  <div class="single_whyus_top wow fadeInUp">
                  <a href="AboutRightTutors.aspx" onclick="return hs.htmlExpand(this, { objectType: 'iframe' } )">
                    <div class="whyus_icon">
                      <span class="fa fa-users"></span>
                   <%--  <img src="img/logo.jpg" height="100" width="100" />--%>
                    </div>
                    </a>
                    <h3>About Right Tutors</h3>
                    <p>Text Text Text Text Text Text Text Text Text Text Text Text Text Text <a href="AboutRightTutors.aspx" onclick="return hs.htmlExpand(this, { objectType: 'iframe' } )">Click Here..</a></p>
                  </div>
                </div>

                <div style="width:50%;float:left">
                  <div class="single_whyus_top wow fadeInUp">
                  <a href="AboutStudent.aspx" onclick="return hs.htmlExpand(this, { objectType: 'iframe' } )">
                    <div class="whyus_icon">
                      <span class="fa fa-users"></span>
                    </div>
                    </a>
                    <h3>About Student</h3>
                    <p>Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text <a href="AboutStudent.aspx" onclick="return hs.htmlExpand(this, { objectType: 'iframe' } )">Student</a></p>
                  </div>
                </div>


              </div>
              <!-- End Why us top content  -->
            </div>
          </div>
        </div>        
      </div>
      <!-- End why us top -->


    </section>
    <!--=========== END WHY US SECTION ================-->


</asp:Content>

