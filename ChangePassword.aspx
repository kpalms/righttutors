﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">
         .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=90);
        opacity: 0.8;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        border-width: 3px;
        border-style: solid;
        border-color: black;
        padding-top: 10px;
        padding-left: 10px;
        width: 300px;
        height: 140px;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <section id="contact">
      <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">Change Password</h2>
              <span></span> 
            </div>
          </div>
       </div>
       <div class="row" >



<div class="container" id="parent">

  <div class="row">
    <div class="col-lg-12">
      <div class="row ">

        <div class="col-lg-12" id="child" >
 <div align="center" >
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
 <ContentTemplate>
<asp:Label ID="lblmsg" runat="server" Visible="false"></asp:Label><br />
<asp:TextBox ID="txtpassword" runat="server" TextMode="Password" class="wp-form-control wpcf7-text" placeholder="Enter Current Password"></asp:TextBox>
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter Current Password" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtpassword"></asp:RequiredFieldValidator>
<br />
<asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" class="wp-form-control wpcf7-text" placeholder="Enter New Password"></asp:TextBox>
<asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="Please Enter New Password" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtNewPassword"></asp:RequiredFieldValidator>
<br />
<asp:TextBox ID="txtReTypePassword" runat="server" TextMode="Password" class="wp-form-control wpcf7-text" placeholder="Enter ReType Password"></asp:TextBox>
<asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="Please Enter ReType Password" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtReTypePassword"></asp:RequiredFieldValidator>
<br />
<asp:CompareValidator ID="CompareValidator1" runat="server" 
ErrorMessage="password does not match" ControlToCompare="txtNewPassword" ControlToValidate="txtReTypePassword"
ForeColor="Red" ValidationGroup="new"></asp:CompareValidator>
<br />
<asp:Button ID="btnsubmit" runat="server" Text="Change Password" ValidationGroup="new" class="wpcf7-submit"
onclick="btnsubmit_Click" />
 </ContentTemplate>
</asp:UpdatePanel>


<asp:HiddenField ID="hfHidden" runat="server" />
    <asp:Panel ID="Panel1" CssClass="modalPopup" align="center" style = "display:none" runat="server">
    <table align="center">
    <tr>
   <b> Your Password Is Change Sussfully ! </b>
    </tr>
    <br />
   <br />
   <br />
    <tr>
    <td>
    <asp:Button ID="Button2" class="LoginButton" runat="server" Text="Ok"/>
    </td>
    </tr>
    </table>
    
    </asp:Panel>
   
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" BackgroundCssClass="modalBackground"
    CancelControlID="Button2" PopupControlID="Panel1" TargetControlID="hfHidden" runat="server">
    </ajaxToolkit:ModalPopupExtender>

</div>
        </div>
      </div>
    </div>
  </div>



       </div>
      </div>
    </section>
</asp:Content>

