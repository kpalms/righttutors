﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="StudentRegistration.aspx.cs" Inherits="Student_StudentRegistration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">
         .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=90);
        opacity: 0.8;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        border-width: 3px;
        border-style: solid;
        border-color: black;
        padding-top: 10px;
        padding-left: 10px;
        width: 300px;
        height: 140px;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 
 

    <!--=========== BEGIN CONTACT SECTION ================-->
    <section id="contact">
      <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">Student/Parent Registration</h2>
              <span></span> 
              <p>The details below will not be visible anywhere on the site, but will be given to those tutors who you choose to teach you. Please ensure your e-mail address is correct so we can advise you when you receive replies from tutors.</p>
            </div>
          </div>
       </div>
       <div class="row" >



<div class="container" id="parent">
  <div class="row">
    <div class="col-lg-12">
      <span style="font-size:12px;color:Red"> * Required Fields </span>
      <br />
      <br>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
<ContentTemplate>



<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<asp:TextBox ID="txtName" runat="server" class="wp-form-control wpcf7-text" placeholder="Your name"></asp:TextBox>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtName"></asp:RequiredFieldValidator>
</div>
</div>


<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<asp:TextBox ID="txtMiddleName" runat="server" class="wp-form-control wpcf7-text" placeholder="Middle Name"></asp:TextBox>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
</div>
</div>


<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<asp:TextBox ID="txtLastName" runat="server" class="wp-form-control wpcf7-text" placeholder="Last Name"></asp:TextBox>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtLastName"></asp:RequiredFieldValidator>
</div>
</div>


<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<asp:TextBox ID="txtDob" runat="server" class="wp-form-control wpcf7-text" placeholder="Date of Birth dd/MM/yyyy"></asp:TextBox>
 <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="txtDob"
        Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true" InputDirection="RightToLeft" >
        </ajaxToolkit:MaskedEditExtender>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">

</div>
</div>



<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True" 
onselectedindexchanged="ddlCountry_SelectedIndexChanged" class="wp-form-control wpcf7-text">
</asp:DropDownList>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ControlToValidate="ddlCountry" ID="RequiredFieldValidator5"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="*"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
</div>
</div>


<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<asp:DropDownList ID="ddlState" runat="server" 
AutoPostBack="True" onselectedindexchanged="ddlState_SelectedIndexChanged" class="wp-form-control wpcf7-text">
</asp:DropDownList>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ControlToValidate="ddlState" ID="RequiredFieldValidator6"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="*"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
</div>
</div>


<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<asp:DropDownList ID="ddlCity" runat="server" 
AutoPostBack="True" onselectedindexchanged="ddlCity_SelectedIndexChanged" class="wp-form-control wpcf7-text">
</asp:DropDownList>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ControlToValidate="ddlCity" ID="RequiredFieldValidator7"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="*"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
</div>
</div>



<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<asp:DropDownList ID="ddlArea" runat="server" class="wp-form-control wpcf7-text">
</asp:DropDownList>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ControlToValidate="ddlArea" ID="RequiredFieldValidator8"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="*"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
</div>
</div>



<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" class="wp-form-control wpcf7-textarea" placeholder="Address"></asp:TextBox>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtAddress"></asp:RequiredFieldValidator>
</div>
</div>


<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<asp:TextBox ID="txtpincode" runat="server" MaxLength="6" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="wp-form-control wpcf7-text" placeholder="PinCode"></asp:TextBox>
<asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="please enter 6 Digit Pin Code"   ValidationExpression="[0-9]{6}" ControlToValidate="txtpincode" ValidationGroup="new" ForeColor="Red"></asp:RegularExpressionValidator>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="*" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtpincode"></asp:RequiredFieldValidator>
</div>
</div>


<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<asp:DropDownList ID="ddlGender" runat="server" class="wp-form-control wpcf7-text">
<asp:ListItem Value="0">Selet Gender</asp:ListItem>
<asp:ListItem Value="1">Male</asp:ListItem>
<asp:ListItem Value="2">Female</asp:ListItem>
</asp:DropDownList>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ControlToValidate="ddlGender" ID="RequiredFieldValidator4"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="*"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
</div>
</div>


<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<asp:TextBox ID="txtMobileNo" runat="server" MaxLength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="wp-form-control wpcf7-text" placeholder="Mobile No"></asp:TextBox>
<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="please enter 10 Digit mobile no"   ValidationExpression="[0-9]{10}" ControlToValidate="txtMobileNo" ValidationGroup="new" ForeColor="Red"></asp:RegularExpressionValidator>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="*" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtMobileNo"></asp:RequiredFieldValidator>
</div>
</div>


<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<asp:TextBox ID="txtEmail" runat="server" class="wp-form-control wpcf7-text" placeholder="Email"></asp:TextBox>
<asp:Label ID="lblemail" runat="server" ForeColor="Red"></asp:Label>
<asp:RegularExpressionValidator id="regEmail" ValidationGroup="new" ForeColor="Red" ControlToValidate="txtEmail" Text="(Invalid email)" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Runat="server" />   
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="*" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
</div>
</div>


<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<asp:DropDownList ID="ddlBoard" runat="server" class="wp-form-control wpcf7-text" 
        AutoPostBack="True" onselectedindexchanged="ddlBoard_SelectedIndexChanged">
</asp:DropDownList>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ControlToValidate="ddlBoard" ID="RequiredFieldValidator1"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="*"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
</div>
</div>


<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<asp:DropDownList ID="ddlClass" runat="server" 
AutoPostBack="True" onselectedindexchanged="ddlClass_SelectedIndexChanged" class="wp-form-control wpcf7-text">
</asp:DropDownList>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ControlToValidate="ddlClass" ID="RequiredFieldValidator2"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="*"
InitialValue="0" runat="server"  Display="Dynamic"></asp:RequiredFieldValidator>
</div>
</div>

<span style="font-size:10px;color:Red">Select Subject which requires the Right Tutor</span>
<br />
<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<asp:CheckBoxList ID="chksubject" RepeatDirection="Horizontal" RepeatColumns="5" runat="server"></asp:CheckBoxList>
<asp:Label ID="lblsubject" runat="server" ForeColor="Red"></asp:Label>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">

</div>
</div>
<br />

<span style="font-size:10px;color:red">select your profile pic (png,jpg,gif,bmp,jpeg)</span>
<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<asp:FileUpload ID="fileimage" runat="server"  />
<br />
<asp:RegularExpressionValidator ID="RegularExpressionValidator4" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.bmp|.jpeg)$"
ControlToValidate="fileimage" ValidationGroup="new" runat="server" Font-Size="10px" ForeColor="Red" ErrorMessage="Please select a valid png,jpg,gif,bmp,jpeg file."
Display="Dynamic" />
</div>
<div class="col-lg-1 col-md-1 col-sm-1">

</div>
</div>
<br />
<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<asp:TextBox ID="txtPassword" runat="server" TextMode="Password" class="wp-form-control wpcf7-text" placeholder="Password"></asp:TextBox>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="*" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
</div>
</div>


<div class="row">
<div class="col-lg-2 col-md-2 col-sm-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<asp:TextBox ID="txtReTypePassword" runat="server" TextMode="Password" class="wp-form-control wpcf7-text" placeholder="ReType Password"></asp:TextBox>
</div>
<div class="col-lg-1 col-md-1 col-sm-1">
<asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="*" ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ControlToValidate="txtReTypePassword"></asp:RequiredFieldValidator>
</div>
</div>

<asp:CompareValidator ID="CompareValidator1" runat="server" 
ErrorMessage="password does not match" ControlToCompare="txtPassword" ControlToValidate="txtReTypePassword"
ForeColor="Red" ValidationGroup="new"></asp:CompareValidator>

<div class="row">
<div class="col-lg-5 col-md-5 col-sm-5">
</div>
<div class="col-lg-2 col-md-2 col-sm-2">
<asp:Button ID="btnsubmit" runat="server" Text="Submit" ValidationGroup="new" class="LoginButton"
onclick="btnsubmit_Click" />
</div>
<div class="col-lg-5 col-md-5 col-sm-5">
</div>
</div>

        </ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="btnsubmit" />
</Triggers>
</asp:UpdatePanel>

  
  <asp:HiddenField ID="hfHidden" runat="server" />
    <asp:Panel ID="Panel1" CssClass="modalPopup" align="center" style = "display:none" runat="server">
    <table align="center">
    <tr>
    <b> Thank Your For Registration !<br />
  Check Your Email and Complete Your Verification
    </tr>
    <br />
    <br />
    <tr>
    <asp:Button ID="btnok" OnClick="btnok_Click" class="LoginButton"  runat="server" Text="Ok"/>
    </tr>
    </table>
    
    </asp:Panel>
   
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" BackgroundCssClass="modalBackground"
    CancelControlID="hfHidden"  PopupControlID="Panel1" TargetControlID="hfHidden" runat="server">
    </ajaxToolkit:ModalPopupExtender>


       </div>
      </div>
      </div>
      </div>
      </div>
    </section>
    <!--=========== END CONTACT SECTION ================-->


</asp:Content>

